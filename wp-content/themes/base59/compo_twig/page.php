<?php
/*
Template Name: Page TWIG
*/ ?>
<?php get_header();
	
	require_once(__DIR__.'/twig/vendor/autoload.php');
	$timber = new \Timber\Timber();
	
	$context = Timber::context();
	$args = ['posts_per_page'=>99999, 'post_type'=>'post'];
	$context['posts'] = Timber::get_posts($args);
	Timber::render( 'templates/page.twig', $context );
	
get_footer(); ?>