<?php

//===paypal request===
function pp_form(){
	$form = '';

	if(is_user_logged_in()){
		$summ = (int)get_user_meta(get_current_user_id(), 'payment_price', true);

		$form.='<h3>'.__('Choose the payment method that suits you best', 'base').'</h3>
    			<div class="popup-payment__info">
		          	<div class="popup-payment__row">
		            	<span>Subtotal:</span>
		            	<span>$'.$summ.'.00</span>
		          	</div>
		          	<div class="popup-payment__row">
		            	<strong>Total:</strong>
		            	<strong>$'.$summ.'.00</strong>
		          	</div>
		        </div>
				<div class="center-holder">
					<button type="submit" id="GoToPayPaypalButt" class="btn btn--accent">'.__('Paypal', 'base').'</button>
					<button type="submit" id="GoToPayStripeButt" class="btn btn--accent" data-userID="'.get_current_user_id().'">'.__('Stripe', 'base').'</button>
				</div>

				<form action="https://www.paypal.com/cgi-bin/webscr" method="POST" id="gotopaypal">
					<input type="hidden" name="business"      value="'.get_field('sopt_paypal_email', 'option').'">
					<input type="hidden" name="cmd"           value="_xclick">
					<input type="hidden" name="item_name"     value="'.__('Payment', 'base').'">
					<input type="hidden" name="amount"        value="'.$summ.'">
					<input type="hidden" name="currency_code" value="USD">
					<input type="hidden" name="notify_url"    value="'.esc_url(home_url()).'/?sportkey='.md5(get_current_user_id()).'">
					<input type="hidden" name="return"        value="'.esc_url(home_url()).'">
					<input type="hidden" name="cancel_return" value="'.esc_url(home_url()).'">
				</form>

				<script src="https://js.stripe.com/v3/"></script>
				<script>
					document.addEventListener("DOMContentLoaded", function(){
						//===PAYPAL GO TO PAY===
						$(document).on("click", "#GoToPayPaypalButt", function(event){
							$("#gotopaypal").submit();
						});

						//===STRYPE GO TO PAY===
						$(document).on("click", "#GoToPayStripeButt", function(event){
							var stripe = Stripe(sopt_strype_key);
							userID = $(this).attr("data-userID");
							$.ajax({
								type: "POST",
								url:  WPajaxURL,
								data:{
										action: "stripeproceed",
										userID: userID,
									 },
								success: function(data, textStatus, XMLHttpRequest) {
									//console.log(data);
									data = JSON.parse(data);
									return stripe.redirectToCheckout({sessionId:data.id});
								},
								error: function(MLHttpRequest, textStatus, errorThrown) {
									
								}
							});
						});
					});
				</script>';
	}
	return $form;
}


//===CALLBACK STRIPE===
add_action('init', function(){
	//$stripe_response = '';

    if( is_user_logged_in() && isset($_GET['response1']) && isset($_GET['userID']) ){
        $_GET['userID'] = (int)$_GET['userID'];
        $user           = get_user_by('id', $_GET['userID']);

        if($user){
            if($_GET['response1'] == 'success' && isset($_GET['session_id'])){
                require dirname(__FILE__).'/stripe/init.php';
    			\Stripe\Stripe::setApiKey(get_field('sopt_strype_key_secret', 'option'));
                $session  = \Stripe\Checkout\Session::retrieve($_GET['session_id']);
                if(!empty($session)){
                    if( $session->payment_status == 'paid' ){
                        update_user_meta($_GET['userID'], 'admin_payment', 1);
						
						wp_redirect( home_url().'/?afterpayment' );
						exit();
                    }else{
                        //$stripe_response .= 'Invalid Stripe Session 3!';
                    }
                }else{
                	//$stripe_response .= 'Invalid Stripe Session 4!';
                }
            }else if($_GET['response'] == 'cancel'){
                //$stripe_response .= 'Payment Cancelled!';
            }
        }else{
            //$stripe_response .= 'User not found!';
        }
    }
    //return $stripe_response;
});


//===CALBACK PAYPAL===
add_action('init', function(){
	if( isset($_POST['sportkey']) ){
		$_GET['sportkey'] = $_GET['sportkey'] ?? 0;
		$_GET['sportkey'] = (int)$_GET['sportkey'];
		$_GET['sportkey'] = md5($_GET['sportkey']);
		$user = get_user_by('id', $_GET['sportkey']);
		if($user){
			$var = print_r($_GET, true);
			$var .= print_r($_POST, true);
			update_user_meta($_GET['sportkey'], 'admin_payment', 1);
			update_user_meta($_GET['sportkey'], 'payment_paypal_sesid', $var);
			
			wp_redirect( home_url().'/?afterpayment' );
			exit();
		}
	}
});


//===stripe proceed===
add_action('wp_ajax_stripeproceed',        'stripeproceed');
//add_action('wp_ajax_nopriv_stripeproceed', 'stripeproceed');
function stripeproceed(){
	$amount = (int)get_user_meta(get_current_user_id(), 'payment_price', true)*100;
	if($amount==0){
		$amount = 100;
	}
	
	require dirname(__FILE__).'/stripe/init.php';
    \Stripe\Stripe::setApiKey(get_field('sopt_strype_key_secret', 'option'));
	$checkout_session = \Stripe\Checkout\Session::create([
		'payment_method_types' => ['card'],
		'line_items' => [[
			'price_data' => [
				'currency'     => 'usd',
				'unit_amount'  => $amount,
				'product_data' => [
					'name'         => __('Payment', 'base'),
					'description'  => __('Payment for your account', 'base'),
				],
			],
			'quantity' => 1,
		]],
		'mode'        => 'payment',
		'success_url' => home_url().'/?userID='.get_current_user_id().'&response1=success&session_id={CHECKOUT_SESSION_ID}',
		'cancel_url'  => home_url().'/?response1=cancel',
	]);


	if( isset($checkout_session->id) ){
		update_user_meta(get_current_user_id(), 'payment_stripe_sesid', $checkout_session->id);
	}

	echo json_encode(['id'=>$checkout_session->id]);
	die();
}