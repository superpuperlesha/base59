<?php
/*
Template Name: My account [Notifications]
*/ ?>

<?php if(!is_user_logged_in()){
	wp_redirect(home_url().'/?upenauth');
	exit();
} ?>

<?php get_header() ?>

<?php page_notifys(1, (isset($_GET['unread']) ?'0' :'1') ) ?>
	
<?php get_footer() ?>