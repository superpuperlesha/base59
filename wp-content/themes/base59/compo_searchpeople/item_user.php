<div class="row mt-1">
	<div class="col-md-3">
		<?php get_template_part('/compo_searchpeople/item_avatar', '', ['ID'=>$args->ID, 'img'=>'thumbnail_100x100']) ?>
	</div>
	<div class="col-md-9"><?php
		$arrUnreaded = (array)unserialize( get_user_meta(get_current_user_id(), 'new_pmsg', true) ); ?>
		<h3>
			<?php if( isset($arrUnreaded[$args->ID]) && $arrUnreaded[$args->ID]==1 ){ ?>
				<i class="fa fa-envelope text-success" data-toggle="tooltip" data-placement="top" data-toggle="tooltip" title="<?= __('New message in chat!', 'base') ?>"></i><?php
			} ?>
			<a href="#uID" data-profileuser="<?= $args->ID ?>" class="w51p_url_pchat" data-toggle="tooltip" data-placement="top" data-toggle="tooltip" data-placement="top" title="<?= __('Open private chat', 'base') ?>"><?= $args->first_name ?></a>
		</h3>
		<p><?= get_user_meta($args->ID, 'usr_zip_loc', true) ?> (<?= get_user_meta($args->ID, 'usr_zip_lt', true) ?>, <?= get_user_meta($args->ID, 'usr_zip_ln', true) ?>)</p>
		<p><?= __('On site with:', 'base') ?> <?= date(get_option('date_format'), strtotime($args->user_registered))  ?></p>
	</div>
</div>