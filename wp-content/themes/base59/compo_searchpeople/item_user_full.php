<div class="row">
    <div class="col-md-12">
        <a href="<?= get_permalink(get_field('sopt_myprofile_view_pid', 'option')) ?>/?profile=<?= $args['ID'] ?>" class="dc_myacc_view" data-userid="<?= $args['ID'] ?>">
            <img src="<?= get_myavatar_url($args['ID'], 'thumbnail_140x140') ?>" width="100" height="100" alt="Ava">
        </a>
        
        <a href="<?= get_permalink(get_field('sopt_myprofile_view_pid', 'option')) ?>/?profile=<?= $args['ID'] ?>" class="dc_myacc_view" data-userid="<?= $args['ID'] ?>"><?= $args['fname'] ?></a>

        <img src="<?= (get_user_meta($args['ID'], 'usr_isee', true) ?verified_y :verified_n) ?>" width="24" height="24" alt="verified"><?php

        if(current_user_can('administrator')){ ?>
            <i class="fa fa-ban        <?= ( $args['admin_blocked']==1 ?'text-danger' :'text-success') ?>" title="<?= __('Block User Profile', 'base') ?>"></i>

            <i class="fa fa-low-vision <?= ( $args['usr_visible']==0   ?'text-danger' :'text-success') ?>" title="<?= __('Visible my profile', 'base') ?>"></i>

            <i class="fa fa-plane      <?= ( $args['trip']==1          ?'text-danger' :'text-success') ?>" title="<?= __('In trip', 'base').' '.date('F d, Y', $args['ttss']).' - '.date('F d, Y', $args['ttsf']) ?>"></i>

            <i class="fa fa-usd        <?= ( $args['admin_payment']==0 ?'text-danger' :'text-success') ?>" title="<?= __('Account not paid', 'base') ?>"></i><?php
        } ?>
        
        <img src="<?= (get_yn_user_fav($args['ID']) ?hartFilled :hartNFilled) ?>" id="usr_fav_i_<?= $args['ID'] ?>" class="dc_favicon_addel" data-favid="<?= $args['ID'] ?>" alt="add">
    </div>
</div>