<!-- PCHAT create thread -->
<div class="modal fade" id="ds_float_initpmsglist_modal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabelpmil" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __('Create a message', 'base') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div id="ds_float_initpmsglist_modal_box" class="modal-body">
                <form action="#uID" class="form" method="POST">
                    <input type="hidden"  id="crth_user_id"      data-err="<?= __('Please select user!', 'base') ?>">
                    <input type="hidden"  id="crth_parent_id"    value="0">
                    <div class="form__row">
                        <div class="form__input-holder">
                            <input type="text"    id="crth_user_search"  maxlength="51">
                            <label for="crth_user_search"><?= __('Search user', 'base') ?></label>
                        </div>
                    </div>
                    <div class="search-box__container">
                        <div id="crth_user_search_box" class="search-box"></div>
                    </div>
                    <div class="form__row">
                        <div class="form__input-holder">
                            <input type="text"    id="crth_title"        maxlength="1000" data-err="<?= __('Please enter "Title".', 'base') ?>">
                            <label for="crth_title"><?= __('Title message', 'base') ?></label>
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="form__input-holder">
                            <textarea id="crth_content"                  maxlength="5000" data-err="<?= __('Please enter "Content".', 'base') ?>"></textarea>
                            <label for="crth_content"><?= __('Content message', 'base') ?></label>
                        </div>
                    </div>
                    <div id="crth_err"></div>
                    <div class="center-holder three-btns">
                        <button type="button" id="crth_submitter_thrmsg" class="btn btn-primary"><?= __('Create thread', 'base') ?></button>
                        <button type="button" id="crth_submitter_draft"  class="btn btn-primary"><?= __('Create draft', 'base') ?></button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><?= __('Close', 'base') ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal DELETE THREAD -->
<div class="modal fade" id="ds_delthread_modal" tabindex="-1" role="dialog" aria-labelledby="ds_delthread_modalhh" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __('Are you serious?', 'base') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div id="ds_delthread_modal_err"></div>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?= __('Close', 'base') ?></button>
                <button id="wd_pmsg_thread_id_deller_y" class="btn btn-primary"><?= __('Delete', 'base') ?></button>
            </div>
        </div>
    </div>
</div>

<!-- Modal DELETE PMSG-SENTET -->
<div class="modal fade" id="ds_delpmsgsentet_modal" tabindex="-1" role="dialog" aria-labelledby="ds_delpmsgsentet_modalhh" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __('Delete sentet message', 'base') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?= __('Close', 'base') ?></button>
                <button id="wd_pmsg_msg_id_deller_y" class="btn btn-primary" data-errmsg=""><?= __('Delete', 'base') ?></button>
            </div>
        </div>
    </div>
</div>

<!-- Modal DELETE PMSG -->
<div class="modal fade" id="ds_delpmsg_modal" tabindex="-1" role="dialog" aria-labelledby="ds_delpmsg_modalhh" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __('Delete message', 'base') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?= __('Close', 'base') ?></button>
                <button id="pmsg_deleter_y" data-msgid="" data-threadid="" class="btn btn-primary"><?= __('Delete', 'base') ?></button>
            </div>
        </div>
    </div>
</div>


<script>
	
	var threadFilled       = '<?= threadFilled ?>';
    var threadNFilled      = '<?= threadNFilled ?>';


	document.addEventListener('DOMContentLoaded', function(){

		function initMessagesTabs() {
			// MESSAGES aside tabs
			$(".tab-list__item").click(function () {
				let name = $(this).attr("data-name");
				$(".tab-list__item , .messages__list-msg").removeClass("tab-list__item--active active");
				$(this).addClass("tab-list__item--active");
				$(".messages__list-msg[data-name=" + name + "]").addClass("active");
			});
		}

        //===set PMSG informer===
        function sitePMSG(arr){
            if(arr.length){
                $('.menu_pchats').addClass('active');
            }else{
                $('.menu_pchats').removeClass('active');
            }

            arr.forEach((element) => {
                $('#thr_has_npm_'+element).addClass('threadhasnewpmsg');
                
                if($('#thr_has_npm_'+element).hasClass('active')){
                    //var threadid = $('#thr_has_npm_'+element).attr('data-threadid');
                    $('#thr_has_npm_'+element).click();
                }
            });
        }

        function initMessages() {
            
        }

		//===THREAD set groups to fav===
		$(document).on('click', '#wd_pmsg_thread_id_favser', function(){
			var threadarr = [];
			$('.wd_pmsg_thread_id:checked').each(function(){
				threadarr.push($(this).val());
			});
			if(threadarr.length){
				$('#wd_pmsg_ajax_body_tab1').html(Loading);
				$('#wd_pmsg_ajax_body').html('');
				$.ajax({
					type: 'POST',
					url:  WPajaxURL,
					data:{
							action:    'favsthreads',
							threadarr: threadarr,
						 },
					success: function(data, textStatus, XMLHttpRequest) {
						$('#wd_pmsg_ajax_body_tab1').html(data);
						pmsgPageInit();
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
					}
				});
			}else{
				alert( $(this).attr('data-errmsg') );
			}
		});

		//===THREAD set groups to UNREAD===
		$(document).on('click', '#wd_pmsg_thread_id_unread', function(){
			var threadarr = [];
			$('.wd_pmsg_thread_id:checked').each(function(){
				threadarr.push($(this).val());
			});
			if(threadarr.length){
				$('#wd_pmsg_ajax_body_tab1').html(Loading);
				$('#wd_pmsg_ajax_body').html('');
				$.ajax({
					type: 'POST',
					url:  WPajaxURL,
					data:{
							action:    'unreadthreads',
							threadarr: threadarr,
						 },
					success: function(data, textStatus, XMLHttpRequest) {
						$('#wd_pmsg_ajax_body_tab1').html(data);
						pmsgPageInit();
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
					}
				});
			}else{
				alert( $(this).attr('data-errmsg') );
			}
		});

		//===THREAD set groups to READ===
		$(document).on('click', '#wd_pmsg_thread_id_read', function(){
			var threadarr = [];
			$('.wd_pmsg_thread_id:checked').each(function(){
				threadarr.push($(this).val());
			});
			if(threadarr.length){
				$('#wd_pmsg_ajax_body_tab1').html(Loading);
				$('#wd_pmsg_ajax_body').html('');
				$.ajax({
					type: 'POST',
					url:  WPajaxURL,
					data:{
							action:    'readthreads',
							threadarr: threadarr,
						 },
					success: function(data, textStatus, XMLHttpRequest) {
						$('#wd_pmsg_ajax_body_tab1').html(data);
						pmsgPageInit();
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
					}
				});
			}else{
				alert( $(this).attr('data-errmsg') );
			}
		});

		//===SEND PRIVATE MESSAGE===
		$(document).on('click', '#pmsg_submit_post', function(event){
			event.preventDefault();
			var err                  = '';
			var pmsg_threadid        = $('#pmsg_threadid').val();
			var pmsg_userid          = $('#pmsg_userid').val();
			var pmsg_title           = $('#pmsg_title').val();
			var pmsg_txt             = $('#pmsg_txt').val();
			var pmsg_reply_parent_id = $('#pmsg_reply_parent_id').val();

			if(pmsg_title.length<6){
				err = $('#pmsg_title').attr('data-errmsg');
			}
			if(pmsg_txt.length<36){
				err = $('#pmsg_txt').attr('data-errmsg');
			}

			if(err==''){
				$('#wd_pmsg_ajax_body').fadeTo('slow', 0, function(){  });
				$.ajax({
					type: 'POST',
					url:  WPajaxURL,
					data: {
							action:               'writepmsg',
							pmsg_threadid:        pmsg_threadid,
							pmsg_title:           pmsg_title,
							pmsg_txt:             pmsg_txt,
							pmsg_reply_parent_id: pmsg_reply_parent_id,
							pmsg_userid:          pmsg_userid,
						  },
					success: function(data, textStatus, XMLHttpRequest) {
						$('#wd_pmsg_ajax_body').html(data);
						$('#wd_pmsg_ajax_body').fadeTo('slow', 1, function(){  });
						initMessages();
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
					}
				});
			}else{
				$('#pmsg_notation_box').html(err);
			}
		});

		//===MENU PCHAT===
		$(document).on('click', '.menu_pchats', function(event){
			event.preventDefault();
			setURLpage($(this).find('a').attr('href'));
			$('#base_main').fadeTo(fadeSpeed, 0, 'linear', function(){  });
			$.ajax({
				type: 'POST',
				url:  WPajaxURL,
				data:{
						action: 'menupchats',
					 },
				success:     function(data, textStatus, XMLHttpRequest) {
					$('#base_main').html(data);
					$('#base_main').fadeTo('slow', 1, function(){  });
					pmsgPageInit();
					initMessagesTabs();
				},
				error: function(MLHttpRequest, textStatus, errorThrown) {
					top.location.href = WPURL;
				}
			});
		});

		//===Load pmsg for thread===
		$(document).on('click', '.wd_pmsg_thread', function(){
			var threadid = $(this).attr('data-threadid');
			$('#wd_pmsg_ajax_body').fadeTo(fadeSpeed, 0, 'linear', function(){  });

			//===init active line===
			$('.wd_pmsg_thread').removeClass('bg-secondary');
			$(this).addClass('bg-secondary');
			$(this).removeClass('threadhasnewpmsg');

			$.ajax({
				type: 'POST',
				url:  WPajaxURL,
				data: {
						action:   'pmsgforthread',
						threadid: threadid,
					  },
				success: function(data, textStatus, XMLHttpRequest) {
					$('#wd_pmsg_ajax_body').html(data);
					$('#wd_pmsg_ajax_body').fadeTo('slow', 1, function(){  });
					initMessages();
				},
				error: function(MLHttpRequest, textStatus, errorThrown) {
					top.location.href = WPURL;
				}
			});
		});

		//===Load pmsg for sent===
		$(document).on('click', '.wd_pmsg_sentet', function(){
			var msgid = $(this).attr('data-msgid');
			$('#wd_pmsg_ajax_body').fadeTo(fadeSpeed, 0, 'linear', function(){  });
			$('.wd_pmsg_sentet').removeClass('bg-secondary');
			$(this).addClass('bg-secondary');
			$.ajax({
				type: 'POST',
				url:  WPajaxURL,
				data: {
						action: 'pmsgforsentet',
						msgid:  msgid,
					  },
				success: function(data, textStatus, XMLHttpRequest) {
					$('#wd_pmsg_ajax_body').html(data);
					$('#wd_pmsg_ajax_body').fadeTo('slow', 1, function(){  });
					initMessages();
				},
				error: function(MLHttpRequest, textStatus, errorThrown) {
					top.location.href = WPURL;
				}
			});
		});

		//===Load pmsg for draft===
		$(document).on('click', '.wd_pmsg_draft', function(){
			var msgid = $(this).attr('data-msgid');
			$('#wd_pmsg_ajax_body').fadeTo(fadeSpeed, 0, 'linear', function(){  });
			$('.wd_pmsg_draft').removeClass('bg-secondary');
			$(this).addClass('bg-secondary');
			$.ajax({
				type: 'POST',
				url:  WPajaxURL,
				data: {
						action: 'pmsgfordraft',
						msgid:  msgid,
					  },
				success: function(data, textStatus, XMLHttpRequest) {
					$('#wd_pmsg_ajax_body').html(data);
					$('#wd_pmsg_ajax_body').fadeTo('slow', 1, function(){  });
					initMessages();
				},
				error: function(MLHttpRequest, textStatus, errorThrown) {
					top.location.href = WPURL;
				}
			});
		});

		//===Clear info tab===
		$(document).on('click', '#wd_pmsg_tab2', function(){
			//===clear body info===
			$('#wd_pmsg_ajax_body_tab2').html(Loading);
			$('#wd_pmsg_ajax_body').html('');

			$.ajax({
				type: 'POST',
				url:  WPajaxURL,
				data: {
						action:   'pmsgseltlist',
					  },
				success: function(data, textStatus, XMLHttpRequest) {
					$('#wd_pmsg_ajax_body_tab2').html(data);
					$('#wd_pmsg_ajax_body_tab2').find('.messages__item').first().click();
					pmsgPageInit();
				},
				error: function(MLHttpRequest, textStatus, errorThrown) {
					top.location.href = WPURL;
				}
			});
		});

		//===Clear info tab===
		$(document).on('click', '#wd_pmsg_tab3', function(){
			//===clear body info===
			$('#wd_pmsg_ajax_body').html('');
			$('#wd_pmsg_ajax_body_tab3').find('.messages__item').first().click();
		});

		//Set first item active
		$(document).on('click', '#wd_pmsg_tab1', function(){
			//===clear body info===
			$('#wd_pmsg_ajax_body_tab1').find('.messages__item').first().click();
		});

		//===TAB INIT===
		function pmsgPageInit(){
			// MESSAGES icon toggle
			$(".messages__btn-favorite").on("click", function () {
			    $(".messages__btn-icon", this).toggleClass("messages__btn-icon--show");
			});
			initMessages();
		}

		//===delete message draft group===
		$(document).on('click', '#wd_pmsg_dgft_id_deller', function(){
			var msgarr = [];
			$('.wd_pmsg_drft_id:checked').each(function(){
				msgarr.push($(this).val());
			});

			if(msgarr.length){
				$('#wd_pmsg_ajax_body_tab3').html(Loading);
				$('#wd_pmsg_ajax_body').html('');

				$.ajax({
					type: 'POST',
					url:  WPajaxURL,
					data:{
							action: 'deledraftarr',
							msgarr: msgarr,
						 },
					success: function(data, textStatus, XMLHttpRequest) {
						$('#wd_pmsg_ajax_body_tab3').html(data);
						initMessages();
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
					}
				});
			}else{
				alert($(this).attr('data-errmsg'));
			}
		});

		//===THREAD delete group===
		$(document).on('click', '#wd_pmsg_thread_id_deller', function(){
			$('#ds_delthread_modal').modal('show');
			var err = $(this).attr('data-errmsg');
			$('#wd_pmsg_thread_id_deller_y').attr('data-errmsg', err);
			$('#ds_delthread_modal_err').html('');
		});
		$(document).on('click', '#wd_pmsg_thread_id_deller_y', function(){
			var threadarr = [];
			$('.wd_pmsg_thread_id:checked').each(function(){
				threadarr.push($(this).val());
			});
			if(threadarr.length){
				$('#ds_delthread_modal_err').html('');
				$('#ds_delthread_modal').modal('hide');
				$('#base_main').html(Loading);
				$.ajax({
					type: 'POST',
					url:  WPajaxURL,
					data:{
							action:    'delthreads',
							threadarr: threadarr,
						 },
					success: function(data, textStatus, XMLHttpRequest) {
						$('#base_main').html(data);
						pmsgPageInit();
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
					}
				});
			}else{
				$('#ds_delthread_modal_err').html($(this).attr('data-errmsg'));
			}
		});

		//===SELECT PMSG-item===
		$(document).on('click', '.w51p_stop_e', function(event){
			event.stopPropagation();
		});

		//===THREAD SELECT ALL===
		$(document).on('click', '#dc_thread_selall_1', function(event){
			if( $(this).is(':checked') ){
				$('.wd_pmsg_thread_id').prop("checked", true);
			}else{
				$('.wd_pmsg_thread_id').prop("checked", false);
			}
		});

		//===sented pmsgs SELECT ALL===
		$(document).on('click', '#dc_thread_selall_2', function(event){
			if( $(this).is(':checked') ){
				$('.wd_pmsg_sentet_id').prop("checked", true);
			}else{
				$('.wd_pmsg_sentet_id').prop("checked", false);
			}
		});

		//===draft pmsgs SELECT ALL===
		$(document).on('click', '#dc_thread_selall_3', function(event){
			if( $(this).is(':checked') ){
				$('.wd_pmsg_drft_id').prop("checked", true);
			}else{
				$('.wd_pmsg_drft_id').prop("checked", false);
			}
		});

		//===delete message sentet===
		$(document).on('click', '.pmsg_deleterst', function(){
			var msgid = $(this).attr('data-msgid');
			$('#wd_pmsg_ajax_body_tab2').fadeTo(fadeSpeed, 0, 'linear', function(){  });
			$('#wd_pmsg_ajax_body').html('');
			$.ajax({
				type: 'POST',
				url:  WPajaxURL,
				data:{
						action: 'deleterst',
						msgid:  msgid,
					 },
				success: function(data, textStatus, XMLHttpRequest) {
					$('#wd_pmsg_ajax_body_tab2').html(data);
					$('#wd_pmsg_ajax_body_tab2').fadeTo('slow', 1, function(){  });
					initMessages();
				},
				error: function(MLHttpRequest, textStatus, errorThrown) {
					top.location.href = WPURL;
				}
			});

		});

		//===delete message draft===
		$(document).on('click', '.pmsg_deletedraft', function(event){
			event.stopPropagation();
			event.preventDefault();
			var msgid = $(this).attr('data-msgid');
			$('#wd_pmsg_ajax_body_tab3').fadeTo(fadeSpeed, 0, 'linear', function(){  });
			$('#wd_pmsg_ajax_body').html('');
			$.ajax({
				type: 'POST',
				url:  WPajaxURL,
				data:{
						action: 'deletedraftid',
						msgid:  msgid,
					 },
				success: function(data, textStatus, XMLHttpRequest) {
					$('#wd_pmsg_ajax_body_tab3').html(data);
					$('#wd_pmsg_ajax_body_tab3').fadeTo('slow', 1, function(){  });
					initMessages();
				},
				error: function(MLHttpRequest, textStatus, errorThrown) {
					top.location.href = WPURL;
				}
			});

		});

		//===delete message sentet group===
		$(document).on('click', '#wd_pmsg_msg_id_deller', function(){
			$('#ds_delpmsgsentet_modal').modal('show');
			$('#wd_pmsg_msg_id_deller_y').attr('data-errmsg', $(this).attr('data-errmsg'));
		});
		$(document).on('click', '#wd_pmsg_msg_id_deller_y', function(){
			$('#ds_delpmsgsentet_modal_err').html('');
			var msgarr = [];
			$('.wd_pmsg_sentet_id:checked').each(function(){
				msgarr.push($(this).val());
			});

			if(msgarr.length){
				$('#wd_pmsg_ajax_body_tab2').html(Loading);
				$('#wd_pmsg_ajax_body').html('');
				$.ajax({
					type: 'POST',
					url:  WPajaxURL,
					data:{
							action:'deleterstarr',
							msgarr: msgarr,
						 },
					success: function(data, textStatus, XMLHttpRequest) {
						$('#wd_pmsg_ajax_body_tab2').html(data);
						$('#ds_delpmsgsentet_modal_err').html('');
						$('#ds_delpmsgsentet_modal').modal('hide');
						initMessages();
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
					}
				});
			}else{
				$('#ds_delpmsgsentet_modal_err').html($(this).attr('data-errmsg'));
			}
		});

	

		//===SELECT USER===
		$(document).on('click', '.crth_user_id_list', function(){
			let name = $(this).siblings('label').find('span').text();
			$('#dcrth_user_search_box').removeClass('show');
			$('#dcrth_user_search').val(name);
			$('#dcrth_user_id').val( $(this).val() );
		});

		//===CREATE DRAFT PMSG===
		$(document).on('click', '#crth_submitter_draft', function(){
			var err = '';
			$('#crth_err').html('');

			var title = $('#crth_title').val();
			if( title.length < 3 ){
				err = $('#crth_title').attr('data-err');
			}

			var content = $('#crth_content').val();
			if( content.length < 36 ){
				err = $('#crth_content').attr('data-err');
			}

			if(err.length == 0){
				$('#wd_pmsg_ajax_body_tab3').fadeTo(fadeSpeed, 0, 'linear', function(){  });
				$.ajax({
						type: 'POST',
						url:  WPajaxURL,
						data: {
								action:  'draftcreate',
								title:   title,
								content: content,
							  },
						success: function(data, textStatus, XMLHttpRequest) {
							$('#crth_err').html('');
							$('#wd_pmsg_ajax_body_tab3').html(data);
							$('#wd_pmsg_ajax_body_tab3').fadeTo('slow', 1, function(){  });
							$('#ds_float_initpmsglist_modal').modal('toggle');
						},
						error: function(MLHttpRequest, textStatus, errorThrown) {
							top.location.href = WPURL;
						}
					});
			}else{
				$('#crth_err').html(err);
			}
		});
		
		//===CREATE THREAD - SEARCH USERS===
		$(document).on('keyup', '#crth_user_search', function(){
			if( $(this).val().length > 2 ){
				$('#crth_user_search_box').addClass('show');
				$('#crth_user_search_box').html(Loading);
				$('#crth_user_id').val('');
				$('#crth_user_search').closest('.form__input-holder').addClass('not-empty');
				var username = $('#crth_user_search').val();
				$.ajax({
					type: 'POST',
					url:  WPajaxURL,
					data: {
							action:   'intmsglsearchusr',
							username: username,
						  },
					success: function(data, textStatus, XMLHttpRequest) {
						$('#crth_user_search_box').html(data);
						$('#crth_user_search_box').addClass('show');
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
						$('#crth_user_search_box').removeClass('show');

					}
				});
			}
		});

		//===SELECT USER===
		$(document).on('click', '.crth_user_id_list', function(){
			let name = $(this).siblings('label').find('span').text();
			$('#crth_user_search_box').removeClass('show');
			$('#crth_user_search').val(name);
			$('#crth_user_id').val( $(this).val() );
		});

		//===DRAFT - SEARCH USERS===
		$(document).on('keyup', '#dcrth_user_search', function(){
			if( $(this).val().length > 2 ){
				$('#dcrth_user_search_box').addClass('show');
				$('#dcrth_user_search_box').html(Loading);
				$('#dcrth_user_id').val('');
				var username = $('#dcrth_user_search').val();
				$.ajax({
					type: 'POST',
					url:  WPajaxURL,
					data: {
							action:   'intmsglsearchusr',
							username: username,
						  },
					success: function(data, textStatus, XMLHttpRequest) {
						$('#dcrth_user_search_box').addClass('show');
						$('#dcrth_user_search_box').html(data);
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						$('#dcrth_user_search_box').removeClass('show');
						top.location.href = WPURL;
					}
				});
			}
		});

		//===MODAL Create a message thread===
		$(document).on('click', '.wd_pmsg_initlist', function(){
			event.preventDefault();
			var set_def_first_name = $(this).attr('data-username');
			if( set_def_first_name != undefined ){
				$('#crth_user_search').val(set_def_first_name);
				$('#crth_user_search').trigger('keyup');
			}

			$('#ds_float_initpmsglist_modal').modal();
		});

		//===CREATE THREAD && PMSG FROM MODAL===
		$(document).on('click', '#crth_submitter_thrmsg', function(){
			var err = '';
			$('#crth_err').removeClass('text-danger').removeClass('text-success');
			$('#crth_err').html(Loading);

			var userid = parseInt($('#crth_user_id').val());
			if( !Number.isInteger(userid) ){
				err = $('#crth_user_id').attr('data-err');
			}

			var parentid = parseInt($('#crth_parent_id').val());

			var title = $('#crth_title').val();
			if( title.length < 3 ){
				err = $('#crth_title').attr('data-err');
			}

			var content = $('#crth_content').val();
			if( content.length < 36 ){
				err = $('#crth_content').attr('data-err');
			}

			if(err.length == 0){
				createCthread(userid, parentid, title, content, 'crth_err');
			}else{
				$('#crth_err').addClass('text-danger');
				$('#crth_err').html(err);
			}
		});

		//===CREATE THREAD && PMSG FROM DRAFT===
		$(document).on('click', '#dcrth_submitter_thrmsg', function(){
			var err = '';
			$('#dcrth_err').html(Loading);

			var userid = parseInt($('#dcrth_user_id').val());
			if( !Number.isInteger(userid) ){
				err = $('#dcrth_user_id').attr('data-err');
			}

			var parentid = parseInt($('#dcrth_parent_id').val());

			var title = $('#dcrth_title').val();
			if( title.length < 3 ){
				err = $('#dcrth_title').attr('data-err');
			}

			var content = $('#dcrth_content').val();
			if( content.length < 36 ){
				err = $('#dcrth_content').attr('data-err');
			}

			if(err.length == 0){
				createCthread(userid, parentid, title, content, 'dcrth_err');
			}else{
				$('#dcrth_err').html(err);
			}
		});

		function createCthread(userid, parentid, title, content, errbox){
			$.ajax({
				type: 'POST',
				url:  WPajaxURL,
				data: {
						action:  'intmsglcreate',
						userid:   userid,
						parentid: parentid,
						title:    title,
						content:  content,
					  },
				success: function(data, textStatus, XMLHttpRequest) {
					if(data.length > 0){
						$('#'+errbox).html(data);
					}else{
						$('#'+errbox).html('');
						$('.menu_pchats').click();
						$('#ds_float_initpmsglist_modal').modal('hide');
						initMessagesTabs();
					}
				},
				error: function(MLHttpRequest, textStatus, errorThrown) {
					top.location.href = WPURL;
				}
			});
		}

        //===DELETE PRIVATE MESSAGE===
        $(document).on('click', '.pmsg_deleter', function(){
            var msgid    = $(this).attr('data-msgid');
            var threadid = $(this).attr('data-threadid');
            $('#ds_delpmsg_modal').modal('show');
            $('#pmsg_deleter_y').attr('data-msgid',    msgid);
            $('#pmsg_deleter_y').attr('data-threadid', threadid);
        });
        //===DELETE PRIVATE MESSAGE===
        $(document).on('click', '#pmsg_deleter_y', function(){
            var msgid    = $(this).attr('data-msgid');
            var threadid = $(this).attr('data-threadid');
            $('#wd_pmsg_ajax_body').fadeTo(fadeSpeed, 0, 'linear', function(){  });
            $('#ds_delpmsg_modal').modal('hide');
            $.ajax({
                type: 'POST',
                url:  WPajaxURL,
                data: {
                        action:   'delpmsg',
                        threadid: threadid,
                        msgid:    msgid,
                      },
                success:     function(data, textStatus, XMLHttpRequest) {
                    $('#wd_pmsg_ajax_body').html(data);
                    $('#wd_pmsg_ajax_body').fadeTo(fadeSpeed, 1, 'linear', function(){  });
                },
                error: function(MLHttpRequest, textStatus, errorThrown) {
                    top.location.href = WPURL;
                }
            });
        });

		//===ADD DEL to FAV-THREAD===
		$(document).on('click', '.dc_threadfav_del', function(event){
			event.stopPropagation();
			event.preventDefault();
			var threadid = $(this).attr('data-thread');
			var eid      = 'thr_threadfav_'+$(this).attr('data-thread');
			if(threadid>0){
				$.ajax({
					type: 'POST',
					url:  WPajaxURL,
					data: {
							action:   'threadfav',
							threadid: threadid,
						  },
					success: function(data, textStatus, XMLHttpRequest) {
						if(data=='1'){
							$('#'+eid).attr('src', threadFilled);
						}else{
							$('#'+eid).attr('src', threadNFilled);
						}
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
					}
				});
			}
		});

        //===ADD DEL to BLOCKED MESSAGING===
        $(document).on('click', '.dc_frend_blockpmsg', function(){
            var myid  = $(this).attr('data-myid');
            var usrid = $(this).attr('data-usrid');
            $.ajax({
                type: 'POST',
                url:  WPajaxURL,
                data: {
                        action: 'blockpmsgadddel',
                        myid:   myid,
                        usrid:  usrid,
                      },
                success: function(data, textStatus, XMLHttpRequest) {

                },
                error: function(MLHttpRequest, textStatus, errorThrown) {
                    top.location.href = WPURL;
                }
            });
        });

		pmsgPageInit();
	});
</script>