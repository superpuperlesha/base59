<?php
/**
 * The searchform template.
 *
 * @package base
 */
?>
<form method="get" class="d-flex" action="<?php echo esc_url(home_url()) ?>">
	<input type="search" name="s" placeholder="<?php echo get_search_query() ?get_search_query() :__( 'Enter word for search', 'base' ) ?>" value="<?php echo get_search_query() ?>" class="form-control mr-2" aria-label="Search">
	<input type="submit" value="<?php _e( 'Search', 'base' ); ?>" class="btn btn-outline-success" />
</form>