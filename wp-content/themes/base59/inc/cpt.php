<?php


/* Register a custom post type called "xxx". */
function dc_fxxx4_init(){
    $labels = array(
        'name'                  => __( 'CsPost', 'base' ),
        'singular_name'         => __( 'CsPost', 'base' ),
        'menu_name'             => __( 'CsPost', 'base' ),
        'name_admin_bar'        => __( 'CsPost', 'base' ),
        'add_new'               => __( 'Add New', 'base' ),
        'add_new_item'          => __( 'Add New', 'base' ),
        'new_item'              => __( 'New CsPost', 'base' ),
        'edit_item'             => __( 'Edit CsPost', 'base' ),
        'view_item'             => __( 'View CsPost', 'base' ),
        'all_items'             => __( 'CsPost', 'base' ),
        'search_items'          => __( 'Search CsPost', 'base' ),
        'parent_item_colon'     => __( 'Parent CsPost:', 'base' ),
        'not_found'             => __( 'No CsPost found.', 'base' ),
        'not_found_in_trash'    => __( 'No CsPost found in Trash.', 'base' ),
        'featured_image'        => __( 'Cover image', 'base' ),
        'set_featured_image'    => __( 'Set cover image', 'base' ),
        'remove_featured_image' => __( 'Remove cover image', 'base' ),
        'use_featured_image'    => __( 'Use as cover image', 'base' ),
        'archives'              => __( 'CsPost posts archives', 'base' ),
        'insert_into_item'      => __( 'Insert into CsPost', 'base' ),
        'uploaded_to_this_item' => __( 'Uploaded to CsPost', 'base' ),
        'filter_items_list'     => __( 'Filter CsPost list', 'base' ),
        'items_list_navigation' => __( 'CsPost list navigation', 'base' ),
        'items_list'            => __( 'CsPost list', 'base' ),
    );
    
    $args = array(
        'labels'             => $labels,
        'description'        => 'CsPost post type.',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => false,
        'query_var'          => true,
        'rewrite'            => array( 'slug'=>'yyy' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => 20,
        'supports'           => array( 'title', 'editor' ), //, 'excerpt', 'comments', 'author', 'thumbnail'
        'taxonomies'         => array(),
        'show_in_rest'       => true
    );
    register_post_type( 'yyy', $args );

    $labels = array(
        'name'                       => __( 'CsCategory', 'base' ),
        'singular_name'              => __( 'CsCategory', 'base' ),
        'menu_name'                  => __( 'CsCategory', 'base' ),
        'all_items'                  => __( 'All Items', 'base' ),
        'parent_item'                => __( 'Parent Item', 'base' ),
        'parent_item_colon'          => __( 'Parent Item:', 'base' ),
        'new_item_name'              => __( 'New Item Name', 'base' ),
        'add_new_item'               => __( 'Add New Item', 'base' ),
        'edit_item'                  => __( 'Edit Item', 'base' ),
        'update_item'                => __( 'Update Item', 'base' ),
        'view_item'                  => __( 'View Item', 'base' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'base' ),
        'add_or_remove_items'        => __( 'Add or remove items', 'base' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'base' ),
        'popular_items'              => __( 'Popular Items', 'base' ),
        'search_items'               => __( 'Search Items', 'base' ),
        'not_found'                  => __( 'Not Found', 'base' ),
        'no_terms'                   => __( 'No items', 'base' ),
        'items_list'                 => __( 'Items list', 'base' ),
        'items_list_navigation'      => __( 'Items list navigation', 'base' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => true,
        'show_in_menu'               => false,
        'rewrite'                    => array('slug'=>'xxx', 'with_front'=>false),
    );
    register_taxonomy('xxx', array('yyy'), $args);
    
}
//add_action( 'init', 'dc_fxxx4_init' );





