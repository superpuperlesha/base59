<?php
function base_scripts_styles() {
	$in_footer = true;

	//wp_dequeue_style( 'wp-block-library' );
	//wp_dequeue_style( 'wp-block-library-theme' );

	wp_enqueue_style('reboot',              esc_url(get_template_directory_uri()).'/_slicing/assets/css/bootstrap-reboot.min.css',      []); // get_stylesheet_directory_uri
	wp_enqueue_style('bootstrap-grid',      esc_url(get_template_directory_uri()).'/_slicing/assets/css/bootstrap-grid.min.css',        []);
	wp_enqueue_style('bootstrap',           esc_url(get_template_directory_uri()).'/_slicing/assets/css/btthemes/bootstrap.min_6.css', []);
	wp_enqueue_style('font-awesome',        esc_url(get_template_directory_uri()).'/_slicing/assets/css/font-awesome.min.css',          []);
	wp_enqueue_style('datepicker',          esc_url(get_template_directory_uri()).'/_slicing/assets/css/datepicker.min.css',            []);
	//wp_enqueue_style('slick',               esc_url(get_template_directory_uri()).'/_slicing/assets/css/slick.css',                     []);
	//wp_enqueue_style('slickth',             esc_url(get_template_directory_uri()).'/_slicing/assets/css/slick-theme.css',               []);
	//wp_enqueue_style('select2css',          esc_url(get_template_directory_uri()).'/_slicing/assets/css/select2.min.css',               []);
	wp_enqueue_style('theme',               get_stylesheet_uri(),                                                                     []);

	wp_deregister_script('jquery');
	wp_enqueue_script('jquery',             esc_url(get_template_directory_uri()).'/_slicing/assets/js/jquery-3.6.0.min.js',            [],         '', $in_footer);
	//wp_enqueue_script('jquery',             esc_url(get_template_directory_uri()).'/_slicing/assets/js/jquery-2.2.4.min.js',            [],         '', $in_footer);
	//wp_enqueue_script('jquery',             esc_url(get_template_directory_uri()).'/_slicing/assets/js/jquery-2.1.0.min.js',            [],         '', $in_footer);
	wp_enqueue_script('popper',             esc_url(get_template_directory_uri()).'/_slicing/assets/js/popper.min.js',                  ['jquery'], '', $in_footer);
	wp_enqueue_script('bundle',             esc_url(get_template_directory_uri()).'/_slicing/assets/js/bootstrap.bundle.min.js',        ['jquery'], '', $in_footer);
	wp_enqueue_script('bootstrap',          esc_url(get_template_directory_uri()).'/_slicing/assets/js/bootstrap.min.js',               ['jquery'], '', $in_footer);
	wp_enqueue_script('datepicker',         esc_url(get_template_directory_uri()).'/_slicing/assets/js/datepicker.min.js',              ['jquery'], '', $in_footer);
	wp_enqueue_script('datepickerln',       esc_url(get_template_directory_uri()).'/_slicing/assets/js/i18n/datepicker.en.js',          ['jquery'], '', $in_footer);
	//wp_enqueue_script('slick',              esc_url(get_template_directory_uri()).'/_slicing/assets/js/slick.min.js',                   ['jquery'], '', $in_footer);
	//wp_enqueue_script('select2js',          esc_url(get_template_directory_uri()).'/_slicing/assets/js/select2.full.min.js',            ['jquery'], '', $in_footer);
	wp_enqueue_script('comment-reply');
	wp_enqueue_script('gm',                 'https://maps.googleapis.com/maps/api/js?key='.get_field('sopt_goomap_key', 'option').'&libraries=places&language=en', [], '', $in_footer);
	wp_enqueue_script('impl',               esc_url(get_template_directory_uri()).'/impl.js',                                         ['jquery'], '', $in_footer);
}
add_action('wp_enqueue_scripts', 'base_scripts_styles');


/*global $wp_locale;
$wdays = $wp_locale->weekday;
$wdaysA = $wp_locale->weekday_abbrev;
$wdaysI = $wp_locale->weekday_initial;
$month = $wp_locale->month;
$monthA = $wp_locale->month_abbrev;
echo 'var dtpsLangTransObj = {
		days:        [\''.$wdays[0].'\', \''.$wdays[1].'\', \''.$wdays[2].'\', \''.$wdays[3].'\', \''.$wdays[4].'\', \''.$wdays[5].'\', \''.$wdays[6].'\'],
		daysShort:   [\''.$wdaysA[$wdays[0]].'\', \''.$wdaysA[$wdays[1]].'\', \''.$wdaysA[$wdays[2]].'\', \''.$wdaysA[$wdays[3]].'\', \''.$wdaysA[$wdays[4]].'\', \''.$wdaysA[$wdays[5]].'\', \''.$wdaysA[$wdays[6]].'\'],
		daysMin:     [\''.$wdaysI[$wdays[0]].'\', \''.$wdaysI[$wdays[1]].'\', \''.$wdaysI[$wdays[2]].'\', \''.$wdaysI[$wdays[3]].'\', \''.$wdaysI[$wdays[4]].'\', \''.$wdaysI[$wdays[5]].'\', \''.$wdaysI[$wdays[6]].'\'],
		months:      [\''.$month['01'].'\',\''.$month['02'].'\',\''.$month['03'].'\',\''.$month['04'].'\',\''.$month['05'].'\',\''.$month['06'].'\', \''.$month['07'].'\',\''.$month['08'].'\',\''.$month['09'].'\',\''.$month['10'].'\',\''.$month['11'].'\',\''.$month['12'].'\'],
		monthsShort: [\''.$monthA[$month['01']].'\', \''.$monthA[$month['02']].'\', \''.$monthA[$month['03']].'\', \''.$monthA[$month['04']].'\', \''.$monthA[$month['05']].'\', \''.$monthA[$month['06']].'\', \''.$monthA[$month['07']].'\', \''.$monthA[$month['08']].'\', \''.$monthA[$month['09']].'\', \''.$monthA[$month['10']].'\', \''.$monthA[$month['11']].'\', \''.$monthA[$month['12']].'\'],
		today:       \''.__('Today', 'rent_angry_cat').'\',
		clear:       \''.__('Clear', 'rent_angry_cat').'\',
		dateFormat:  \'dd/mm/yyyy\',
		timeFormat:  \'hh:ii aa\',
		firstDay:    1,
	};';*/


