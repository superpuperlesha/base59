<?php


//===USER ADDRESS TO user-table===
/*function new_contact_methods( $contactmethods ) {
    $contactmethods['addr'] = __('Address', 'base');
    return $contactmethods;
}
add_filter( 'user_contactmethods', 'new_contact_methods', 10, 1 );
function new_modify_user_table( $column ) {
    $column['addr'] = 'Addr';
    return $column;
}
add_filter( 'manage_users_columns', 'new_modify_user_table' );
function new_modify_user_table_row( $val, $column_name, $user_id ) {
    switch($column_name){
        case 'addr':
            return get_user_meta($user_id, 'usr_zip_loc', true);
        default:
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );*/


//===admin POST column thumbnail===
function ST4_columns_headp($columns){
	//unset($columns['categories']);
	$my_columns = ['ft_img' => __('Image', 'base'),];
	return array_slice($columns, 0, 1) + $my_columns + $columns;
};
function ST4_columns_contentp($column_name, $post_ID){
	if($column_name == 'ft_img'){
		$img = wp_get_attachment_image_src(get_post_thumbnail_id($post_ID), 'thumbnail_100x100'); ?>
		<img src="<?php echo(isset($img[0]) ?$img[0] :esc_url(get_template_directory_uri()).'/img/noimage.jpg') ?>" alt="thumb" height="100" width="100"><?php
	}
}
add_filter('manage_post_posts_columns',        'ST4_columns_headp');
add_action('manage_post_posts_custom_column',  'ST4_columns_contentp', 10, 2);


//===small margin admin tables forms===
add_action('admin_head', 'insert_header_wpse_51023');
function insert_header_wpse_51023(){
	echo'<style>
			.form-table td, .form-table th{/*admin user*/
				padding: 3px 10px 3px 0;
			}
			.qtranxs-lang-switch{
				cursor: pointer;
			}
			.qtranxs-lang-switch-wrap li.qtranxs-lang-switch.active, .qtranxs-lang-switch-wrap li.qtranxs-lang-switch.active:focus, .qtranxs-lang-switch-wrap li.qtranxs-lang-switch.active:hover {
				background-color: #ffffff;
			}
			.column-ft_img svg{
				max-width: 100px;
				max-height: 100px;
			}
			.multi-language-field{/*QTRANSLATE*/
				margin-top: -20px;
			}
		</style>';
}


/*
 * admin login page logo link to site
 */
function mb_login_url() {  return home_url(); }
add_filter( 'login_headerurl', 'mb_login_url' );

/*
 * admin login page
 */
function my_login_logo() { ?>
	<style>
		#login h1 a, .login h1 a {
			background-image: url('<?php echo get_stylesheet_directory_uri() . '/_slicing/assets/' ?>/images/admin_logo.png');
			height: 40px;
			width: 154px;
			background-size: 154px 40px;
			background-repeat: no-repeat;
			padding-bottom: 10px;
		}
		input[type=color],
		input[type=date],
		input[type=datetime-local],
		input[type=datetime],
		input[type=email],
		input[type=month],
		input[type=number],
		input[type=password],
		input[type=search],
		input[type=tel],
		input[type=text],
		input[type=time],
		input[type=url],
		input[type=week],
		select,
		textarea {
			border: 1px solid #181818 !important;
			border-radius: 7px !important;
		}
		input[type=checkbox],
		input[type=radio] {
			border: 1px solid #181818 !important;
			border-radius: 7px !important;
		}
		.wp-core-ui .button-primary {
			border-radius: 7px !important;
			background: #181818 !important;
			border-color: #181818 !important;
		}
		.login .button.wp-hide-pw .dashicons {
			color: #181818 !important;
		}
		.login .privacy-policy-page-link,
		.login #backtoblog {
			display: none;
		}
	</style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


//===ADD WP customizer field (WP-SETTINGS-DEFAULT)===
// function mytheme_customize_register( $wp_customize ) {
    // $wp_customize->add_section( 'mytheme_company_section' , array(
        // 'title'      => __( 'Additional Company Info', 'mytheme' ),
        // 'priority'   => 30,
    // ));

    // $wp_customize->add_setting('mytheme_company', array());
    // $wp_customize->add_control(new WP_Customize_Control(
        // $wp_customize,
        // 'mytheme_company_control',
            // array(
                // 'label'      => __( 'Company Name', 'mytheme' ),
                // 'section'    => 'mytheme_company_section',
                // 'settings'   => 'mytheme_company-name',
                // 'priority'   => 1
            // )
        // )
    // );
	
// }
// add_action('customize_register', 'mytheme_customize_register');




