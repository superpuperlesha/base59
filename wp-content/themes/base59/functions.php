<?php
error_reporting( E_ALL & ~E_WARNING & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_NOTICE );
//add_filter('https_ssl_verify', '__return_false');

define( 'theme_fs_path', __DIR__ );
define( 'theme_slice_http_path', get_template_directory_uri() . '/_slicing/assets/' );





// Disable actions in core
include( get_template_directory() . '/inc/functions_disables.php' );
// Default settings
include( get_template_directory() . '/inc/default.php' );
// Theme functions
include( get_template_directory() . '/inc/functions_theme.php' );
// Custom Post Types
include( get_template_directory() . '/inc/cpt.php' );
// Custom Menu Walker
include( get_template_directory() . '/inc/classes.php' );
// Theme thumbnails
include( get_template_directory() . '/inc/thumbnails.php' );
// Theme menus
include( get_template_directory() . '/inc/menus.php' );
// Theme css & js
include( get_template_directory() . '/inc/scripts.php' );
// Theme JAX
include( get_template_directory() . '/inc/ajax.php' );
// Theme Customizer
include( get_template_directory() . '/inc/functions_customizer.php' );
// Theme SPEED OPTIMIZATION
include( get_template_directory() . '/inc/functions_optimization.php' );
// Admin page
include( get_template_directory() . '/inc/functions_admin.php' );


//===GUTENBERG BLOCK ACF===
include( get_template_directory() . '/compo_acfgutenbergblock/starter.php' );
//===COMPONENTS/PRIVATE_THREADS_PCHATS===
include( get_template_directory() . '/compo_threads_pchats/starter.php' );
//===COMPONENTS/google events===
include( get_template_directory() . '/compo_events/starter.php' );
//===COMPONENTS/notifys===
include( get_template_directory() . '/compo_notifys/starter.php' );
//===COMPONENTS/post&&images&&ajax===
include( get_template_directory() . '/compo_postimagesajax/starter.php' );
//===COMPONENTS/myaccount===
include( get_template_directory() . '/compo_myaccount/starter.php' );
//===COMPONENTS/search people===
include( get_template_directory() . '/compo_searchpeople/starter.php' );
//===COMPONENTS/group messages===
include( get_template_directory() . '/compo_gmessages/starter.php' );
//===COMPONENTS/generate pdf===
include( get_template_directory() . '/compo_generate_pdf/starter.php' );
//===COMPONENTS/generate xls===
include( get_template_directory() . '/compo_generate_xls/starter.php' );
//===COMPONENTS/SOCIAL AUTH===
include( get_template_directory() . '/compo_auth_social/functions_auth_social.php' );
//===COMPONENTS/CMB2===
//include( get_template_directory() . '/compo_cmb2/starter.php' );
//===COMPONENTS/WOO===
include( get_template_directory() . '/compo_woocommerce/functions_woo.php' );
//===seo===
require_once( theme_fs_path . '/seo.php' );
