<?php
/**
 * Search template.
 *
 * @package base
*/
?>
<?php get_header() ?>

<section class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center"><?= __('Search page') ?></h2>
		</div>
	</div><?php
	if(have_posts()){
		while(have_posts()){
			the_post();
			get_template_part('template_part/php/list_item_post');
		}
		get_template_part('template_part/php/block_pagination');
	}else{
		get_template_part('template_part/php/not_found');
	} ?>
</section>

<?php get_footer() ?>