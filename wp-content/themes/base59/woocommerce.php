<?php
get_header();

    while(have_posts()){
        the_post();
        woocommerce_content();
    }

get_footer();
