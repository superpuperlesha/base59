<?php echo get_css_block('nbo_b1') ?>
<section id="acf_b1_<?php echo $args['wmns_flax_counter'] ?>" class="nbo_b1">
    <div class="container">
        <div class="nbo_b1_item pitno_box">
	        <div class="pitno_2"></div>

	        <?php if( $args['wmns_flax_counter'] == 0 ){ ?>
		        <h1><?php the_sub_field('b1_title') ?></h1>
	        <?php }else{ ?>
		        <h3><?php the_sub_field('b1_title') ?></h3>
	        <?php } ?>

	        <?php if( have_rows('b1_list') ){
				echo'<ul>';
		        while( have_rows('b1_list') ){
			        the_row(); ?>
			        <li>
				        <a href="<?php echo get_page_link( get_sub_field('b1_list_url') ) ?>"><?php the_sub_field('b1_list_title') ?></a>
			            <?php the_sub_field('b1_list_svg') ?>
			        </li><?php
		        }
		        echo'</ul>';
			} ?>
        </div>
        <div class="nbo_b1_item nbo_b1_item_center">
	        <div class="nbo_b1_item_avaround">
                <img data-src="<?php echo jy_slice_http_path ?>img/dote.jpg" width="460" height="710" alt="dote">
		    </div>
        </div>
        <div class="nbo_b1_item pitno_box">
	        <div class="pitno_1"></div>

	        <?php the_sub_field('b1_content') ?>

	        <?php echo( get_sub_field('b1_url') ? get_cursor( get_sub_field('b1_url_title'), get_page_link( get_sub_field('b1_url') ), false ) :'' ) ?>
        </div>
    </div>
</section>