<div class="row">
	<div class="col-md-12">
		<h1><?= __( 'Not found', 'base' ) ?></h1>
		<p><?= __( 'Sorry, the page has been deleted or renamed!', 'base' );?></p>
		<?php get_search_form() ?>
	</div>
</div>