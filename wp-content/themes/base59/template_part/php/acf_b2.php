<?php echo get_css_block('acf_b2') ?>
<section id="acf_b2_<?php echo $args['wmns_flax_counter'] ?>" class="acf_b2">
	<div class="container">
		<h3><?php the_sub_field( 'acf_b2_title' ) ?></h3>
		<?php the_sub_field( 'acf_b2_content' ) ?>
	</div>
</section>