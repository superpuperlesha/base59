<div id="post-<?= get_the_ID() ?>" <?php post_class('row') ?>>
  <div class="col-md-3"><?php
      $img         = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail_300x200');
      $image_alt   = get_post_meta($img, '_wp_attachment_image_alt', true);
      $image_alt   = ($image_alt ?htmlspecialchars($image_alt) :htmlspecialchars(get_the_title()));
      $src         = (isset($img[0]) ?$img[0] :esc_url(get_template_directory_uri()).'/img/noimage.jpg'); ?>
      <img src="<?= $src ?>" alt="<?= $image_alt ?>" width="300" height="200" class="img-fluid">
  </div>
  <div class="col-md-9">
      <h2>
        <a href="<?php the_permalink() ?>">
          <?= get_the_title() ?>
        </a>
      </h2>
    <?= get_the_excerpt() ?>
  </div>
</div>