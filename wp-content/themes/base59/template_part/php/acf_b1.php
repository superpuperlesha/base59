<?php echo get_css_block('acf_b1') ?>
<section id="acf_b1_<?php echo $args['wmns_flax_counter'] ?>" class="acf_b1">
    <div class="container">
        <h3><?php the_sub_field( 'acf_b1_title' ) ?></h3>
        <?php the_sub_field( 'acf_b1_content' ) ?>
    </div>
</section>