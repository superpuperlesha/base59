<?php
/**
 * Template 404.
 *
 * @package base
 */
?>
<?php get_header() ?>

	<section class="container">
		<div class="row">
			<div class="col-mg-4">
				<div class="text-center">
					<h1>404</h1>
					<?php get_template_part('template_part/php/not_found') ?>
				</div>
			</div>
		</div>
	</section>

<?php get_footer() ?>