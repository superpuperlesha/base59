<?php

add_action('acf/init', 'my_acf_init_block_types');
function my_acf_init_block_types(){
    if(function_exists('acf_register_block_type')){
        acf_register_block_type([
            'name'              => 'testimonial',
            'title'             => __('TE title Block xxx'),
            'description'       => __('Testimonial block xxx.'),
            'render_template'   => 'compo_acfgutenbergblock/testimonial.php',
            'category'          => 'formatting',
            'icon'              => 'admin-comments',
            'keywords'          => ['testimonial', 'quote'],
			'enqueue_style'     => get_template_directory_uri().'/compo_acfgutenbergblock/css.css',
		]);
	}
}

?>