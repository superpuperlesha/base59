<?php
/*
Template Name: Event for users
*/ ?>

<?php if(!is_user_logged_in()){
	wp_redirect(home_url().'/?upenauth');
	exit();
} ?>

<?php get_header() ?>

<?php page_events(1, (isset($_GET['past']) ?1 :0)) ?>

<?php get_footer() ?>