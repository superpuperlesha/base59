<?php
/*
Template Name: xxx
*/
get_header(); ?>

<?php while(have_posts()){
	the_post(); ?>
	
	<?php the_content() ?>



	<?php //===acf repeater PROGRESSIVE===
	if( have_rows ('hom_slider_list') ){
		while ( have_rows('hom_slider_list') ){
			the_row(); ?>

			<?php the_sub_field('hom_slider_list_name') ?>

			<?php
			$img_id = (int)get_sub_field('hom_slider_list_img');
			$img_2x = wp_get_attachment_image_src( $img_id, 'thumbnail_1920x1080' );
			$img_1x = wp_get_attachment_image_src( $img_id, 'thumbnail_800x600' );

			/*$img_path_webp = ABSPATH . substr( $img_2x[0], strpos( $img_2x[0], '/wp-content/' ) ) . '.webp';
			$img_2x = file_exists( $img_path_webp ) ? $img_2x[0] . '.webp' : $img_2x[0];

			$img_path_webp = ABSPATH . substr( $img_1x[0], strpos( $img_1x[0], '/wp-content/' ) ) . '.webp';
			$img_1x = file_exists( $img_path_webp ) ? $img_1x[0] . '.webp' : $img_1x[0];*/

			$image_alt = get_post_meta( $img_id, '_wp_attachment_image_alt', true ); ?>
			<picture>
				<source media="(min-width:800px)" srcset="<?php echo esc_url( $img_2x ) ?>">
				<source media="(max-width:800px)" srcset="<?php echo esc_url( $img_1x ) ?>">
				<img src="<?php echo $img_1x[0] ?>" alt="<?php echo esc_attr( $image_alt ? $image_alt : get_the_title() ) ?>">
			</picture>

			<?php echo wp_get_attachment_image( $img_id, 'full', "", array("class" => "fadeIn")) ?>

			<?php _e('xxx', 'base') ?>
		<?php }
	} ?>


	
	<?php //===acf repeater===
	if( have_rows ('hom_slider_list') ){
		while ( have_rows('hom_slider_list') ){
			the_row(); ?>

			<?php the_sub_field('hom_slider_list_name') ?>

			<?php if ( !wp_is_mobile() ){
				$img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'thumbnail_1920x1080' );
				$w = 1920;
				$h = 1080;
			}else{
				$img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'thumbnail_800x600' );
				$w = 800;
				$h = 600;
			}
			$image_alt = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true ); ?>
			
			<img src="<?php echo esc_url( isset( $img[0] ) ? $img[0] : get_template_directory_uri() . '/img/noimage.jpg') ?>"
				 alt="<?php echo esc_attr( $image_alt ? $image_alt : get_the_title() ) ?>"
				 width="<?php echo $w ?>"
				 height="<?php echo $h ?>"
			>

			<?php _e('xxx', 'base') ?>
		<?php }
	} ?>

	
	<?php //===get posts in loop===
	$lastposts = get_posts( array('posts_per_page'=>3, 'post_type'=>'xxx') );
	foreach( $lastposts as $post ){
		setup_postdata( $post );
	}
	wp_reset_postdata(); ?>
<?php } ?>


<?php //===custom loop===
query_posts(array('post_type'      => 'xxx',
				  'posts_per_page' => -1,
	)
);
while(have_posts()){
	the_post();
	echo wp_get_attachment_image( $img_id, 'full', "", array("class" => "fadeIn"));
}
wp_reset_query(); ?>


<?php
$arr = get_field('empsl_press_list');
foreach( $arr as $arri ){ ?>
	<a href="<?php echo get_permalink( $arri ) ?>"><?php
		echo wp_get_attachment_image( get_post_thumbnail_id( $arri ), 'full', "", array("class" => "fadeIn")) ?>
		<?php echo get_post_time( get_option('date_format'), true, $arri ) ?>
		<?php echo get_the_title( $arri ) ?>
		<?php echo get_the_excerpt( $arri ) ?>
        <?php echo apply_filters('the_excerpt', get_the_excerpt( $arri ) )  ?>
		<?php _e('', 'base') ?>
	</a><?php
} ?>


<?php //===template part===
get_template_part('template_part/xxx', null, array( 'arg'=>1 )  ); ?>

	
<?php get_footer(); ?>