<?php
/*
Template Name: ACF Flexible
*/
get_header(); ?>

<?php if( have_rows( 'acf' ) ){
	$wmns_flax_counter = 0;
	while ( have_rows( 'acf' ) ){
		the_row();
		get_template_part('template_part/php/'.get_row_layout(), null, [ 'wmns_flax_counter'=>$wmns_flax_counter++ ] );
	}
}else{
	echo '<h6>'.__('This page does not contain flexible blocks!', 'base').'</h6>';
} ?>
	
<?php get_footer() ?>