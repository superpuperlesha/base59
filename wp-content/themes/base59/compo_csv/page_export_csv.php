<?php
/*
Template Name: Page [CSV]
*/
    
    
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header( 'Content-Disposition: attachment;filename='.microtime(true).'.csv');
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    echo "\xEF\xBB\xBF"; // UTF-8 BOM
    
    
	$fp = fopen('php://output', 'x');
	$query1 = new WP_Query(array(
			'posts_per_page' => 999999,
	));
	while( $query1->have_posts() ){
		$query1->the_post();
		$fields = [
			1,
			2,
			3,
		];
		fputcsv( $fp, $fields );
	}
	
	fclose( $fp );
	
?>