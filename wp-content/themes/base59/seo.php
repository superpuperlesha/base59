<?php

	// Page Title
	add_filter( 'wp_title', 'site_seo_title', 10, 3 );
	function site_seo_title( $title, $sep, $seplocation ) {
		/*if( $title ){
			return $title;
		}*/

		$page_id = get_queried_object_id();
		$title = get_field( 'site_seo_title', $page_id );
		$title = site_seo_replace_domain( $title );
		$title = site_seo_replace_girl( $title );

		return $title;
	}

	// Page Description
	function site_seo_description() {
		$page_id = get_queried_object_id();
		$description = get_field( 'site_seo_description', $page_id );
		$description = site_seo_replace_domain( $description );
		$description = site_seo_replace_girl( $description );

		if( $description ){
			?><meta name="description" content="<?php echo esc_attr( $description ) ?>"><?php
		}
	}
	add_action( 'wp_head', 'site_seo_description' );



	// simple shortcode
	function site_seo_replace_domain( $txt='' ){
		return str_replace( '[domain]', $_SERVER['SERVER_NAME'], $txt );
	}

	// simple shortcode
	function site_seo_replace_girl( $txt='' ){
		global $post;

		$model_id = get_queried_object_id();
		$model_name = get_the_title( $model_id );
		$model_excerpt = get_the_excerpt( $model_id );
		$model_slug = $post->post_name;

		$model_district = '';
		$girl_location = wp_get_post_terms( $model_id, 'girl_cat' );
		foreach ( $girl_location as $girl_location_i ) {
			if( $girl_location_i->parent > 0 ){
				$model_district .= $girl_location_i->name;
			}
		}

		$txt = str_replace( '[domain]',            $_SERVER['SERVER_NAME'], $txt );
		$txt = str_replace( '[page_name]',         $model_name, $txt );
		$txt = str_replace( '[page_description]',  $model_excerpt, $txt );
		$txt = str_replace( '[slug]',              $model_slug, $txt );
		return $txt;
	}

?>