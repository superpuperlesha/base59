<?php
/**
 * Plugin Name: Images To WEBP
 * Plugin URI: https://stackoverflow.com/a/67234000
 * Description: After uploading an image it will be converted to WebP format using the GD image engine. <a target="_blank" href="https://developer.wordpress.org/reference/classes/wp_image_editor_gd/">WP GD Image Engine</a> If the file is deleted form the Media Library the created WebP conversions will also be deleted.
 * Version: 1.0.0
 * Requires at least: 5.5
 * Requires PHP: 7.2
 * Author: lowtechsun
 * Author URI: https://stackoverflow.com/users/1010918/lowtechsun
 * License: GPL v2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 */

//=================================================
// Security: Abort if this file is called directly
//=================================================
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

function debug( $info ) {
	$message = null;

	if ( is_string( $info ) || is_int( $info ) || is_float( $info ) ) {
		$message = $info;
	} else {
		$message = var_export( $info, true );
	}

	/*if ( $fh = fopen( ABSPATH . '/gdwebpconvert.log', 'a' ) ) {
		fputs( $fh, date( 'Y-m-d H:i:s' ) . " $message\n" );
		fclose( $fh );
	}*/
}

add_filter( 'wp_generate_attachment_metadata', 'gd_webp_converter', 10, 2 );

function gd_webp_converter( $metadata, $attachment_id ) {

	$gd_webp_converter = new GDWebPConverter( $attachment_id );
	$gd_webp_converter->check_file_exists( $attachment_id );
	$gd_webp_converter->check_mime_type();
	$gd_webp_converter->create_array_of_sizes_to_be_converted( $metadata );
	$gd_webp_converter->convert_array_of_sizes();

	return $metadata;
}

class GDWebPConverter {

	private $file_path;
	private $file_dirname;
	private $file_ext;
	private $file_name_no_ext;

	private $array_of_sizes_to_be_converted = array();
	private $array_of_sizes_to_be_deleted   = array();

	public function __construct( $attachment_id ) {

		$this->file_path = get_attached_file( $attachment_id );
		debug( $this->file_path );

		// https://stackoverflow.com/questions/2183486/php-get-file-name-without-file-extension/19040276
		$this->file_dirname = pathinfo( $this->file_path, PATHINFO_DIRNAME );
		debug( $this->file_dirname );

		$this->file_ext = strtolower( pathinfo( $this->file_path, PATHINFO_EXTENSION ) );
		debug( $this->file_ext );

		$this->file_name_no_ext = pathinfo( $this->file_path, PATHINFO_FILENAME );
		debug( $this->file_name_no_ext );
	}

	public function check_file_exists( $attachment_id ) {

		$file = get_attached_file( $attachment_id );

		if ( ! file_exists( $file ) ) {
			$message = 'The uploaded file does not exist on the server. Encoding not possible.';
			debug( $message );
			throw new Exception( 'The uploaded file does exist on the server. Encoding not possible.', 1 );
		}

	}

	public function check_mime_type() {

		// https://www.php.net/manual/en/function.finfo-file.php
		$finfo = finfo_open( FILEINFO_MIME_TYPE );

		$this->file_mime_type = finfo_file( $finfo, $this->file_path );

		finfo_close( $finfo );
		// debug( $this->file_mime_type );

		// https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
		$this->allowed_mime_type = array( 'image/jpeg', 'image/png' );

		if ( ! in_array( $this->file_mime_type, $this->allowed_mime_type, true ) ) {

			$message = 'MIME type of file not supported';
			// debug( $message );
			throw new Exception( 'MIME type of file not supported', 1 );

		}
	}

	public function create_array_of_sizes_to_be_converted( $metadata ) {

		// push original file to the array
		array_push( $this->array_of_sizes_to_be_converted, $this->file_path );
		// debug( $this->array_of_sizes_to_be_converted );

		// push all created sizes of the file to the array
		foreach ( $metadata['sizes'] as $value ) {
			// debug( $value['file'] );
			array_push( $this->array_of_sizes_to_be_converted, $this->file_dirname . '/' . $value['file'] );
		}
		// // debug( $this->array_of_sizes_to_be_converted );
	}

	public function convert_array_of_sizes() {
		$quality = 75;
		debug( $this->array_of_sizes_to_be_converted );

		switch ( $this->file_ext ) {
			case 'jpeg':
			case 'jpg':
				foreach ( $this->array_of_sizes_to_be_converted as $key => $value ) {
					$image = imagecreatefromjpeg( $value );

					if ( 0 === $key ) {
						imagewebp( $image, $this->file_dirname . '/' . $this->file_name_no_ext . '.' . $this->file_ext . '.webp', $quality );
					} else {
						$current_size = getimagesize( $value );
						// debug( $current_size );
						$name_clear = $this->file_dirname . '/' . $this->file_name_no_ext . '-' . $current_size[0] . 'x' . $current_size[1] . '.' . $this->file_ext . '.webp';
						$name_clear = str_replace( '-scaled-', '-', $name_clear  );
						imagewebp( $image, $name_clear, $quality );
					}

					imagedestroy( $image );
				}
				break;

			case 'png':
				foreach ( $this->array_of_sizes_to_be_converted as $key => $value ) {
					$image = imagecreatefrompng( $value );
					imagepalettetotruecolor( $image );
					imagealphablending( $image, true );
					imagesavealpha( $image, true );

					if ( 0 === $key ) {
						imagewebp( $image, $this->file_dirname . '/' . $this->file_name_no_ext . '.' . $this->file_ext . '.webp', $quality );
					} else {
						$current_size = getimagesize( $value );
						// debug( $current_size );
						$name_clear = $this->file_dirname . '/' . $this->file_name_no_ext . '-' . $current_size[0] . 'x' . $current_size[1] . '.' . $this->file_ext . '.webp';
						$name_clear = str_replace( '-scaled-', '-', $name_clear  );
						imagewebp( $image, $name_clear, $quality );
					}

					imagedestroy( $image );
				}
				break;

			// animated GIF to WebP not supported by GD - imagecreatefromgif
			// case 'gif':
			//  foreach ( $this->array_of_sizes_to_be_converted as $key => $value ) {
			//      $image = imagecreatefromgif( $value );

			//      if ( 0 === $key ) {
			//          imagewebp( $image, $this->file_dirname . '/' . $this->file_name_no_ext . '.webp', $quality );
			//      } else {
			//          $current_size = getimagesize( $value );
			//          // debug( $current_size );
			//          imagewebp( $image, $this->file_dirname . '/' . $this->file_name_no_ext . '-' . $current_size[0] . 'x' . $current_size[1] . '.webp', 80 );
			//      }

			//      imagedestroy( $image );
			//  }
			//  break;

			default:
				return false;
		}

	}

	public function create_array_of_sizes_to_be_deleted( $attachment_id ) {

		// debug( $attachment_id );

		$this->attachment_metadata_of_file_to_be_deleted = wp_get_attachment_metadata( $attachment_id );
		// debug( $this->attachment_metadata_of_file_to_be_deleted );

		// push original file to the array
		array_push( $this->array_of_sizes_to_be_deleted, $this->file_dirname . '/' . $this->file_name_no_ext . '.webp' );
		// debug( $this->array_of_sizes_to_be_converted );

		// push all created sizes of the file to the array
		foreach ( $this->attachment_metadata_of_file_to_be_deleted['sizes'] as $value ) {

			// debug( $value );

			$this->value_file_name_no_ext = pathinfo( $value['file'], PATHINFO_FILENAME );
			// debug( $this->value_file_name_no_ext );

			array_push( $this->array_of_sizes_to_be_deleted, $this->file_dirname . '/' . $this->value_file_name_no_ext . '.webp' );
		}
		// debug( $this->array_of_sizes_to_be_deleted );
	}

	public function delete_array_of_sizes() {

		debug( $this->array_of_sizes_to_be_deleted );

		foreach ( $this->array_of_sizes_to_be_deleted as $key => $value ) {

			// debug( $value );
			unlink( $value );

		}
	}

}

add_action( 'delete_attachment', 'delete_webp_conversions', 10 );

function delete_webp_conversions( $attachment_id ) {

	$delete_webp_conversions = new GDWebPConverter( $attachment_id );
	$delete_webp_conversions->create_array_of_sizes_to_be_deleted( $attachment_id );
	$delete_webp_conversions->delete_array_of_sizes();

}
