<?php

/*
 * Plugin styles && scripts
 */
function woo_coinmap_scripts() {
	$woo_coinmap_googleapi = (string)get_option('woo_coinmap_googleapi', true );

	wp_register_style('woo_coinmap_css', plugins_url('/front/woo_coinmap.css', __FILE__ ) );
	wp_enqueue_style('woo_coinmap_css');

	if( function_exists('is_checkout') && is_checkout() ){
		wp_register_script( 'woo_coinmap_map_2', plugins_url('/front/woo_coinmap_map_2.js', __FILE__ ), array(), null, true );
		wp_enqueue_script('woo_coinmap_map_2');
	}else{
		wp_register_script( 'woo_coinmap_map_1', plugins_url('/front/woo_coinmap_map_1.js', __FILE__ ), array(), null, true );
		wp_enqueue_script('woo_coinmap_map_1');
	}

	wp_register_script( 'woo_coinmap_googleapi', 'https://maps.googleapis.com/maps/api/js?key=' . $woo_coinmap_googleapi . '&libraries=places', array(), null, true);
	wp_enqueue_script('woo_coinmap_googleapi');
	//&callback=woo_coinmap_initmap
}
add_action( 'wp_enqueue_scripts','woo_coinmap_scripts');

?>