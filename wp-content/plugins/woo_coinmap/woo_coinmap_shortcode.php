<?php
/*
 * Plugin ShortCode for output on frontend v1
 */
function woo_coinmap_shortcode_1() {
	$woo_coinmap_zip    = (string)get_option('woo_coinmap_zip', '10007' );
	$woo_coinmap_radius = (int)get_option('woo_coinmap_radius', '25' );

	$res = '<div class="woo_coinmap">
				<table class="woo_coinmap_info">
					<tr>
						<td>' . __('Address', '' ) . '</td>
						<td><input type="text"   id="woo_coinmap_address"></td>
					</tr>
					<tr>
						<td>' . __('Radius, km', '' ) . '</td>
						<td><input type="number" id="woo_coinmap_radius" value="'.$woo_coinmap_radius.'" min="1" max="100"></td>
					</tr>
					<tr>
						<td><input type="hidden"   id="woo_coinmap_address_tat"></td>
						<td><input type="hidden"   id="woo_coinmap_address_lng"></td>
					</tr>
				</table>
				<div id            = "woo_coinmap_map" 
					 class         = "woo_coinmap_map" 
					 data-zip_def  = "' . esc_attr( $woo_coinmap_zip ) . '"
					 data-wp_ajax  = "' . esc_attr( admin_url('admin-ajax.php') ) . '"
				></div>
				<table id="woo_coinmap_atms_listall" class="woo_coinmap_atms_listall"></table>
			</div>';
	return $res;
}
add_shortcode('woo_coinmap_1', 'woo_coinmap_shortcode_1');


/*
 * Plugin ShortCode for output on frontend v2
 */
function woo_coinmap_shortcode_2() {
	$woo_coinmap_zip    = (string)get_option('woo_coinmap_zip', '10007' );
	$woo_coinmap_radius = (int)get_option('woo_coinmap_radius', '25' );

	$res = '<div class="woo_coinmap woo_coinmap_v2">
				<table class="woo_coinmap_info">
					<tr>
						<td>' . __('Radius, km', '' ) . '</td>
						<td><input type="number" id="woo_coinmap_radius_2" value="'.$woo_coinmap_radius.'" min="1" max="100"></td>
					</tr>
					<tr>
						<td><input type="hidden"   id="woo_coinmap_address_tat"></td>
						<td><input type="hidden"   id="woo_coinmap_address_lng"></td>
					</tr>
				</table>
				<div id="woo_coinmap_addrfull_2" class="woo_coinmap_addrfull_2"></div>
				<div id            = "woo_coinmap_map_2" 
					 class         = "woo_coinmap_map" 
					 data-zip_def  = "' . esc_attr( $woo_coinmap_zip ) . '"
					 data-wp_ajax  = "' . esc_attr( admin_url('admin-ajax.php') ) . '"
				></div>
				<table id="woo_coinmap_atms_listall" class="woo_coinmap_atms_listall"></table>
			</div>';
	return $res;
}
add_shortcode('woo_coinmap_2', 'woo_coinmap_shortcode_2');

?>