function woo_coinmap_initmap(){
	var woo_coinmap_address      = document.querySelector('#woo_coinmap_address');
	var woo_coinmap_map          = document.querySelector('#woo_coinmap_map');
	var woo_coinmap_radius       = document.querySelector('#woo_coinmap_radius');
	var woo_coinmap_address_lat  = document.querySelector('#woo_coinmap_address_tat');
	var woo_coinmap_address_lng  = document.querySelector('#woo_coinmap_address_lng');
	var woo_coinmap_atms_listall = document.querySelector('#woo_coinmap_atms_listall');
	var markersArray       = [];

	if( woo_coinmap_address === null || woo_coinmap_map === null || woo_coinmap_radius === null ){
		return;
	}


	// map init
	var woo_coinmap_map_style = [{"elementType":"geometry","stylers":[{"color":"#ebe3cd"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#523735"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f1e6"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#c9b2a6"}]},{"featureType":"administrative.land_parcel","elementType":"geometry.stroke","stylers":[{"color":"#dcd2be"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#ae9e90"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#dfd2ae"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#dfd2ae"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#93817c"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#a5b076"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#447530"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#f5f1e6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#fdfcf8"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#f8c967"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#e9bc62"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry","stylers":[{"color":"#e98d58"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#db8555"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#806b63"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#dfd2ae"}]},{"featureType":"transit.line","elementType":"labels.text.fill","stylers":[{"color":"#8f7d77"}]},{"featureType":"transit.line","elementType":"labels.text.stroke","stylers":[{"color":"#ebe3cd"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#dfd2ae"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#b9d3c2"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#92998d"}]}];
	var map = new google.maps.Map( woo_coinmap_map, {
		center: { lat: 0.0, lng: 0.0 },
		zoom: 16,
		styles: woo_coinmap_map_style,
	});
	var bounds = new google.maps.LatLngBounds();


	var def_latitude         = 50.0;
	var def_longitude        = 50.0;
	var geocoder              = '';
	var zip_def               = woo_coinmap_map.getAttribute('data-zip_def');
	woo_coinmap_load_defaults( zip_def );

	// start Autocomplete
	var options = {
		fields: ['address_components', 'geometry'],
		types: ['address'],
	};
	var autocomplete = new google.maps.places.Autocomplete( woo_coinmap_address, options );


	// Change Autocomplete
	autocomplete.addListener('place_changed', () => {
		const place = autocomplete.getPlace();
		let lat = place.geometry.location.lat();
		let lng = place.geometry.location.lng();
		woo_coinmap_address_lat.value = lat;
		woo_coinmap_address_lng.value = lng;

		woo_coinmap_start_search();
	});
	// Change Radius
	woo_coinmap_radius.addEventListener('change', (e) => {
		woo_coinmap_start_search();
	});
	woo_coinmap_radius.addEventListener('keyup', (e) => {
		woo_coinmap_start_search();
	});


	// clear list ATMs
	function woo_coinmap_atms_clear(){
		// remove markers
		if ( markersArray ) {
			for ( i in markersArray ) {
				markersArray[i].setMap( null );
			}
		}

		// clear ATM list
		woo_coinmap_atms_listall.innerHTML = '';
	}


	// load defaults
	function woo_coinmap_load_defaults( zip_def_set ){
		def_latitude  = 50.0;
		def_longitude = 50.0;
		zip_def       = zip_def_set;
		geocoder      = new google.maps.Geocoder();

		geocoder.geocode({ 'address': 'zipcode ' + zip_def }, function (results, status) {
			if ( status === google.maps.GeocoderStatus.OK ) {
				def_latitude  = results[0].geometry.location.lat();
				def_longitude = results[0].geometry.location.lng();
				woo_coinmap_address_lat.value = def_latitude;
				woo_coinmap_address_lng.value = def_longitude;
			} else {
				alert("Request failed.")
			}

			// set map center default
			map.setCenter( new google.maps.LatLng( def_latitude, def_longitude ) );

			woo_coinmap_load_data_map( def_latitude, def_longitude, woo_coinmap_radius.value );
		});

		woo_coinmap_address_lat.value = def_latitude;
		woo_coinmap_address_lng.value = def_longitude;
	}

	function woo_coinmap_start_search(){
		map.setCenter( new google.maps.LatLng( woo_coinmap_address_lat.value, woo_coinmap_address_lng.value ) );
		woo_coinmap_load_data_map( woo_coinmap_address_lat.value, woo_coinmap_address_lng.value, woo_coinmap_radius.value );
	}


	function woo_coinmap_load_data_map( lat, lon, radius ){
		let wp_ajax = woo_coinmap_map.getAttribute('data-wp_ajax');
		var data = new FormData();
		data.append( 'action', 'woo_coinmap_search' );
		data.append( 'lat',    lat );
		data.append( 'lng',    lon );
		data.append( 'radius', radius );

		fetch( wp_ajax, {
			method: 'POST',
			body:   data
		})
			.then((response) => response.json())
			.then((data) => {
				woo_coinmap_markers_data_map( data );
			})
			.catch((error) => {
				console.error( error );
			});
	}

	function woo_coinmap_markers_data_map( data ){
		//console.log( data );
		woo_coinmap_atms_clear();

		bounds = new google.maps.LatLngBounds();
		let def_lat = parseFloat( woo_coinmap_address_lat.value );
		let def_lng = parseFloat( woo_coinmap_address_lng.value );
		var icon = {
			path: "M-20,0a20,20 0 1,0 40,0a20,20 0 1,0 -40,0",
			fillColor: '#FF0000',
			fillOpacity: .6,
			anchor: new google.maps.Point(0,0),
			strokeWeight: 0,
			scale: 1
		};

		var marker = new google.maps.Marker({
			position: { lat: def_lat, lng: def_lng },
			map,
			title: 'Center',
			icon: icon,
		});
		markersArray.push( marker );

		for (let i = 0; i < data.length; i++) {
			const beach = data[i];
			let lat = parseFloat( beach['lat'] );
			let lng = parseFloat( beach['lon'] );

			let title = beach['name'] + ' \n' + beach['addr'] + '\n' + beach['distance'] + ' km';

			var marker = new google.maps.Marker({
				position: { lat: lat, lng: lng },
				map,
				title: title,
			});

			bounds.extend( marker.getPosition() );
			markersArray.push( marker );

			woo_coinmap_atms_listall.innerHTML += '<td class="woo_coinmap_atms_listall_i_n">' + beach['name'] + '</td>' +
												  '<td class="woo_coinmap_atms_listall_i_d">(' + beach['distance'] + ' km)</td>';
		}

		if( data.length > 0 ){
			map.fitBounds( bounds );
		}
	}
}
window.addEventListener("DOMContentLoaded", (event) => {
	woo_coinmap_initmap();
});

/*let radius   = woo_coinmap_map.getAttribute('data-radius');
let lat_min = def_latitude;
let lat_max  = def_latitude + radius;
let lon_nin = def_longitude;
let lon_max  = def_longitude + radius;
let url      = 'https://coinmap.org/api/v1/venues/?lat1='+lat_min+'&lat2='+lat_max+'&lon1='+lon_nin+'&lon2='+lon_max;
console.info( url );
fetch( url )
	.then( res => res.json() )
	.then( (json) => {
		console.info( json );
	})
	.catch( (err) => {
		console.error( err );
	});*/