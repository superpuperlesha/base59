function woo_coinmap_initmap_2(){
	var woo_coinmap_addrfull_2   = document.querySelector('#woo_coinmap_addrfull_2');
	var woo_coinmap_map          = document.querySelector('#woo_coinmap_map_2');
	var woo_coinmap_radius       = document.querySelector('#woo_coinmap_radius_2');
	var woo_coinmap_address_lat  = document.querySelector('#woo_coinmap_address_tat');
	var woo_coinmap_address_lng  = document.querySelector('#woo_coinmap_address_lng');
	var billing_country          = document.querySelector('#billing_country');
	var billing_city             = document.querySelector('#billing_city');
	var billing_address_1        = document.querySelector('#billing_address_1');
	var woo_coinmap_atms_listall = document.querySelector('#woo_coinmap_atms_listall');
	var markersArray       = [];

	if( woo_coinmap_map === null || woo_coinmap_radius === null ){
		return;
	}


	// map init
	var woo_coinmap_map_style = [{"elementType":"geometry","stylers":[{"color":"#ebe3cd"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#523735"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f1e6"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#c9b2a6"}]},{"featureType":"administrative.land_parcel","elementType":"geometry.stroke","stylers":[{"color":"#dcd2be"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#ae9e90"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#dfd2ae"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#dfd2ae"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#93817c"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#a5b076"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#447530"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#f5f1e6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#fdfcf8"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#f8c967"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#e9bc62"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry","stylers":[{"color":"#e98d58"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#db8555"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#806b63"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#dfd2ae"}]},{"featureType":"transit.line","elementType":"labels.text.fill","stylers":[{"color":"#8f7d77"}]},{"featureType":"transit.line","elementType":"labels.text.stroke","stylers":[{"color":"#ebe3cd"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#dfd2ae"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#b9d3c2"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#92998d"}]}];
	var map = new google.maps.Map( woo_coinmap_map, {
		center: { lat: 0.0, lng: 0.0 },
		zoom: 16,
		styles: woo_coinmap_map_style,
	});
	var bounds = new google.maps.LatLngBounds();


	var def_latitude         = 50.0;
	var def_longitude        = 50.0;
	var geocoder              = '';
	var woo_coinmap_wooaddressfull = billing_country.value + ', ' + billing_city.value + ', ' + billing_address_1.value;

	woo_coinmap_load_defaults( woo_coinmap_wooaddressfull );


	// Change Radius
	woo_coinmap_radius.addEventListener('change', (e) => {
		woo_coinmap_start_search();
	});
	woo_coinmap_radius.addEventListener('keyup', (e) => {
		woo_coinmap_start_search();
	});


	// change woo checkout address
	if( billing_country !== null && billing_city !== null && billing_address_1 !== null ){
		billing_country.addEventListener('change', (e) => {
			woo_coinmap_woo_checkout_search();
		});
		billing_city.addEventListener('keyup', (e) => {
			woo_coinmap_woo_checkout_search();
		});
		billing_address_1.addEventListener('keyup', (e) => {
			woo_coinmap_woo_checkout_search();
		});
	}
	// start WOO map search
	function woo_coinmap_woo_checkout_search(){
		//console.log( 'FULL:' + billing_country.value +' - '+ billing_city.value + ' - ' + billing_address_1.value );
		if( billing_country.value.length && billing_city.value.length && billing_address_1.value.length ) {
			woo_coinmap_wooaddressfull = billing_country.value + ', ' + billing_city.value + ', ' + billing_address_1.value;
			woo_coinmap_load_defaults( woo_coinmap_wooaddressfull );
		}
	}

	// clear list ATMs
	function woo_coinmap_atms_clear(){
		// remove markers
		if ( markersArray ) {
			for ( i in markersArray ) {
				markersArray[i].setMap( null );
			}
		}

		// clear ATM list
		woo_coinmap_atms_listall.innerHTML = '';
	}


	// load defaults
	function woo_coinmap_load_defaults( addr_set ){
		def_latitude               = 50.0;
		def_longitude              = 50.0;
		woo_coinmap_wooaddressfull = addr_set;
		woo_coinmap_addrfull_2.innerHTML = woo_coinmap_wooaddressfull;
		geocoder                   = new google.maps.Geocoder();

		geocoder.geocode( { 'address': woo_coinmap_wooaddressfull }, function (results, status) {
			if ( status === google.maps.GeocoderStatus.OK ) {
				def_latitude  = results[0].geometry.location.lat();
				def_longitude = results[0].geometry.location.lng();
				woo_coinmap_address_lat.value = def_latitude;
				woo_coinmap_address_lng.value = def_longitude;
			} else {
				alert("Request failed.")
			}

			// set map center default
			map.setCenter( new google.maps.LatLng( def_latitude, def_longitude ) );

			woo_coinmap_load_data_map( def_latitude, def_longitude, woo_coinmap_radius.value );
		});

		woo_coinmap_address_lat.value = def_latitude;
		woo_coinmap_address_lng.value = def_longitude;
	}

	function woo_coinmap_start_search(){
		map.setCenter( new google.maps.LatLng( woo_coinmap_address_lat.value, woo_coinmap_address_lng.value ) );
		woo_coinmap_load_data_map( woo_coinmap_address_lat.value, woo_coinmap_address_lng.value, woo_coinmap_radius.value );
	}


	function woo_coinmap_load_data_map( lat, lon, radius ){
		let wp_ajax = woo_coinmap_map.getAttribute('data-wp_ajax');
		var data = new FormData();
		data.append( 'action', 'woo_coinmap_search' );
		data.append( 'lat',    lat );
		data.append( 'lng',    lon );
		data.append( 'radius', radius );

		fetch( wp_ajax, {
			method: 'POST',
			body:   data
		})
			.then((response) => response.json())
			.then((data) => {
				woo_coinmap_markers_data_map( data );
			})
			.catch((error) => {
				console.error( error );
			});
	}

	function woo_coinmap_markers_data_map( data ){
		//console.log( data );
		woo_coinmap_atms_clear();

		bounds = new google.maps.LatLngBounds();
		let def_lat = parseFloat( woo_coinmap_address_lat.value );
		let def_lng = parseFloat( woo_coinmap_address_lng.value );
		var icon = {
			path: "M-20,0a20,20 0 1,0 40,0a20,20 0 1,0 -40,0",
			fillColor: '#FF0000',
			fillOpacity: .6,
			anchor: new google.maps.Point(0,0),
			strokeWeight: 0,
			scale: 1
		};

		var marker = new google.maps.Marker({
			position: { lat: def_lat, lng: def_lng },
			map,
			title: woo_coinmap_wooaddressfull,
			icon: icon,
		});
		markersArray.push( marker );

		for (let i = 0; i < data.length; i++) {
			const beach = data[i];
			let lat = parseFloat( beach['lat'] );
			let lng = parseFloat( beach['lon'] );

			let title = beach['name'] + ' \n' + beach['addr'] + '\n' + beach['distance'] + ' km';

			var marker = new google.maps.Marker({
				position: { lat: lat, lng: lng },
				map,
				title: title,
			});

			bounds.extend( marker.getPosition() );
			markersArray.push( marker );

			woo_coinmap_atms_listall.innerHTML += '<td class="woo_coinmap_atms_listall_i_n">' + beach['name'] + '</td>' +
				'<td class="woo_coinmap_atms_listall_i_d">(' + beach['distance'] + ' km)</td>';
		}

		if( data.length > 0 ){
			map.fitBounds( bounds );
		}
	}
}
window.addEventListener("DOMContentLoaded", (event) => {
	woo_coinmap_initmap_2();
});
