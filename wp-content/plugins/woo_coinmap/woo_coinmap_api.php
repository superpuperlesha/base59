<?php
/*
 * Search for ATMs on the map.
 * API https://coinmap.org/api/#api-Venue-GetVenue
 */
function woo_coinmap_search( $lat=0, $lon=0, $radius=0 ){
	$res         = array();
	$lat         = (float)$lat;
	$lon         = (float)$lon;
	$radius      = (int)$radius;
	$radius      = $radius ? $radius : (int)get_option('woo_coinmap_radius' );

	$radius_coef = $radius * 1000 * 0.0000089;
	$lat_min = $lat - $radius_coef / 2;
	$lat_max = $lat + $radius_coef / 2;
	$lon_min = $lon - $radius_coef / 2;
	$lon_max = $lon + $radius_coef / 2;

	/*$radius = $radius * 1000;
	$earth  = 6378.137;
	// lat
    $m_lat   = (1 / ((2 * pi() / 360) * $earth)) / 1000;  //1 meter in degree
	$lat_min = $lat - ( $radius / 2 * $m_lat );
	$lat_max = $lat + ( $radius / 2 * $m_lat );
	// lon
	$m_lon   = (1 / ((2 * pi() / 360) * $earth)) / 1000;  //1 meter in degree
	$lon_min = $lon - ( $radius / 2 * $m_lon ) / cos($lat * ( pi() / 180));
	$lon_max = $lon + ( $radius / 2 * $m_lon ) / cos($lat * ( pi() / 180));*/

	$array = @file_get_contents( 'https://coinmap.org/api/v1/venues/?lat1='.$lat_min.'&lat2='.$lat_max.'&lon1='.$lon_min.'&lon2='.$lon_max );
	if ( $array !== false ) {
		$array = json_decode( $array, true );
		if( isset( $array['venues'] ) ){
			foreach( $array['venues'] as $venues_i ){
				$lat2     = (float)$venues_i['lat'];
				$lon2     = (float)$venues_i['lon'];
				$distance = round( acos(sin( $lat ) * sin( $lat2 ) + cos( $lat ) * cos( $lat2 ) * cos($lon2 - $lon ) ) * 6371 ) / 10;

				$res[] = array( 'lat'      => $venues_i['lat'],
				                'lon'      => $venues_i['lon'],
				                'name'     => $venues_i['name'],
				                'addr'     => $venues_i['geolocation_degrees'],
								'distance' => $distance,
				);
			}

			// sort array
			foreach( $res as $key=>$row ){
				$alat[$key]      = $row['lat'];
				$alon[$key]      = $row['lon'];
				$aname[$key]     = $row['name'];
				$aaddr[$key]     = $row['addr'];
				$adistance[$key] = $row['distance'];
			}
			$alat      = array_column( $res, 'lat' );
			$alon      = array_column( $res, 'lon' );
			$aname     = array_column( $res, 'name' );
			$aaddr     = array_column( $res, 'addr' );
			$adistance = array_column( $res, 'distance' );
			array_multisort($adistance, SORT_ASC, $aname, SORT_ASC, $aaddr, SORT_ASC, $alat, SORT_ASC, $alon, SORT_ASC, $res );

		}
	}

	return $res;
}


/*
 * Ajax request to search
 */
function ajax_woo_coinmap_search() {
	$_POST['lat']    = $_POST['lat']    ?? 0;
	$_POST['lng']    = $_POST['lng']    ?? 0;
	$_POST['radius'] = $_POST['radius'] ?? 0;
	$_POST['lat']    = (float)$_POST['lat'];
	$_POST['lng']    = (float)$_POST['lng'];
	$_POST['radius'] = (int)$_POST['radius'];

	echo json_encode( woo_coinmap_search( $_POST['lat'], $_POST['lng'], $_POST['radius'] ) );
	exit();
}
add_action( 'wp_ajax_woo_coinmap_search',        'ajax_woo_coinmap_search' );
add_action( 'wp_ajax_nopriv_woo_coinmap_search', 'ajax_woo_coinmap_search' );
