<?php
/**
 * Plugin Name: WOO CoinMap
 * Plugin URI: https://woo_coinmap/
 * Description: Search for ATMs on the map.
 * Version: 1.0.0
 * Author: Denis Sidorkin (densid)
 * Author URI: https://densid.com/
 * Text Domain: woo_coinmap
 * License: GPLv2
 * Released under the GNU General Public License (GPL)
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */


require_once dirname( __FILE__ ).'/admin_menu.php';
require_once dirname( __FILE__ ).'/woo_coinmap_api.php';
require_once dirname( __FILE__ ).'/woo_coinmap_front.php';
require_once dirname( __FILE__ ).'/woo_coinmap_shortcode.php';


/*
 * Add Plugin link to Settings
 */
add_filter( 'plugin_action_links_woo_coinmap/woo_coinmap.php', 'nc_settings_link' );
function nc_settings_link( $links ) {
	$url = esc_url( add_query_arg(
		'page',
		'woo_coinmap',
		get_admin_url() . 'options-general.php'
	) );

	$settings_link = '<a href=' . $url . '>' . __( 'Settings', 'woo_coinmap' ) . '</a>';

	array_push( $links, $settings_link );
	return $links;
}

