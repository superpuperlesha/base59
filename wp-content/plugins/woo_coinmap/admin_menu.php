<?php

add_action( 'admin_menu', 'woo_coinmap_admin_menu' );
function woo_coinmap_admin_menu() {
	add_submenu_page(
		'options-general.php',
		__(  'WOO coinmap', 'woo_coinmap' ),
		__(  'WOO coinmap', 'woo_coinmap' ),
		 'manage_options',
		'woo_coinmap',
		  'woo_coinmap_page'
	);

	function woo_coinmap_page() {
		$admin_url   = get_admin_url() . '/options-general.php';

		$msg='';
		if( isset( $_POST['woo_coinmap_saveoption'] ) ) {
			$_POST['woo_coinmap_googleapi'] = $_POST['woo_coinmap_googleapi'] ?? '';
			$_POST['woo_coinmap_zip']       = $_POST['woo_coinmap_zip']       ?? '';
			$_POST['woo_coinmap_radius']    = $_POST['woo_coinmap_radius']    ?? 25;

			update_option('woo_coinmap_googleapi', (string)$_POST['woo_coinmap_googleapi'] );
			update_option('woo_coinmap_zip',       (string)$_POST['woo_coinmap_zip'] );
			update_option('woo_coinmap_radius',    (int)$_POST['woo_coinmap_radius'] );

			$msg = '<div class="updated notice is-dismissible">
						<p>'. __( ' Settings saved ', 'ceoposts' ) . '</p>
					</div>';
		}
		$woo_coinmap_googleapi = (string)get_option('woo_coinmap_googleapi', true );
		$woo_coinmap_zip       = (string)get_option('woo_coinmap_zip',       true );
		$woo_coinmap_radius    = (int)get_option('woo_coinmap_radius',       true ); ?>

		<div class="wrap wp-core-ui">
			<div class="wrap">
				<h1 class="wp-heading-inline">
					<?php _e( 'WOO coinmap settings', 'woo_coinmap' ) ?>
				</h1>
			</div>
			<?php echo $msg ?>
			<br/>
			<b><?php echo __( 'Server time:', 'woo_coinmap' ) . '</b> ' . date( 'd.m.Y H:i:s', current_time( 'timestamp' ) ) ?>
			<br/>
			<b><?php echo __( 'Short code simple:', 'woo_coinmap' ) . '</b> [woo_coinmap_1]' ?>
			<br/>
			<b><?php echo __( 'Short code checkout:', 'woo_coinmap' ) . '</b> [woo_coinmap_2]' ?>
			<br/>
			<form action="<?php echo $admin_url ?>?page=woo_coinmap" method="post">
				<table class="form-table">
					<tr>
						<td>
							<label for="woo_coinmap_googleapi">
								<b><?php _e( 'GOOGLE API key (maps)', 'woo_coinmap' ) ?></b>
							</label>
						</td>
						<td>
							<input type="text"   id="woo_coinmap_googleapi" name="woo_coinmap_googleapi" value="<?php echo esc_attr( $woo_coinmap_googleapi ) ?>">
						</td>
					</tr>
					<tr>
						<td>
							<label for="woo_coinmap_zip">
								<b><?php _e( 'Search ZIP default', 'woo_coinmap' ) ?></b>
							</label>
						</td>
						<td>
							<input type="text"   id="woo_coinmap_zip" name="woo_coinmap_zip"    value="<?php echo esc_attr( $woo_coinmap_zip ) ?>">
						</td>
					</tr>
					<tr>
						<td>
							<label for="woo_coinmap_radius">
								<b><?php _e( 'Search radius default', 'woo_coinmap' ) ?></b>
							</label>
						</td>
						<td>
							<input type="number" id="woo_coinmap_radius" name="woo_coinmap_radius"    value="<?php echo esc_attr( $woo_coinmap_radius ) ?>" min=1>
						</td>
					</tr>
				</table>
				<p class="submit">
					<input type="hidden"   name="woo_coinmap_saveoption">
					<input type="submit" value="<?php _e( 'Save settings', 'woo_coinmap' ) ?>" class="button button-primary">
				</p>
			</form>

		</div><?php
	}
}
