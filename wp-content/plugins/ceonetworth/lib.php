<?php


function dd( $arr ){
	echo'<pre>';
	print_r( $arr );
	echo'</pre>';
}


/*
 * Custom Fields
 */
$kaggle_fields = @unserialize( get_option('kaggle_fields', true ) );
$kaggle_fields = is_array( $kaggle_fields ) ? $kaggle_fields : array();
array_push( $kaggle_fields, 'post_title','post_content','post_thumbnail' );
define( 'kimp_meta_fields_post', $kaggle_fields );


/*
 * Convert csv to array
 */
function get_csv_format_from_text( $url ){
	$msg        = array();
	$file       = @file_get_contents( $url );
	$path_parts = pathinfo( $url );
	$tmp_file   = __DIR__ . '/tmp/' . time() . '_' . rand(0,9) . rand(0,9) . rand(0,9) . '.' . $path_parts['extension'];

	if( $file === false ){
		$msg = '<div class="updated notice is-dismissible">
					<p>' .  __( 'File not opened!', 'ceonetworth' ) . ' ' . $url . '</p>
				</div>';
	}

	if ( !$msg && !$fp = fopen( $tmp_file, 'w' )) {
		$msg = '<div class="updated notice is-dismissible">
					<p>' .  __( 'Cannot open file!', 'ceonetworth' ) . ' ' . $tmp_file . '</p>
				</div>';
	}

	if ( !$msg && fwrite( $fp, $file ) === FALSE ) {
		$msg = '<div class="updated notice is-dismissible">
					<p>' .  __( 'Cannot write to file!', 'ceonetworth' ) . ' ' . $tmp_file . '</p>
				</div>';
	}

	if( !$msg ){
		fclose( $fp );
	}

	if( !$msg ){
		$file = @fopen( $tmp_file, 'r' );
		while( !feof( $file ) ) {
			$line  = fgets( $file );
			$msg[] = str_getcsv( $line, ',' );
		}
		fclose( $file );
	}

	return $msg;
}


/*
 * Convert xls to array
 */
function get_xls_format_from_text( $url ){
	// https://blog.programster.org/phpspreadsheet-read-excel-file-to-array
	$msg        = array();
	$file       = @file_get_contents( $url );
	$path_parts = pathinfo( $url );
	$tmp_file   = __DIR__ . '/tmp/' . time() . '_' . rand(0,9) . rand(0,9) . rand(0,9) . '.' . $path_parts['extension'];

	if( $file === false ){
		$msg = '<div class="updated notice is-dismissible">
					<p>' .  __( 'File not opened!', 'ceonetworth' ) . ' ' . $url . '</p>
				</div>';
	}

	if ( !$msg && !$fp = fopen( $tmp_file, 'w' )) {
		$msg = '<div class="updated notice is-dismissible">
					<p>' .  __( 'Cannot open file!', 'ceonetworth' ) . ' ' . $tmp_file . '</p>
				</div>';
	}

	if ( !$msg && fwrite( $fp, $file ) === FALSE ) {
		$msg = '<div class="updated notice is-dismissible">
					<p>' .  __( 'Cannot write to file!', 'ceonetworth' ) . ' ' . $tmp_file . '</p>
				</div>';
	}

	if( !$msg ){
		fclose( $fp );
	}

	if( !$msg ){
		require_once( __DIR__ . '/lib/xls/vendor/autoload.php' );
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		$reader->setReadDataOnly(true);
		$spreadsheet = $reader->load( $tmp_file );
		$sheet = $spreadsheet->getSheet( $spreadsheet->getFirstSheetIndex() );
		$data  = $sheet->toArray();
		foreach ( $data as $data_i ){
			$msg[] = $data_i;
		}
	}

	return $msg;
}


function get_xxx_format_from_text( $url ){
	$array_import = '';
	$msg          = '';
	$path_parts   = pathinfo( $url );

	if( $path_parts['extension'] == 'csv' ){
		$array_import = get_csv_format_from_text( $url );
	}elseif( $path_parts['extension'] == 'xlsx' ){
		$array_import = get_xls_format_from_text( $url );
	}else{
		$msg = '<div class="updated notice is-dismissible">
						<p>' .  __( 'Format not supported!', 'ceonetworth' ) . '</p>
					</div>';
	}

	return array( 'records'=>$array_import, 'msg'=>$msg );
}



/*
 * Import row ajax
 */
add_action('wp_ajax_kimp_import_row',        'kimp_import_row');
add_action('wp_ajax_nopriv_kimp_import_row', 'kimp_import_row');
function kimp_import_row(){
	$res  = array( 'res'=>'' );

	$cronrun = $_POST['cronrun'] ?? 0;
	$imgreq  = $_POST['imgreq']  ?? 0;
	$count   = $_POST['count']   ?? 0;
	$time    = $_POST['time']    ?? '';
	$url     = $_POST['url']     ?? '';
	$catid   = $_POST['catid']   ?? 0;

	$cronrun = (int)$cronrun;
	$imgreq  = (int)$imgreq;
	$count   = (int)$count;
	$catid   = (int)$catid;

	$res['res'] = kaggle_import_posts( $cronrun, $count, $time, $url, $catid, $imgreq );

	echo json_encode( $res );
	die();
}


/*
 * Plugin styles && scripts
 */
function admin_menu_kaggle_styles() {
	wp_register_style('admin_menu_kaggle', plugins_url('/front/admin_menu_kaggle.css', __FILE__ ));
	wp_enqueue_style('admin_menu_kaggle');
	//wp_register_script( 'admin_menu_kaggle', plugins_url('/front/admin_menu_kaggle.js', __FILE__ ));
	//wp_enqueue_script('admin_menu_kaggle');
}
add_action( 'admin_init','admin_menu_kaggle_styles');


?>