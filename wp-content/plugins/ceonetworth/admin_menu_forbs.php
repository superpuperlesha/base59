<?php

add_action( 'admin_menu', 'ceo_import' );
function ceo_import() {
	add_submenu_page(
		'edit.php?post_type=ceonetworth',
		__(  'Import',        'ceonetworth' ),
		__(  'Richie Import', 'ceonetworth' ),
		  'manage_options',
		 'ceo_import',
		   'ceo_import_page'
	);

	function ceo_import_page() {
		$admin_url   = get_admin_url() . '/edit.php';

		// test Forbs import
		$msg_import = '';
		if ( isset( $_GET['ceo_start_import'] ) &&
		     isset( $_GET['ceo_num'] )
		) {
			$msg_import = '<div id="message" class="updated notice is-dismissible">
								<p>' . run_import_ceonet_forbs( $_GET['ceo_num'] ) . '</p>
							</div>';
		}

		// save settings
		$msg='';
		if( isset( $_GET['ceo_save_setup']             ) &&
			isset( $_GET['ceo_setup_import_count']     ) &&
			isset( $_GET['ceo_setup_import_minutes']   ) &&
			isset( $_GET['ceo_setup_import_url']       )
		) {
			update_option('ceo_setup_import_count',     (int)$_GET['ceo_setup_import_count'] );
			update_option('ceo_setup_import_minutes',   (int)$_GET['ceo_setup_import_minutes'] );
			update_option('ceo_setup_import_url',            $_GET['ceo_setup_import_url'] );
			update_option('ceo_setup_import_emailsend', ( isset( $_GET['ceo_setup_import_emailsend'] ) ?1 :0 ) );
			update_option('ceo_setup_imp_last_update',  current_time('timestamp') );

			$msg =  '<div id="message" class="updated notice is-dismissible">
						<p>'. __( ' Settings saved ', 'ceonetworth' ) . '</p>
					 </div>';
		} ?>
		<br/>
		<?php echo $msg ?>
		<h2><?php _e( 'Person import settings', 'ceonetworth' ) ?></h2>
		<p><?php echo __( 'Last started', 'ceonetworth' ) . ': ' . date( 'd.m.Y H:i', get_option('ceo_setup_imp_last_update') ) ?></p>
		<form action="<?php echo $admin_url ?>" method="get">
			<label for="ceo_setup_import_count">
				<?php echo __( 'Count import items: ', 'ceonetworth' ) ?>
				<br/>
				<input name="ceo_setup_import_count"
					   id="ceo_setup_import_count"
					   value="<?php echo get_option('ceo_setup_import_count') ?>"
					   type="number"
					   min="1"
					   class="regular-text"
				>
			</label>
			<br/>
			<label for="ceo_setup_import_count">
				<?php echo __( 'Time import cron minutes: ', 'ceonetworth' )?>
				<br/>
				<input name="ceo_setup_import_minutes"
					   id="ceo_setup_import_minutes"
					   value="<?php echo get_option('ceo_setup_import_minutes') ?>"
					   type="number"
					   min="1"
					   class="regular-text"
				>
			</label>
			<br/>
			<label for="ceo_setup_import_url">
				<?php echo __( 'Feed URL: ', 'ceonetworth' )?>
				<br/>
				<input name="ceo_setup_import_url"
					   id="ceo_setup_import_url"
					   value="<?php echo esc_url( get_option('ceo_setup_import_url') ) ?>"
					   type="url"
					   class="regular-text"
				>
			</label>
			<br/><br/>
			<label for="ceo_setup_import_emailsend">
				<input  name="ceo_setup_import_emailsend"
						id="ceo_setup_import_emailsend"
					<?php echo ( (int)get_option('ceo_setup_import_emailsend') ?'checked' :'' ) ?>
						type="checkbox"
				>
				<?php echo __( 'Send E-Mail', 'ceonetworth' )?>
			</label>
			<input name="post_type"        value="ceonetworth"  type="hidden" >
			<input name="page"             value="ceo_import"   type="hidden" >
			<input name="ceo_save_setup"   value="start"        type="hidden" >
			<br/><br/>
			<input type="submit" class="button" value="<?php _e( 'Save Settings', 'ceonetworth' )?>">
		</form>


		<!--Forbs IMPORT-->
		<br/>
		<?php echo $msg_import ?>
		<h2><?php _e( 'Test Import Persons', 'ceonetworth' ) ?></h2>
		<form action="<?php echo $admin_url ?>" method="get">
			<label for="ceo_num">
				<?php echo __( 'Number of Persons: ', 'ceonetworth' )?>
				<br/>
				<input name="ceo_num" id="ceo_num" value="3" type="text" class="regular-text">
			</label>
			<input name="post_type"        value="ceonetworth" type="hidden" >
			<input name="page"             value="ceo_import"  type="hidden" >
			<input name="ceo_start_import" value="start"       type="hidden" >
			<br/><br/>
			<input type="submit" class="button" value="<?php echo __( 'Start Import Persons', 'ceonetworth' )?>">
		</form><?php
	}
} ?>