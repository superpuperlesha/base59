<?php
/*
 * Impoort POSTS-KAGGLE
 */
function kaggle_import_posts( $cronrun, $max_count, $time, $url, $cat_id, $pimp_post_imgreq ){
	set_time_limit(1200 );
	ini_set('max_execution_time', 1200 );

	include_once( ABSPATH . 'wp-admin/includes/media.php' );
	include_once( ABSPATH . 'wp-admin/includes/file.php' );
	include_once( ABSPATH . 'wp-admin/includes/image.php' );

	$res                  = '';
	$count_res            = 0;
	$cronrun              = (int)$cronrun;
	$max_count            = (int)$max_count;
	$time                 = (int)$time;
	$url                  = (string)$url;
	$cat_id               = (int)$cat_id;
	$inserted_res         = 0;
	$updated_res          = 0;
	$img_inserted_res     = 0;
	$del_post_imgcrsh_res = 0;
	$array_import         = '';
	$msg_last_time        = '';
	$kimp_map_fields_rec  = array();
	$row_finded           = false;

	$kimp_post_count      = @unserialize( get_option('kimp_post_count',      true ) );
	$kimp_minutes         = @unserialize( get_option('kimp_minutes',         true ) );
	$kimp_url             = @unserialize( get_option('kimp_url',             true ) );
	$kimp_cat             = @unserialize( get_option('kimp_cat',             true ) );
	$kimp_map_fields      = @unserialize( get_option('kimp_map_fields'     , true ) );
	$kimp_map_file_fields = @unserialize( get_option('kimp_map_file_fields', true ) );
	$kimp_map_meta_fields = @unserialize( get_option('kimp_map_meta_fields', true ) );
	$kimp_last_start      = @unserialize( get_option('kimp_last_start',      true ) );

	$kimp_post_count      = ( is_array( $kimp_post_count      ) ? $kimp_post_count      :array() );
	$kimp_minutes         = ( is_array( $kimp_minutes         ) ? $kimp_minutes         :array() );
	$kimp_url             = ( is_array( $kimp_url             ) ? $kimp_url             :array() );
	$kimp_cat             = ( is_array( $kimp_cat             ) ? $kimp_cat             :array() );
	$kimp_map_fields      = ( is_array( $kimp_map_fields      ) ? $kimp_map_fields      :array() );
	$kimp_map_file_fields = ( is_array( $kimp_map_file_fields ) ? $kimp_map_file_fields :array() );
	$kimp_map_meta_fields = ( is_array( $kimp_map_meta_fields ) ? $kimp_map_meta_fields :array() );
	$kimp_last_start      = ( is_array( $kimp_last_start      ) ? $kimp_last_start      :array() );

	// get map fields row && set last update row
	for( $i=0; $i<count( $kimp_post_count ); $i++ ){
		if( $kimp_post_count[ $i ] == $max_count &&
		    $kimp_minutes[ $i ]    == $time &&
		    $kimp_url[ $i ]        == $url &&
		    $kimp_cat[ $i ]        == $cat_id
		){
			$row_finded = true;

			// get map fields row
			if( isset( $kimp_map_fields[ $i ] ) && is_array( $kimp_map_fields[ $i ] ) ){
				$kimp_map_fields_rec = $kimp_map_fields[ $i ];
			}

			// get map fields
			$kimp_map_file_fields = $kimp_map_file_fields[ $i ];
			$kimp_map_meta_fields = $kimp_map_meta_fields[ $i ];

			// set last update row
			$kimp_last_start[ $i ] = current_time('timestamp');
			$msg_last_time         = $kimp_last_start[ $i ];

			break;
		}
	}



	if( $cronrun === 1 ){

		// check file format
		$tmp          = get_xxx_format_from_text( $url );
		$res          = $tmp['msg'];
		$array_import = $tmp['records'];

		// check row finded
		if( !$row_finded ){
			$res = '<div class="updated notice is-dismissible">
						<p>' .  __( 'Row not finded!', 'ceonetworth' ) . '</p>
					</div>';
		}


		// check map fields setup
		if( !$kimp_map_fields_rec ){
			$res = '<div class="updated notice is-dismissible">
						<p>' .  __( 'Map fields not set!', 'ceonetworth' ) . '</p>
					</div>';
		}

		// search post title field
		$post_title_field_id = array_search( 'post_title', $kimp_map_meta_fields );
		// search post thumbnail field
		$post_thumb_field_id = array_search( 'post_thumbnail', $kimp_map_meta_fields );

		if( $post_title_field_id === false ){
			$res = '<div class="updated notice is-dismissible">
						<p>' .  __( 'Post title not set! Go to setup this row!', 'ceonetworth' ) . '</p>
					</div>';
		}


		//echo $res;
		if( !$res ){
			foreach ( $array_import as $item ){
				//dd($item);

				// skip row title
				if( $count_res === 0 ){
					$count_res++;
					continue;
				}

				// search post title
				$post_title = htmlspecialchars( $item[ $post_title_field_id ] );
				$post_key   = htmlspecialchars( 'TITLE:' . $post_title .' || URL:' . $url );

				// search existing posts
				$queried_post = get_posts( array('fields'     => 'ids',
				                                 'post_type'  => 'post',
				                                 'meta_query' => array(
					                                 array(
						                                 'key'   => 'imp_key',
						                                 'value' => $post_key,
					                                 )
				                                 )
					)
				);
				if ( $queried_post ) {
					$queried_post = get_post( (int)$queried_post[0] );
					$post_id = $queried_post->ID;
					$updated_res++;
				}else{
					$new_post = array();
					$new_post['post_status']  = 'publish';
					$new_post['post_type']    = 'post';
					$new_post['post_title']   = $post_title;
					$post_id = wp_insert_post( $new_post );
					update_post_meta( $post_id, 'imp_key', $post_key );
					$inserted_res++;
				}

				// set category
				if( $post_id && $cat_id > 0 ){
					wp_set_post_categories( $post_id, array( $cat_id ) );
					update_term_meta( $cat_id, 'cnw_last_set_post', current_time('timestamp') );
				}

				// meta fields
				if( $post_id ){
					$iii=0;
					foreach( $kimp_map_fields_rec as $key=>$kimp_map_fields_rec_i ){
						if( $post_id && $key !== '-' && $kimp_map_fields_rec_i !== '-' ){

							if( $kimp_map_meta_fields[ $iii ] === 'post_title' ){
								/*$queried_post = get_post( $post_id );
								$queried_post->post_title = (string)$item[ $iii ];
								wp_update_post( $queried_post );*/
							}elseif( $kimp_map_meta_fields[ $iii ] === 'post_content' ){
								$queried_post = get_post( $post_id );
								$queried_post->post_content = (string)$item[ $iii ];
								wp_update_post( $queried_post );
							}elseif( $kimp_map_meta_fields[ $iii ] === 'post_thumbnail' ){
								// featured image
								$f = (string)$item[ $post_thumb_field_id ];
								$image_id = media_sideload_image( $f, $post_id, $post_title, 'id' );
								if( is_integer( $image_id ) ){
									set_post_thumbnail( $post_id, $image_id );
									$img_inserted_res++;
								}elseif( $pimp_post_imgreq == 1 ){
									wp_delete_post( $post_id, true );
									$post_id = 0;
									$del_post_imgcrsh_res++;
								}
							}else{
								// custom meta field
								update_post_meta( $post_id, $kimp_map_meta_fields[ $iii ], (string)$item[ $iii ] );
							}

							$iii++;
						}
					}

					$count_res++;
				}

				if( $count_res >= $max_count ){
					break;
				}
			}

			// set last row import
			update_option('kimp_last_start', serialize( $kimp_last_start ) );

			// send email
			$res_mail = '';
			if( (int)get_option('kimp_emailsend') ){
				$to      = get_option('admin_email');
				$subject = __( 'Import Posts finished!', 'ceonetworth' );
				$body    = __( 'Count', 'ceonetworth' ) . ':' . $count_res;
				$headers = array('Content-Type: text/html; charset=UTF-8');

				if( wp_mail( $to, $subject, $body, $headers ) === true ){
					$res_mail .= __( 'E-Mail sended to',  'ceonetworth' ).': '.$to;
				}else{
					$res_mail .= __( 'E-Mail sending error',  'ceonetworth' );
				}
			}

			// result message
			$res = '<div class="updated notice is-dismissible">
						<p>' .  __( 'New Posts',            'ceonetworth' ) . ': ' . $inserted_res     . '; <br/>' .
				       __( 'Updated Posts',                 'ceonetworth' ) . ': ' . $updated_res      . '; <br/>' .
				       __( 'New images',                    'ceonetworth' ) . ': ' . $img_inserted_res . '; <br/>' .
				       __( 'Deleted post image crash',      'ceonetworth' ) . ': ' . $del_post_imgcrsh_res . '; <br/>' .
				       __( 'Request item count',            'ceonetworth' ) . ': ' . $max_count        . '; <br/>' .
				       __( 'Set time updated',              'ceonetworth' ) . ': ' . date( 'd.m.Y H:i', $msg_last_time ) . '; <br/>' .
				       $res_mail . ';
					</p>
				</div>';
		}

	}

	return $res;
} ?>