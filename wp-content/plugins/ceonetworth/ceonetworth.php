<?php
/**
 * Plugin Name: Ceonetworth
 * Plugin URI: https://ceonetworth/
 * Description: Top 400 forbes people.
 * Version: 1.0.0
 * Author: Denis Sidorkin (densid)
 * Author URI: https://densid.com/
 * Text Domain: ceonetworth
 * License: GPLv2
 * Released under the GNU General Public License (GPL)
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */


require_once dirname( __FILE__ ).'/cpt.php';
require_once dirname( __FILE__ ).'/lib.php';
require_once dirname( __FILE__ ).'/import_forbs.php';
require_once dirname( __FILE__ ).'/import_kaggle.php';
require_once dirname( __FILE__ ).'/admin_menu_forbs.php';
require_once dirname( __FILE__ ).'/admin_menu_kaggle.php';
require_once dirname( __FILE__ ).'/admin_menu_kaggle_setup.php';
require_once dirname( __FILE__ ).'/admin_menu_kagglefields_setup.php';


/*
 * Check start import Ceo-Persons
 */
add_action( 'init', 'ceo_persons_import_template_redirect' );
function ceo_persons_import_template_redirect(){
	$ts = (int)get_option('ceo_setup_imp_last_update') + (int)get_option('ceo_setup_import_minutes') * MINUTE_IN_SECONDS;
	if( $ts < current_time('timestamp') ){
		run_import_ceonet_forbs( (int)get_option('ceo_setup_import_count') );
	}
}


/*
 * Check start import Kaggle
 */
add_action( 'init', 'ceo_persons_import_template_redirect' );
function ceo_kaggle_import_template_redirect(){
	$kimp_post_run    = @unserialize( get_option('kimp_post_run',    true ) );
	$kimp_post_imgreq = @unserialize( get_option('kimp_post_imgreq', true ) );
	$kimp_post_count  = @unserialize( get_option('kimp_post_count',  true ) );
	$kimp_minutes     = @unserialize( get_option('kimp_minutes',     true ) );
	$kimp_url         = @unserialize( get_option('kimp_url',         true ) );
	$kimp_cat         = @unserialize( get_option('kimp_cat',         true ) );
	$kimp_last_start  = @unserialize( get_option('kimp_last_start',  true ) );

	$kimp_post_run    = ( is_array( $kimp_post_run )    ? $kimp_post_run    :array() );
	$kimp_post_imgreq = ( is_array( $kimp_post_imgreq ) ? $kimp_post_imgreq :array() );
	$kimp_post_count  = ( is_array( $kimp_post_count )  ? $kimp_post_count  :array() );
	$kimp_minutes     = ( is_array( $kimp_minutes )     ? $kimp_minutes     :array() );
	$kimp_url         = ( is_array( $kimp_url )         ? $kimp_url         :array() );
	$kimp_cat         = ( is_array( $kimp_cat )         ? $kimp_cat         :array() );
	$kimp_last_start  = ( is_array( $kimp_last_start )  ? $kimp_last_start  :array() );

	for( $i=0; $i<count( $kimp_post_count ); $i++ ){
		$ts = $kimp_last_start[ $i ] + $kimp_minutes[ $i ] * MINUTE_IN_SECONDS;
		if( $ts < current_time('timestamp') ) {
			kaggle_import_posts( $kimp_post_run[ $i ], $kimp_post_count[ $i ], $kimp_minutes[ $i ], $kimp_url[ $i ], $kimp_cat[ $i ], $kimp_post_imgreq[ $i ] );
		}
	}
}


