<?php

add_action( 'admin_menu', 'kaggle_import' );
function kaggle_import() {
	add_submenu_page(
		'edit.php?post_type=ceonetworth',
		__(  'Kaggle Import', 'ceonetworth' ),
		__(  'Kaggle Import', 'ceonetworth' ),
		  'manage_options',
		 'kaggle_import',
		   'kaggle_import_page'
	);

	function kaggle_import_page() {

		//echo kaggle_import_posts( 1, 3, 2000, 'https://cv.u32085.ua2.d5.com.ua/xlsx.xlsx', 7, 1 );
		//echo kaggle_import_posts( 1, 3, 2000, 'https://cv.u32085.ua2.d5.com.ua/xlsx.xlsx', 7, 1 );

		$admin_url                    = get_admin_url() . '/edit.php?post_type=ceonetworth&page=kaggle_import';
		$kaggle_menu_setup_url        = get_admin_url() . '/edit.php?post_type=ceonetworth&page=kaggle_import_setup&kaggle_row_id=';
		$kaggle_menu_setup_fields_url = get_admin_url() . '/edit.php?post_type=ceonetworth&page=kaggle_import_setup_fields';

		$list_cats = array();
		$terms     = get_terms( array(
			'taxonomy'   => 'category',
			'hide_empty' => false,
		) );
		foreach( $terms as $term ) {
			$list_cats[ $term->term_id ] = $term->name;
		}
		$list_cats_str = '';
		foreach ( $list_cats as $key=>$val ){
			$list_cats_str .= '<option value="'.$key.'">'.$val.'</option>';
		}

		$msg='';
		if( isset( $_POST['kimp_save_action'] ) ) {
			$_POST['kimp_post_run']    = $_POST['kimp_post_run']    ?? array();
			$_POST['kimp_post_imgreq'] = $_POST['kimp_post_imgreq'] ?? array();
			$_POST['kimp_post_count']  = $_POST['kimp_post_count']  ?? array();
			$_POST['kimp_minutes']     = $_POST['kimp_minutes']     ?? array();
			$_POST['kimp_url']         = $_POST['kimp_url']         ?? array();
			$_POST['kimp_cat']         = $_POST['kimp_cat']         ?? array();

			// reset array last started cron
			$kimp_last_start = array();
			foreach( $_POST['kimp_post_count'] as $i ){
				$kimp_last_start[] = current_time('timestamp');
			}

			update_option('kimp_post_run',    serialize( $_POST['kimp_post_run'] ) );
			update_option('kimp_post_imgreq', serialize( $_POST['kimp_post_imgreq'] ) );
			update_option('kimp_post_count',  serialize( $_POST['kimp_post_count'] ) );
			update_option('kimp_minutes',     serialize( $_POST['kimp_minutes'] ) );
			update_option('kimp_url',         serialize( $_POST['kimp_url'] ) );
			update_option('kimp_cat',         serialize( $_POST['kimp_cat'] ) );
			update_option('kimp_last_start',  serialize( $kimp_last_start ) );
			update_option('kimp_emailsend',   ( isset( $_POST['kimp_emailsend'] ) ? 1 : 0 ) );

			$msg = '<div class="updated notice is-dismissible">
						<p>'. __( ' Settings saved ', 'ceonetworth' ) . '</p>
					</div>';
		}
		$kimp_post_run    = @unserialize( get_option('kimp_post_run',    true ) );
		$kimp_post_imgreq = @unserialize( get_option('kimp_post_imgreq', true ) );
		$kimp_post_count  = @unserialize( get_option('kimp_post_count',  true ) );
		$kimp_minutes     = @unserialize( get_option('kimp_minutes',     true ) );
		$kimp_url         = @unserialize( get_option('kimp_url',         true ) );
		$kimp_cat         = @unserialize( get_option('kimp_cat',         true ) );
		$kimp_last_start  = @unserialize( get_option('kimp_last_start',  true ) );
		$kimp_map_fields  = @unserialize( get_option('kimp_map_fields',  true ) );
		$kimp_emailsend   =         (int)@get_option('kimp_emailsend',   true );

		$kimp_post_run    = ( is_array( $kimp_post_run )    ? $kimp_post_run    :array() );
		$kimp_post_imgreq = ( is_array( $kimp_post_imgreq ) ? $kimp_post_imgreq :array() );
		$kimp_post_count  = ( is_array( $kimp_post_count )  ? $kimp_post_count  :array() );
		$kimp_minutes     = ( is_array( $kimp_minutes )     ? $kimp_minutes     :array() );
		$kimp_url         = ( is_array( $kimp_url )         ? $kimp_url         :array() );
		$kimp_cat         = ( is_array( $kimp_cat )         ? $kimp_cat         :array() );
		$kimp_last_start  = ( is_array( $kimp_last_start )  ? $kimp_last_start  :array() );
		$kimp_map_fields  = ( is_array( $kimp_map_fields )  ? $kimp_map_fields  :array() );
		?>

		<div class="wrap wp-core-ui kimp_box">
			<h1 class="wp-heading-inline">
				<?php _e( 'Post import settings', 'ceonetworth' ) ?>
			</h1>
			<?php echo $msg ?>
			<a href="#Uid" id="mysubmit" class="page-title-action" onClick="saveRows()">
				<?php _e( 'Save records', 'ceonetworth' ) ?>
			</a>
			<a href="#Uid" id="mysubmit" class="page-title-action" onClick="addMoreRows()">
				<?php _e( 'Add new Record', 'ceonetworth' ) ?>
			</a>
			<a href="<?php echo $kaggle_menu_setup_fields_url ?>" class="page-title-action">
				<?php _e( 'Setup Fields', 'ceonetworth' ) ?>
			</a>
			<br/><br/>
			<div id="kimp_import_res"></div>
			<br/>
			<form action="<?php echo $admin_url ?>"
			      method="post"
			      id="kimp_save_form"
			>
				<input type="hidden" name="kimp_save_action">
				<label for="kimp_emailsend">
					<input  name="kimp_emailsend" id="kimp_emailsend" <?php echo ( $kimp_emailsend ?'checked' :'' ) ?> type="checkbox">
					<?php echo __( 'Send E-Mail', 'ceonetworth' )?>
				</label>
				<table class="wp-list-table widefat fixed striped table-view-list posts"
				       id="kimp_table"
				>
					<thead>
					<tr class="manage-column">
						<th class="manage-column column-title column-primary">
							<?php _e( 'Run in cron', 'ceonetworth' ) ?>
						</th>
						<th class="manage-column column-title">
							<?php _e( 'Necessary Image', 'ceonetworth' ) ?>
						</th>
						<th class="manage-column column-title">
							<?php _e( 'Count import posts', 'ceonetworth' ) ?>
						</th>
						<th class="manage-column column-title column-primary">
							<?php _e( 'Time import minutes', 'ceonetworth' ) ?>
						</th>
						<th class="manage-column column-title column-primary">
							<?php _e( 'Feed json URL (CSV/XLS)', 'ceonetworth' ) ?>
						</th>
						<th class="manage-column column-title column-primary">
							<?php _e( 'Posts to category', 'ceonetworth' ) ?>
						</th>
						<th class="manage-column column-title column-primary">
							<?php _e( 'Action', 'ceonetworth' ) ?>
						</th>
						<th class="manage-column column-title column-primary">
							<?php _e( 'Last start', 'ceonetworth' ) ?>
						</th>
						<th class="manage-column column-title column-primary">
							<?php _e( 'Map Fields', 'ceonetworth' ) ?>
						</th>
					</tr>
					</thead>

					<tbody><?php
					for( $i=0; $i<count( $kimp_post_count ); $i++ ){ ?>
						<tr class="iedit author-self level-0 type-post status-publish format-standard has-post-thumbnail hentry category-blog">
							<td class="title column-title has-row-actions column-primary">
								<select name="kimp_post_run[]">
									<option value="1" <?php echo ( (int)$kimp_post_run[ $i ] == 1 ?'selected' :'' ) ?>><?php _e( 'yes', 'ceonetworth' ) ?></option>
									<option value="0" <?php echo ( (int)$kimp_post_run[ $i ] == 0 ?'selected' :'' ) ?>><?php _e( 'no',  'ceonetworth' ) ?></option>
								</select>
								<button type="button" class="toggle-row"><span class="screen-reader-text"><?php _e( 'Content Desktop', 'ceocats' ) ?></span></button>
							</td>
							<td class="title column-title has-row-actions">
								<select name="kimp_post_imgreq[]">
									<option value="1" <?php echo ( (int)$kimp_post_imgreq[ $i ] == 1 ?'selected' :'' ) ?>><?php _e( 'yes', 'ceonetworth' ) ?></option>
									<option value="0" <?php echo ( (int)$kimp_post_imgreq[ $i ] == 0 ?'selected' :'' ) ?>><?php _e( 'no', 'ceonetworth'  ) ?></option>
								</select>
							</td>
							<td class="title column-title has-row-actions">
								<input type="number"   name="kimp_post_count[]" value="<?php echo (int)$kimp_post_count[ $i ] ?>" min=1>
								<button type="button" class="toggle-row"><span class="screen-reader-text">Show more details</span></button>
							</td>
							<td class="title column-title has-row-actions" data-colname="<?php _e( 'Time import minutes', 'ceonetworth' ) ?>">
								<input type="number"   name="kimp_minutes[]" value="<?php echo (int)$kimp_minutes[ $i ] ?>" min=1>
							</td>
							<td class="title column-title has-row-actions" data-colname="<?php _e( 'Feed json URL', 'ceonetworth' ) ?>">
								<input type="text"     name="kimp_url[]" value="<?php echo esc_attr( $kimp_url[ $i ] ) ?>">
							</td>
							<td class="title column-title has-row-actions" data-colname="<?php _e( 'Posts to category', 'ceonetworth' ) ?>">
								<select name="kimp_cat[]" ><?php
									foreach ( $list_cats as $key=>$val ){
										echo '<option value="'.$key.'" '.( $key == $kimp_cat[ $i ] ?'selected' :'' ).'>'.$val.'</option>';
									} ?>
								</select>
							</td>
							<td class="title column-title has-row-actions" data-colname="<?php _e( 'Action', 'ceonetworth' ) ?>">
								<a href="#uID" class="dashicons dashicons-trash"           onClick="deleteRow( this )"        title="<?php echo esc_attr( __( 'Delete', 'ceonetworth' ) ) ?>"></a>
								<a href="#uID" class="dashicons dashicons-database-import" onClick="importRow( this )"        title="<?php echo esc_attr( __( 'Import', 'ceonetworth' ) ) ?>"></a>
								<a href="<?php echo $kaggle_menu_setup_url . $i ?>" class="dashicons dashicons-admin-generic" title="<?php echo esc_attr( __( 'Setup',  'ceonetworth' ) ) ?>"></a>
							</td>
							<td class="title column-title has-row-actions" data-colname="<?php _e( 'Last started', 'ceonetworth' ) ?>">
								<?php echo date( 'd.m.Y', (int)$kimp_last_start[ $i ] ).'<br/>'.date( 'H:i', (int)$kimp_last_start[ $i ] ) ?>
							</td>
							<td class="title column-title has-row-actions" data-colname="<?php _e( 'Last started', 'ceonetworth' ) ?>">
								<?php echo count( $kimp_map_fields[ $i ] ?? array() ) . ' ' . __( 'fields', 'ceonetworth' ) ?>
								<?php //echo print_r( $kimp_map_fields[ $i ] ?? array(), false ) ?>
							</td>
						</tr><?php
					} ?>
					</tbody>
				</table>
			</form>
		</div>

		<script type="text/javascript">
			var WPajaxURL = '<?php echo admin_url('admin-ajax.php') ?>';

			// add table row
			function addMoreRows() {
				let table = document.getElementById('kimp_table');
				let row   = table.insertRow();

				let td1 = row.insertCell();
				let td2 = row.insertCell();
				let td3 = row.insertCell();
				let td4 = row.insertCell();
				let td5 = row.insertCell();
				let td6 = row.insertCell();
				let td7 = row.insertCell();
				let td8 = row.insertCell();

				td1.innerHTML = '<select name="kimp_post_run[]"   ><option value="1"><?php _e( 'yes', 'ceonetworth' ) ?></option><option value="0" ><?php _e( 'no', 'ceonetworth'  ) ?></option></select>';
				td2.innerHTML = '<select name="kimp_post_imgreq[]"><option value="1"><?php _e( 'yes', 'ceonetworth' ) ?></option><option value="0" ><?php _e( 'no', 'ceonetworth'  ) ?></option></select>';
				td3.innerHTML = '<input type="number" name="kimp_post_count[]"  class="kimp_post_count" min=1>';
				td4.innerHTML = '<input type="number" name="kimp_minutes[]"     class="kimp_minutes"    min=1>';
				td5.innerHTML = '<input type="text"   name="kimp_url[]"         class="kimp_url">';
				td6.innerHTML = '<select              name="kimp_cat[]"         class="kimp_cat"><?php echo $list_cats_str ?></select>';
				td7.innerHTML = '<a class="button" onClick="deleteRow( this )"><?php _e( 'Delete', 'ceonetworth' ) ?></a>';
				td8.innerHTML = '';
			}

			// delete table row
			function deleteRow( el ){
				if( window.confirm('<?php echo esc_attr( __('Confirm delete row?', 'ceonetworth' ) ) ?>') ){
					el.parentElement.parentElement.remove();
				}
			}

			// save table rows
			function saveRows(){
				let kimp_save_form = document.getElementById('kimp_save_form');
				if( kimp_save_form !== false ){
					kimp_save_form.submit();
				}
			}

			// import record
			function importRow( el ){
				let kimp_import_res = document.getElementById('kimp_import_res');
				kimp_import_res.innerHTML = '<?php _e( 'Importing...', 'ceonetworth' ) ?>';

				let record = el.parentElement.parentElement;
				let cronrun = record.querySelector("select[name='kimp_post_run[]']");
				let imgreq  = record.querySelector("select[name='kimp_post_imgreq[]']");
				let count   = record.querySelector("input[name='kimp_post_count[]']");
				let time    = record.querySelector("input[name='kimp_minutes[]']");
				let url     = record.querySelector("input[name='kimp_url[]']");
				let catid   = record.querySelector("select[name='kimp_cat[]']");

				var data = new FormData();
				data.append( 'action',  'kimp_import_row' );
				data.append( 'cronrun', cronrun.value );
				data.append( 'imgreq',  imgreq.value );
				data.append( 'count',   count.value );
				data.append( 'time',    time.value );
				data.append( 'url',     url.value );
				data.append( 'catid',   catid.value );

				fetch( WPajaxURL, {
					method: 'POST',
					body:   data,
				}).then(
					response => response.text()
				).then(
					html => {
						var response              = JSON.parse( html );
						kimp_import_res.innerHTML = response.res;
					}
				);
			}
		</script><?php
	}
} ?>