<?php

add_action( 'admin_menu', 'kaggle_import_setup_fields' );
function kaggle_import_setup_fields() {
	add_submenu_page(
		'edit.php?post_type=ceonetworth',
		__(  'Kaggle Import fields Setup', 'ceonetworth' ),
		null,
		'manage_options',
		'kaggle_import_setup_fields',
		'kaggle_import_setup_fields_page'
	);

	function kaggle_import_setup_fields_page() {
		$msg           = '';
		$kaggle_fields = $_GET['kaggle_fields'] ?? 0;

		$admin_url             = get_admin_url() . '/edit.php?post_type=ceonetworth&page=kaggle_import';
		$kaggle_menu_setup_url = get_admin_url() . '/edit.php?post_type=ceonetworth&page=kaggle_import_setup_fields';

		$kaggle_fields = @unserialize( get_option('kaggle_fields',    true ) );
		$kaggle_fields = ( is_array( $kaggle_fields ) ? $kaggle_fields :array() );


		if( isset( $_POST['kaggle_fields_action'] ) ) {
			// save settings
			$_POST['kaggle_fields'] = $_POST['kaggle_fields'] ?? '';
			$_POST['kaggle_fields'] = explode(',', $_POST['kaggle_fields'] );
			update_option('kaggle_fields', serialize( $_POST['kaggle_fields'] ) );

			$msg = '<div class="updated notice is-dismissible">
						<p>'. __( ' Settings saved.', 'ceonetworth' ) . '</p>
						<p><a href="' . $admin_url . '">' . __( ' Go to import list.', 'ceonetworth' ) . '</a></p>
					</div>';
		}


		$kaggle_fields = @unserialize( get_option('kaggle_fields', true ) );
		$kaggle_fields = is_array( $kaggle_fields ) ? $kaggle_fields : array(); ?>

		<div class="wrap wp-core-ui kimp_box">
			<h1 class="wp-heading-inline">
				<?php _e( 'Post import Fields settings', 'ceonetworth' ) ?>
			</h1>
			<?php echo $msg ?>
			<a href="#Uid" class="page-title-action" onClick="saveSettings()">
				<?php _e( 'Save settings', 'ceonetworth' ) ?>
			</a>
			<br/><br/>
			<form action="<?php echo $kaggle_menu_setup_url ?>"
			      method="post"
			      id="kimp_save_form"
			>
				<input type="hidden" name="kaggle_fields_action">
				<textarea name="kaggle_fields" rows="4" style="width:80%;"><?php echo implode( ',', $kaggle_fields ) ?></textarea>
			</form>
		</div>
		<script type="text/javascript">
			// save table rows
			function saveSettings(){
				let kimp_save_form = document.getElementById('kimp_save_form');
				if( kimp_save_form !== false ){
					kimp_save_form.submit();
				}
			}
		</script><?php
	}
} ?>