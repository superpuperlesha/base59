<?php

/*
 * Import FORBS
 */
function run_import_ceonet_forbs( $max_count=0 ){
	set_time_limit(1200 );
	ini_set('max_execution_time', 1200 );

	$max_count = (int)$max_count;

	// read json file
	$string = file_get_contents(get_option('ceo_setup_import_url' ) . '?limit=' . $max_count );

	if( $string === FALSE ) {
		echo "Could not read the file.";
	}

	$import_new_forb     = 0;
	$import_updated_forb = 0;
	$import_new_img      = 0;
	$items = json_decode( $string );

	foreach ( $items as $item ) {
		// Get post by poist gui
		$queried_post = get_page_by_path( $item->person->uri,OBJECT,'ceonetworth' );
		if ( isset( $queried_post ) ) {
			$queried_post->post_title = $item->person->name;
			$queried_post->post_content = '<p>'.implode('</p><p>', $item->bios).'</p>';
			$queried_post->menu_order = $item->rank;
			wp_update_post($queried_post);

			$finalWorth = (float)$item->finalWorth;
			if( $finalWorth > 1000 ){
				$finalWorth = '$ '.round( $finalWorth / 1000 ) .' '.__( 'B', 'ceonetworth' );
			}else{
				$finalWorth = '$ '.round( $finalWorth, 2 ).' '.__( 'M', 'ceonetworth' );
			}

			// Set ceonetworth meta data
			update_post_meta( $queried_post->ID, 'rank',      $item->rank );
			update_post_meta( $queried_post->ID, 'netWorth',  $finalWorth );
			update_post_meta( $queried_post->ID, 'age',       date('Y') - date( 'Y', ( $item->birthDate / 1000 ) ) );
			update_post_meta( $queried_post->ID, 'gender',    $item->gender );
			update_post_meta( $queried_post->ID, 'source', is_array($item->source) ? implode(', ', $item->source) : $item->source );
			update_post_meta( $queried_post->ID, 'residence', $item->countryOfCitizenship );

			$import_updated_forb++;

			continue;
		}

		$ceo = array();
		$ceo['post_title']   = $item->person->name;
		$ceo['post_content'] = '<p>'.implode('</p><p>', $item->bios).'</p>';
		$ceo['post_status']  = 'publish';
		$ceo['post_type']    = 'ceonetworth';
		$ceo['post_name']    = $item->person->uri;
		$ceo['menu_order']   = $item->rank;

		$post_id = wp_insert_post( $ceo );
		$import_new_forb++;

		if($item->person->imageExists === true) {
			// Add Featured Image to Post
			$image_url        = $item->person->squareImage; // Define the image URL here
			if(!strstr($image_url, 'http' ))
				$image_url = 'https:' . $image_url;

			$image_name       = $item->person->uri . '.png';
			$upload_dir       = wp_upload_dir(); // Set upload folder
			$image_data       = file_get_contents( $image_url ); // Get image data
			$unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
			$filename         = basename( $unique_file_name ); // Create image file name

			// Check folder permission and define file location
			if ( wp_mkdir_p( $upload_dir['path'] ) ) {
				$file = $upload_dir['path'] . '/' . $filename;
			} else {
				$file = $upload_dir['basedir'] . '/' . $filename;
			}

			// Create the image  file on the server
			file_put_contents( $file, $image_data );

			// Check image file type
			$wp_filetype = wp_check_filetype( $filename, null );

			// Set attachment data
			$attachment = array(
				'post_mime_type' => $wp_filetype['type'],
				'post_title'     => sanitize_file_name( $filename ),
				'post_content'   => '',
				'post_status'    => 'inherit'
			);

			// Create the attachment
			$attach_id = wp_insert_attachment( $attachment, $file, $post_id );

			// Include image.php
			require_once( ABSPATH . 'wp-admin/includes/image.php' );

			// Define attachment metadata
			$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

			// Assign metadata to attachment
			wp_update_attachment_metadata( $attach_id, $attach_data );

			// And finally assign featured image to post
			set_post_thumbnail( $post_id, $attach_id );

			$import_new_img++;
		}

		$finalWorth = (float)$item->finalWorth;
		if( $finalWorth > 1000 ){
			$finalWorth = '$ '.round( $finalWorth / 1000 ) .' '.__( 'B', 'ceonetworth' );
		}else{
			$finalWorth = '$ '.round( $finalWorth, 2 ).' '.__( 'M', 'ceonetworth' );
		}

		// Set ceonetworth meta data
		update_post_meta( $post_id, 'rank',      $item->rank );
		update_post_meta( $post_id, 'netWorth',  $finalWorth );
		update_post_meta( $post_id, 'age',       date('Y') - date( 'Y', ( $item->birthDate / 1000 ) ) );
		update_post_meta( $post_id, 'gender',    $item->gender );
		update_post_meta( $post_id, 'source', is_array($item->source) ? implode(', ', $item->source) : $item->source );
		update_post_meta( $post_id, 'residence', $item->countryOfCitizenship );
	}

	$res = __( 'New Persons',     'ceonetworth' ) . ': ' . $import_new_forb     . '; <br/>' .
	       __( 'Updated Persons', 'ceonetworth' ) . ': ' . $import_updated_forb . '; <br/>' .
	       __( 'New images',      'ceonetworth' ) . ': ' . $import_new_img      . '; <br/>' .
	       __( 'Request count',   'ceonetworth' ) . ': ' . $max_count           . '; <br/>';

	if( (int)get_option('ceo_setup_imp_emailsend') ){
		$to      = get_option('admin_email');
		$subject = __( 'Import Forbs complited!', 'ceonetworth' );
		$body    = $res;
		$headers = array('Content-Type: text/html; charset=UTF-8');

		if( wp_mail( $to, $subject, $body, $headers ) === true ){
			$res .= __( 'E-Mail sended to',  'ceonetworth' ).': '.$to;
		}else{
			$res .= __( 'E-Mail sending error;',  'ceonetworth' );
		}
	}

	update_option( 'ceo_setup_imp_last_update', current_time('timestamp') );

	return $res;
} ?>