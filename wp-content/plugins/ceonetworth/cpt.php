<?php
/*
* Creating a function to create our CPT
*/
function  ceo_custom_post_type() {

	// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Richies', 'Post Type General Name', 'ceonetworth' ),
		'singular_name'       => _x( 'Richie', 'Post Type Singular Name', 'ceonetworth' ),
		'menu_name'           => __( 'CEONETWORTH', 'ceonetworth' ),
		'parent_item_colon'   => __( 'Parent Richie', 'ceonetworth' ),
		'all_items'           => __( 'All Richies', 'ceonetworth' ),
		'view_item'           => __( 'View Richie', 'ceonetworth' ),
		'add_new_item'        => __( 'Add New Richie', 'ceonetworth' ),
		'add_new'             => __( 'Add New', 'ceonetworth' ),
		'edit_item'           => __( 'Edit Richie', 'ceonetworth' ),
		'update_item'         => __( 'Update Richie', 'ceonetworth' ),
		'search_items'        => __( 'Search Richie', 'ceonetworth' ),
		'not_found'           => __( 'Not Found', 'ceonetworth' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'ceonetworth' ),
	);

	// Set other options for Custom Post Type

	$args = array(
		'label'               => __( 'ceonetworth', 'ceonetworth' ),
		'description'         => __( 'Top 100 richest people', 'ceonetworth' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title',
			'editor',
			'excerpt',
			//'author',
			'thumbnail',
			'comments',
			'revisions',
			//'custom-fields'
		),
		// You can associate this CPT with a taxonomy or custom taxonomy.
		'taxonomies'          => array(''),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'show_in_rest' => true,
	);

	register_post_type( 'ceonetworth', $args );
}
add_action( 'init', 'ceo_custom_post_type', 0 );


/*
 * Order by
 */
add_action( 'pre_get_posts', 'ceo_sort_order');
function ceo_sort_order( $query ){
	if( is_archive() && is_post_type_archive( "ceonetworth" ) && !is_admin() ) {
		$query->set( 'order',          'ASC' );
		$query->set( 'orderby',        'menu_order' );
		$query->set( 'posts_per_page', '10' );
	}
};


/*
 * ADD POST METABOX
 */
add_action( 'admin_init', 'ceo_admin' );
function ceo_admin() {
	add_meta_box( 'ceo_meta_box',
		__( 'Richie Details', 'ceonetworth' ),
		'display_ceo_meta_box',
		'ceonetworth', 'normal', 'high'
	);
}

function display_ceo_meta_box( $ceo ) {
	// Retrieve current name of the Director and Movie Rating based on review ID
	$ceo_rank = esc_html( get_post_meta( $ceo->ID, 'rank', true ) );
	$ceo_netWorth = esc_html( get_post_meta( $ceo->ID, 'netWorth', true ) );
	$ceo_age = esc_html( get_post_meta( $ceo->ID, 'age', true ) );
	$ceo_gender = esc_html( get_post_meta( $ceo->ID, 'gender', true ) );
	$ceo_residence = esc_html( get_post_meta( $ceo->ID, 'residence', true ) );
	$ceo_source = esc_html( get_post_meta( $ceo->ID, 'source', true ) );
	?>
	<table>
		<tr>
			<td style="width: 100%"><?php echo __( 'Richie Rank', 'ceonetworth' ) ?></td>
			<td><input type="text" size="80" name="ceo_rank" value="<?php echo $ceo_rank; ?>" /></td>
		</tr>
		<tr>
			<td style="width: 100%"><?php echo __( 'Richie Net Worth', 'ceonetworth' ) ?></td>
			<td><input type="text" size="80" name="ceo_netWorth" value="<?php echo $ceo_netWorth; ?>" /></td>
		</tr>
		<tr>
			<td style="width: 100%"><?php echo __( 'Richie Age', 'ceonetworth' ) ?></td>
			<td><input type="text" size="80" name="ceo_age" value="<?php echo $ceo_age; ?>" /></td>
		</tr>
		<tr>
			<td style="width: 100%"><?php echo __( 'Richie Gender', 'ceonetworth' ) ?></td>
			<td><input type="text" size="80" name="ceo_gender" value="<?php echo $ceo_gender; ?>" /></td>
		</tr>
		<tr>
			<td style="width: 100%"><?php echo __( 'Richie Source', 'ceonetworth' ) ?></td>
			<td><input type="text" size="80" name="ceo_source" value="<?php echo $ceo_source; ?>" /></td>
		</tr>
		<tr>
			<td style="width: 100%"><?php echo __( 'Richie Residense', 'ceonetworth' ) ?></td>
			<td><input type="text" size="80" name="ceo_residence" value="<?php echo $ceo_residence; ?>" /></td>
		</tr>
	</table>
	<?php
}

add_action( 'save_post', 'add_ceo_fields', 10, 2 );
function add_ceo_fields( $ceo_id, $ceo ) {
	// Check post type for movie reviews
	if ( $ceo->post_type == 'ceonetworth' ) {
		// Store data in post meta table if present in post data
		if ( isset( $_POST['ceo_rank'] ) && $_POST['ceo_rank'] != '' ) {
			update_post_meta( $ceo_id, 'rank', $_POST['ceo_rank'] );
		}
		if ( isset( $_POST['ceo_netWorth'] ) && $_POST['ceo_netWorth'] != '' ) {
			update_post_meta( $ceo_id, 'netWorth', $_POST['ceo_netWorth'] );
		}
		if ( isset( $_POST['ceo_age'] ) && $_POST['ceo_age'] != '' ) {
			update_post_meta( $ceo_id, 'age', $_POST['ceo_age'] );
		}
		if ( isset( $_POST['ceo_gender'] ) && $_POST['ceo_gender'] != '' ) {
			update_post_meta( $ceo_id, 'gender', $_POST['ceo_gender'] );
		}
		if ( isset( $_POST['ceo_source'] ) && $_POST['ceo_source'] != '' ) {
			update_post_meta( $ceo_id, 'source', $_POST['ceo_source'] );
		}
		if ( isset( $_POST['ceo_residence'] ) && $_POST['ceo_residence'] != '' ) {
			update_post_meta( $ceo_id, 'residence', $_POST['ceo_residence'] );
		}
	}
} ?>