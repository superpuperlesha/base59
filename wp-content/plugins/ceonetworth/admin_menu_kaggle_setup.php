<?php

add_action( 'admin_menu', 'kaggle_import_setup' );
function kaggle_import_setup() {
	add_submenu_page(
		'edit.php?post_type=ceonetworth',
		__(  'Kaggle Import Setup', 'ceonetworth' ),
		 null,
		  'manage_options',
		 'kaggle_import_setup',
		   'kaggle_import_setup_page'
	);

	function kaggle_import_setup_page() {
		$msg                   = '';
		$row_id                = $_GET['kaggle_row_id'] ?? 0;
		$row_id                = (int)$row_id;
		$admin_url             = get_admin_url() . '/edit.php?post_type=ceonetworth&page=kaggle_import';
		$kaggle_menu_setup_url = get_admin_url() . '/edit.php?post_type=ceonetworth&page=kaggle_import_setup&kaggle_row_id=' . $row_id;

		$url               = @unserialize( get_option('kimp_url',    true ) );
		$url               = ( is_array( $url ) ? $url :array() );
		$url               = $url[ $row_id ] ?? '-';

		$kimp_map_meta_fields_post = kimp_meta_fields_post;
		$kimp_map_file_fields_file = array();


		$get_file = @file_get_contents( $url );
		if( $get_file === false ){
			// try upload file
			$msg = '<div class="updated notice is-dismissible">
						<p>' .  __( 'Error upload file!', 'ceonetworth' ) . '</p>
					</div>';
		}

		if( !$msg ){
			// check file format
			$tmp          = get_xxx_format_from_text( $url );
			$msg          = $tmp['msg'];
			$array_import = $tmp['records'];
		}


		if( isset( $_POST['kimp_setup_save_action'] ) ) {
			// save settings
			$_POST['kimp_map_file_fields'] = $_POST['kimp_map_file_fields'] ?? array();
			$_POST['kimp_map_meta_fields'] = $_POST['kimp_map_meta_fields'] ?? array();
			$kimp_map_fields               = array();

			for ( $i=0; $i<count( $_POST['kimp_map_file_fields'] ); $i++ ){
				$kimp_map_fields[ $_POST['kimp_map_file_fields'][ $i ] ] = $_POST['kimp_map_meta_fields'][ $i ];
			}

			$kimp_map_fields_orig      = @unserialize( get_option('kimp_map_fields',      true ) );
			$kimp_map_file_fields_orig = @unserialize( get_option('kimp_map_file_fields', true ) );
			$kimp_map_meta_fields_orig = @unserialize( get_option('kimp_map_meta_fields', true ) );

			$kimp_map_fields_orig      = ( is_array( $kimp_map_fields_orig )      ? $kimp_map_fields_orig      : array() );
			$kimp_map_file_fields_orig = ( is_array( $kimp_map_file_fields_orig ) ? $kimp_map_file_fields_orig : array() );
			$kimp_map_meta_fields_orig = ( is_array( $kimp_map_meta_fields_orig ) ? $kimp_map_meta_fields_orig : array() );

			$kimp_map_fields_orig[ $row_id ]      = $kimp_map_fields;
			$kimp_map_file_fields_orig[ $row_id ] = $_POST['kimp_map_file_fields'];
			$kimp_map_meta_fields_orig[ $row_id ] = $_POST['kimp_map_meta_fields'];

			update_option('kimp_map_fields',      serialize( $kimp_map_fields_orig ) );
			update_option('kimp_map_file_fields', serialize( $kimp_map_file_fields_orig ) );
			update_option('kimp_map_meta_fields', serialize( $kimp_map_meta_fields_orig ) );

			$msg = '<div class="updated notice is-dismissible">
						<p>'. __( ' Settings saved.', 'ceonetworth' ) . '</p>
						<p><a href="' . $admin_url . '">' . __( ' Go to import list.', 'ceonetworth' ) . '</a></p>
					</div>';
		}


		// map data for record
		$kimp_map_fields = get_option('kimp_map_fields', true );
		$kimp_map_fields = unserialize( $kimp_map_fields );
		if( !is_array( $kimp_map_fields ) ){
			$kimp_map_fields = array();
		}

		$kimp_map_file_fields = @unserialize( get_option('kimp_map_file_fields', true ) );
		$kimp_map_file_fields = ( is_array( $kimp_map_file_fields ) ? $kimp_map_file_fields : array() );
		$kimp_map_file_fields = $kimp_map_file_fields[ $row_id ] ?? array();
		$kimp_map_meta_fields = @unserialize( get_option('kimp_map_meta_fields', true ) );
		$kimp_map_meta_fields = ( is_array( $kimp_map_meta_fields ) ? $kimp_map_meta_fields : array() );
		$kimp_map_meta_fields = $kimp_map_meta_fields[ $row_id ] ?? array();

		foreach ( $array_import as $item ) {
			foreach ( $item as $item_i ) {
				$kimp_map_file_fields_file[] = $item_i;
			}
			break;
		} ?>

		<div class="wrap wp-core-ui kimp_box">
			<h1 class="wp-heading-inline">
				<?php _e( 'Post import settings', 'ceonetworth' ) ?>
			</h1>
			<?php echo $msg ?>
			<a href="#Uid" class="page-title-action" onClick="saveSettings()">
				<?php _e( 'Save settings', 'ceonetworth' ) ?>
			</a>
			<br/><br/>
			<form action="<?php echo $kaggle_menu_setup_url ?>"
			      method="post"
			      id="kimp_save_form"
			>
				<input type="hidden" name="kimp_setup_save_action">
				<table class="wp-list-table widefat fixed striped table-view-list posts"
				       id="kimp_table"
				>
					<thead>
					<tr class="manage-column">
						<th class="manage-column column-title column-primary">
							<?php _e( 'File Fields', 'ceonetworth' ) ?>
						</th>
						<th class="manage-column column-title">
							<?php _e( 'Post Meta Fields', 'ceonetworth' ) ?>
						</th>
					</tr>
					</thead>

					<tbody><?php
					for( $i=0; $i<count( $kimp_map_file_fields_file ); $i++ ){ ?>
						<tr class="iedit author-self level-0 type-post status-publish format-standard has-post-thumbnail hentry category-blog">
							<td class="title column-title has-row-actions column-primary"><?php
								echo '<input type="text" name="kimp_map_file_fields[]"  value="' . esc_attr( $kimp_map_file_fields_file[ $i ] ) . '" readonly="readonly">' ?>
							</td>
							<td class="title column-title has-row-actions" data-colname="<?php _e( 'Meta field', 'ceonetworth' ) ?>">
								<select name="kimp_map_meta_fields[]" ><?php
									echo '<option value="-">---</option>';
									foreach ( $kimp_map_meta_fields_post as $kimp_map_meta_fields_post_i ){
										echo '<option value="' . esc_attr( $kimp_map_meta_fields_post_i ) . '" ' . ( $kimp_map_meta_fields[ $i ] == $kimp_map_meta_fields_post_i ?'selected' :'' ) . '>' . $kimp_map_meta_fields_post_i . '</option>';
									} ?>
								</select>
							</td>
						</tr><?php
					} ?>
					</tbody>
				</table>
			</form>
		</div>
		<script type="text/javascript">
			// save table rows
			function saveSettings(){
				let kimp_save_form = document.getElementById('kimp_save_form');
				if( kimp_save_form !== false ){
					kimp_save_form.submit();
				}
			}
		</script><?php
	}
} ?>