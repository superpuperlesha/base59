<?php
/**
 * Plugin Name: page-blocks
 * Plugin URI: https://page-blocks/
 * Description: page-blocks.
 * Version: 1.0.0
 * Author: Denis Sidorkin (densid)
 * Author URI: https://densid.com/
 * Text Domain: pblocks
 * License: GPLv2
 * Released under the GNU General Public License (GPL)
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */


require_once dirname( __FILE__ ).'/admin_menu.php';


/*
 * Plugin styles && scripts
 */
function ceopost_styles() {
	wp_register_style('page-blocks', plugins_url('/front/ceoposts.css', __FILE__ ));
	wp_enqueue_style('page-blocks');
	//wp_register_script( 'ceopost', plugins_url('/front/ceopost.js', __FILE__ ));
	//wp_enqueue_script('ceopost');
}
add_action( 'admin_init','ceopost_styles');


//Load template from specific page
add_filter( 'page_template', 'page_blocks_page_template' );
function page_blocks_page_template( $page_template ){
	if ( get_page_template_slug() == 'template-configurator.php' ) {
		$page_template = dirname( __FILE__ ) . '/template-configurator.php';
	}
	return $page_template;
}

/**
 * Add "Custom" template to page attirbute template section.
 */
add_filter( 'theme_page_templates', 'page_blocks_add_template_to_select', 10, 4 );
function page_blocks_add_template_to_select( $post_templates, $wp_theme, $post, $post_type ) {
	$post_templates['template-configurator.php'] = __('Configurator');
	return $post_templates;
}
