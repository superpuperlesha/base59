<?php
/**
 * Plugin Name: Delivery service "Arta"
 * Plugin URI: https://profiles.wordpress.org/superpuperlesha/
 * Description: Delivery service "Arta" for woocommerce.
 * Version: 2.0.0
 * Author: superpuperlesha@gmail.com
 * Author URI: https://profiles.wordpress.org/superpuperlesha/#content-plugins
 * Text Domain: delivery_service_arta
 * License: GPLv2
 * Released under the GNU General Public License (GPL)
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */


if ( ! defined( 'WPINC' ) ) {
	die;
}


/*
 * Check WOOCOMMERCE plugin
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	require_once __DIR__ . '/class-delivery_service_arta.php';
}else{
	add_action( 'admin_notices', function (){
		echo'<div class="error">
                <h3>'.__('Woocommerce plugin is required to use the plugin [delivery_service_arta] !', 'delivery_service_arta').'</h3>
             </div>';
	} );
}


/*
 * Add settings URL to plugin
 */
add_filter( 'plugin_action_links_' . plugin_basename(__FILE__ ), 'delivery_service_arta_settings_link' );
function delivery_service_arta_settings_link( $links ) {
	$url           = get_admin_url() . 'admin.php?page=wc-settings&tab=shipping&section=delivery_service_arta';
	$settings_link = '<a href=' . esc_url( $url ) . '>' . __( 'Settings', 'delivery_service_arta' ) . '</a>';
	array_push(	$links, $settings_link );
	return $links;
}


