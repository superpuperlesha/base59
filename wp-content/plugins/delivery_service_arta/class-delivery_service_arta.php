<?php
/*
 * ARTA shipping method
 */
function start_arta_shipping_method() {
	if ( ! class_exists( 'delivery_service_arta' ) ) {
		class delivery_service_arta extends WC_Shipping_Method {

			/**
			 * Constructor for your shipping class
			 *
			 * @access public
			 * @return void
			 */
			public function __construct() {
				$this->id                 = 'delivery_service_arta';
				$this->method_title       = __( 'ARTA Shipping settings', 'delivery_service_arta' );
				$this->method_description = __( 'Shipping from:', 'delivery_service_arta' ) .
				                            ' <a href="https://arta.io/" target="_blank">ARTA</a>, ' .
				                            ' <a href="https://api-reference.arta.io/" target="_blank">ARTA-API</a>.';

				$this->init();
				//$this->enabled = isset( $this->settings['enabled'] ) ? $this->settings['enabled'] : 'yes';
				$this->title   = isset( $this->settings['title']   ) ? $this->settings['title']   : __( 'ARTA Shipping', 'delivery_service_arta' );
			}

			/**
			 * Init your settings
			 *
			 * @access public
			 * @return void
			 */
			function init() {
				// Load the settings API
				$this->init_form_fields();
				$this->init_settings();

				// Save settings in admin if you have any defined
				add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
			}

			/**
			 * Define settings field for this shipping
			 * @return void
			 */
			function init_form_fields() {

				$this->form_fields = array(
					'quote_types' => array(
						'title'       => __( 'Quote Types', 'woo_arta_shipping' ),
						'type'        => 'multiselect',
						'description' => __( 'Select one or more items', 'delivery_service_arta' ),
						'options' => array(
							'premium'   => 'premium',
							'select'    => 'select',
							'parcel'    => 'parcel'
						)
					),

					'title' => array(
						'title'       => __( 'Title', 'delivery_service_arta' ),
						'type'        => 'text',
						'description' => __( 'Title to be display on site', 'delivery_service_arta' ),
						'default'     => __( 'ARTA Shipping', 'delivery_service_arta' )
					),

					'apikey' => array(
						'title'       => __( 'ARTA API key', 'delivery_service_arta' ),
						'type'        => 'text',
						'description' => __( 'ARTA API key service', 'delivery_service_arta' ),
						'default'     => ''
					),

				);
			}

			/**
			 * This function is used to calculate the shipping cost. Within this function we can check for weights, dimensions and other parameters.
			 *
			 * @access public
			 * @param mixed $package
			 * @return void
			 */
			public function calculate_shipping( $package = array() ) {
				$arta_key              = $this->settings['apikey'];
				$quote_types           = $this->settings['quote_types'];

				$origin_country        = WC()->countries->get_base_country();
				$origin_region         = WC()->countries->get_base_state();
				$origin_city           = WC()->countries->get_base_city();
				$origin_postal         = WC()->countries->get_base_postcode();
				$origin_address        = WC()->countries->get_base_address();

				$destination_country   = $package[ 'destination' ]['country'];
				$destination_region    = $package[ 'destination' ]['state'];
				$destination_city      = $package[ 'destination' ]['city'];
				$destination_postal    = $package[ 'destination' ]['postcode'];
				$destination_address   = $package[ 'destination' ]['address'];

				$product_title    = array();
				$product_width    = array();
				$product_height   = array();
				$product_length   = array();
				$product_weight   = array();
				$product_quantity = array();
				$product_price    = array();

				foreach ( $package['contents'] as $item_id => $values ){
					$_product           = $values['data'];

					$product_title[]    = $_product->get_title();
					$product_width[]    = $_product->get_width();
					$product_height[]   = $_product->get_height();
					$product_length[]   = $_product->get_length();
					$product_weight[]   = $_product->get_weight();
					$product_price[]    = $_product->get_regular_price();
					$product_quantity[] = $values['quantity'];
				}

				$arr = self::arta_request_cost(
					$arta_key,
					$quote_types,
					$origin_country,
					$origin_region,
					$origin_city,
					$origin_postal,
					$origin_address,
					$product_title,
					$product_width,
					$product_height,
					$product_length,
					$product_weight,
					$product_price,
					$product_quantity,
					$destination_country,
					$destination_region,
					$destination_city,
					$destination_postal,
					$destination_address
				);

				if( $arr['err'] ){
					$this->add_rate( array(
						'id'    => $this->id,
						'label' => $this->title.' ('.$arr['err'].')',
						'cost'  => 0,
					) );
				} else {
					foreach( $arr['data'] as $arr_i ){
						$this->add_rate( array(
							'id'    => $arr_i['id'].'_'.$this->id,
							'label' => esc_html( $this->title.' ('.$arr_i['quote_type'].')' ),
							'cost'  => $arr_i['total'],
						) );
					}
				}
			}

			/*
			 * Get prices to order delivery
			 */
			public static function arta_request_cost(   $arta_key,
														$quote_types=array('premium','select','parcel'),
														$origin_country='',
														$origin_region='',
														$origin_city='',
														$origin_postal='',
														$origin_address='',
														$product_title=array(),
													    $product_width=array(),
													    $product_height=array(),
														$product_length=array(),
														$product_weight=array(),
														$product_price=array(),
														$product_quantity=array(),
														$destination_country='',
														$destination_region='',
														$destination_city='',
														$destination_postal='',
														$destination_address=''

			){
				$res                    = array('err'=>'', 'data'=>array() );
				$quote_types            = (array)$quote_types;
				$arta_key               = (string)$arta_key;
				$origin_country         = (string)$origin_country;
				$origin_region          = (string)$origin_region;
				$origin_city            = (string)$origin_city;
				$origin_postal          = (string)$origin_postal;
				$origin_address         = (string)$origin_address;

				$product_title          = (array)$product_title;
				$product_width          = (array)$product_width;
				$product_height         = (array)$product_height;
				$product_length         = (array)$product_length;
				$product_weight         = (array)$product_weight;
				$product_price          = (array)$product_price;
				$product_quantity       = (array)$product_quantity;
				$product_weight_unit    = substr( get_option('woocommerce_weight_unit'), 0, 2 );
				$product_dimension_unit = get_option('woocommerce_dimension_unit');
				$product_currency_unit  = get_woocommerce_currency();

				$destination_country    = (string)$destination_country;
				$destination_region     = (string)$destination_region;
				$destination_city       = (string)$destination_city;
				$destination_postal     = (string)$destination_postal;
				$destination_address    = (string)$destination_address;

				if( $js_request = file( __DIR__ . '/js_arta_request_object.js' ) ){
					$js_request = implode( '', $js_request );
					$js_request = json_decode( $js_request );
				} else {
					$js_request = array();
				}

				if( $js_request_product = file( __DIR__ . '/js_arta_request_object_product.js' ) ){
					$js_request_product = implode( '', $js_request_product );
					$js_request_product = json_decode( $js_request_product );
				} else {
					$js_request_product = array();
				}

				$arta_url   = 'https://api.arta.io/requests';

				// set addtess to delivery
				$js_request->request->origin->country        = $origin_country;
				$js_request->request->origin->region         = $origin_region;
				$js_request->request->origin->city           = $origin_city;
				$js_request->request->origin->postal_code    = $origin_postal;
				$js_request->request->origin->address_line_1 = $origin_address;
				$js_request->request->origin->address_line_2 = '';
				$js_request->request->origin->address_line_3 = '';
				$js_request->request->preferred_quote_types  = $quote_types;

				// add products to package to tarification
				for( $i=0; $i<count( $product_title ); $i++ ){
					$js_request_product->internal_reference  = $product_title[ $i ];
					$js_request_product->creation_date        = time();
					$js_request_product->creator             = 'user';
					$js_request_product->notes               = 'excerpt';
					$js_request_product->title               = $product_title[ $i ];
					$js_request_product->width               = $product_width[ $i ];
					$js_request_product->height              = $product_height[ $i ];
					$js_request_product->unit_of_measurement = $product_dimension_unit;
					$js_request_product->weight              = $product_weight[ $i ];
					$js_request_product->weight_unit         = $product_weight_unit;
					$js_request_product->value               = $product_price[ $i ];
					$js_request_product->value_currency      = $product_currency_unit;
					$js_request_product->images              = array();

					for( $ii=0; $ii<$product_quantity[ $i ]; $ii++ ){
						$js_request->request->objects[] = $js_request_product;
					}
				}

				// set address destination
				$js_request->request->destination->country        = $destination_country;
				$js_request->request->destination->region         = $destination_region;
				$js_request->request->destination->city           = $destination_city;
				$js_request->request->destination->postal_code    = $destination_postal;
				$js_request->request->destination->address_line_1 = $destination_address;
				$js_request->request->destination->address_line_2 = '';
				$js_request->request->destination->address_line_3 = '';

				$response = wp_remote_post( $arta_url, array(
					'timeout'     => 6000,
					'redirection' => 5,
					'httpversion' => '1.0',
					'blocking'    => true,
					'headers'     => array('content-type'=>'application/json', 'Authorization'=>'ARTA_APIKey '.$arta_key, 'Arta-Quote-Timeout'=>6000, ),
					'body'        => json_encode( $js_request ),
					'cookies'     => array(),
					'method'      => 'POST',
					'data_format' => 'body',
				) );
				if ( is_wp_error( $response ) ) {
					$res['err'] .= esc_html( $response->get_error_message() );
				} else {
					$response_data = json_decode( $response['body'], true );
				}

				if( isset( $response_data['disqualifications'] ) &&
					count( $response_data['disqualifications'] ) > 0
				){
					foreach ( $response_data['disqualifications'] as $isqualification ){
						$res['err'] .= esc_html( $isqualification['reason'] ).'<br/>';
					}
				}elseif(  isset( $response_data['errors'] ) &&
					count( $response_data['errors'] ) > 0
				){
					foreach ( $response_data['errors'] as $key=>$errors ){
						$res['err'] .=  __( 'Error', 'delivery_service_arta' ) . ': ' . $key . '<br/>';
					}
				}else{
					foreach( $response_data['quotes'] as $quote_i ){
						$res['data'][] = array( 'id'=>$quote_i['id'], 'quote_type'=>$quote_i['quote_type'],  'total'=> $quote_i['total'] );
					}
				}

				return $res;
			}
		}
	}
}
add_action( 'woocommerce_shipping_init', 'start_arta_shipping_method' );


/*
 * Add shipping method
 */
function add_delivery_service_arta( $methods ) {
	$methods[] = 'delivery_service_arta';
	return $methods;
}
add_filter( 'woocommerce_shipping_methods', 'add_delivery_service_arta' );


/*
 * Validate order
 */
/*function arta_validate_order( $posted )   {
	$packages       = WC()->shipping->get_packages();
	$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );

	if( is_array( $chosen_methods ) && in_array( 'delivery_service_arta', $chosen_methods ) ) {

		foreach ( $packages as $i => $package ) {
			if ( $chosen_methods[ $i ] != "delivery_service_arta" ) {
				continue;
			}

			if( ! wc_has_notice( $message, $messageType ) ) {
				wc_add_notice( $message, $messageType );
			}
		}
	}
}
add_action( 'woocommerce_review_order_before_cart_contents', 'arta_validate_order' , 10 );
add_action( 'woocommerce_after_checkout_validation',         'arta_validate_order' , 10 );*/


