=== Delivery insurance for woocommerce. ===
Contributors: superpuperlesha
Donate link: https://example.com/
Tags: Arta, Arata woocommerce, Arta shipping, Arta delivery, Arta price
Tested up to: 6.2.2
Stable tag: 2.0.0
Requires PHP: 7.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Delivery service "Arta" for woocommerce.

== Description ==

Plugin Delivery service "Arta" for woocommerce, create a delivery with a price.

Installation:

* Go to woocommerce->settings->shipping->ARTA Shipping settings.
* Write "ARTA API key" production or tested.

== Frequently Asked Questions ==

= A question that someone might have =

== Screenshots ==

1. The plugin has a menu here.

== Changelog ==

= 1.0 =
* First version.

== Upgrade Notice ==
= 1.0 =
First version

== A brief Markdown Example ==