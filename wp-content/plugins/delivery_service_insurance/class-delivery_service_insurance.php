<?php
/*
 * Insurance Delivery
 */
function start_insurance_shipping_method() {
	if ( ! class_exists( 'delivery_service_insurance' ) ) {
		class delivery_service_insurance extends WC_Shipping_Method {

			/**
			 * Constructor for your shipping class
			 *
			 * @access public
			 * @return void
			 */
			public function __construct() {
				$this->id                 = 'delivery_service_insurance';
				$this->method_title       = __( 'Insurance Shipping settings', 'delivery_service_insurance' );
				$this->method_description = __( 'In this section, you can select the percentage of delivery for each shipping method. One more insured person will be added for each shipping method.', 'delivery_service_insurance' );
				$this->init();
			}

			/**
			 * Init your settings
			 *
			 * @access public
			 * @return void
			 */
			function init() {
				$this->init_form_fields();
				$this->init_settings();
				add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
			}

			/**
			 * Define settings field for this shipping
			 * @return void
			 */
			function init_form_fields() {
				$this->form_fields = array(
					'delivery_service_insurance_title' => array(
						'title'       => __( 'Delivery method Title', 'delivery_service_insurance' ),
						'type'        => 'text',
						'description' => __( 'Delivery method insurance', 'delivery_service_insurance' ),
						'default'     => __( 'Insurance', 'delivery_service_insurance' )
					),
					'delivery_service_insurance_procent' => array(
						'title'       => __( '% shipping insurance', 'delivery_service_insurance' ),
						'type'        => 'number',
						'description' => __( 'Enter the shipping insurance percentage', 'delivery_service_insurance' ),
						'default'     => '10'
					),
				);
			}

			/**
			 * This function is used to calculate the shipping cost. Within this function we can check for weights, dimensions and other parameters.
			 *
			 * @access public
			 * @param mixed $package
			 * @return void
			 */
			public function calculate_shipping( $package = array() ) {
				$delivery_service_insurance_title   = $this->settings['delivery_service_insurance_title'];
				$delivery_service_insurance_procent = (float)$this->settings['delivery_service_insurance_procent'];

                $price     = WC()->cart->subtotal;
                $new_price = $price / 100 * $delivery_service_insurance_procent;
                $new_price = round( $new_price, 2 );

                foreach( $package['rates'] as $key=>$value ){
                    /*file_put_contents( __DIR__ . '/d_1.txt',
                        'UNSET: '.print_r( $value->get_label(),true ).chr(10),
                        FILE_APPEND
                    );*/

                    if( strpos( $key, 'id_delivery_service_insurance_', 0 ) === false ){
                        $new_price_delivery = $new_price + $value->get_cost();
                        $id = 'id_delivery_service_insurance_'.rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
                        $this->add_rate( array(
                            'id'    => $id,
                            'label' => $delivery_service_insurance_title . ' (' . esc_html( $value->get_label() ) . ')',
                            'cost'  => $new_price_delivery,
                        ) );
                    }
                }
			}
		}
	}
}
add_action( 'woocommerce_shipping_init', 'start_insurance_shipping_method' );



/*
 * Add shipping method
 */
function add_delivery_service_insurance( $methods ) {
	$methods[] = 'delivery_service_insurance';
	return $methods;
}
add_filter( 'woocommerce_shipping_methods', 'add_delivery_service_insurance' );


/*
 * Validate order
 */
/*function arta_validate_order( $posted )   {
	$packages       = WC()->shipping->get_packages();
	$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );

	if( is_array( $chosen_methods ) && in_array( 'delivery_service_insurance', $chosen_methods ) ) {

		foreach ( $packages as $i => $package ) {
			if ( $chosen_methods[ $i ] != "delivery_service_insurance" ) {
				continue;
			}

			if( ! wc_has_notice( $message, $messageType ) ) {
				wc_add_notice( $message, $messageType );
			}
		}
	}
}
add_action( 'woocommerce_review_order_before_cart_contents', 'arta_validate_order' , 10 );
add_action( 'woocommerce_after_checkout_validation',         'arta_validate_order' , 10 ); */

