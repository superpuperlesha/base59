<?php
/*
	Plugin Name: tz1
	Plugin URI: http://www.tz1.com/
	Description: Short code to form ajax
	Version: 1.0.0
	Author: superpuperlesha
	Author URI: https://profiles.wordpress.org/superpuperlesha/#content-plugins
	License: GPL2
	Text Domain: cbs
*/


	if ( ! defined( 'WPINC' ) ) {
		die;
	}


	// list letters
	$short_code_info_letters = array('a','b','c');


	// TRANSLATE plugin
	load_plugin_textdomain(
		'cbs',
		false,
		dirname( plugin_basename( __FILE__ ) ) . '/languages'
	);


	// register Short-Code form
	add_shortcode( 'short_code_info', 'short_code_info_shortcode' );
	function short_code_info_shortcode( $atts ) {
		// $atts['same']
		$res = '<div class="short_code_info_form">
					<input type="text" id="short_code_info_input">
					<button type="submit" id="short_code_info_submit">' . __('Convert', 'cbs') . '</button>
					<p id="short_code_info_msg"></p>
				</div>';
		return $res;
	}


	// Register AJAX-JS
	function short_code_info_scripts_styles() {
		$args = array( 'in_footer' => true );

		wp_enqueue_style( 'tz1_form', plugin_dir_url( __FILE__ ) . '/short_code_info.css', array() );
		wp_enqueue_script( 'tz1_ajax',   plugin_dir_url( __FILE__ ) . '/short_code_info.js', array(), '', $args );
		wp_add_inline_script( 'tz1_ajax', ' var TZ1ajaxURL = "' . admin_url( 'admin-ajax.php' ) . '"; ' );

	}
	add_action( 'wp_enqueue_scripts', 'short_code_info_scripts_styles' );


	// Register AJAX form action
	add_action('wp_ajax_short_code_info_proceed', 'short_code_info_proceed');
	add_action('wp_ajax_nopriv_short_code_info_proceed', 'short_code_info_proceed');
	function short_code_info_proceed() {
		GLOBAL $short_code_info_letters;

		$size_txt = isset( $_POST['short_code_info_input'] ) ? (string)$_POST['short_code_info_input'] :'';
		$size_txt = strtolower( $size_txt );

		if( !in_array( $size_txt, $short_code_info_letters )) {
			_e('Invalid size', 'cbs');
			exit();
		}

		echo sanitize_text_field( short_code_info( $size_txt ) );

		exit();
	}


	// main function
	function short_code_info( $char='' ){
		if( $char === 'a' ){
			return 1;
		}else if( $char === 'b' ){
			return 2;
		}else if( $char === 'c' ){
			return 3;
		}
	}


	// add link to setup plugin
	function short_code_info_settings_link( $links ) {
		$settings_link = '<a href="' . esc_url( admin_url( 'tools.php?page=bra_admin' ) ) . '">' . __( 'Settings', 'cbs' ) . '</a>';
		array_unshift($links, $settings_link );
		return $links;
	}
	add_filter( 'plugin_action_links_'.plugin_basename(__FILE__), 'short_code_info_settings_link' );


	// add page to admin panel
	add_action( 'admin_menu', 'short_code_info_settings_page' );
	function short_code_info_settings_page() {
		add_submenu_page(
			'tools.php',
			__(  'Bra sizes', 'cbs' ),
			__(  'Bra sizes', 'cbs' ),
			'manage_options',
			'bra_admin',
			'short_code_info_settings_page_return'
		);

		function short_code_info_settings_page_return() {
			echo '<h1>' . __( 'Bra Sizes', 'cbs' ) . '</h1>';

			// generate form
			echo do_shortcode('[short_code_info]');
			// add scripts && styles
			echo'<script src="' . plugin_dir_url( __FILE__ ) . '/tz1.js" id="tz1_ajax-js-admin"></script>
				 <link rel="stylesheet" id="tz1_form-css-admin" href="' . plugin_dir_url( __FILE__ ) . '/tz1.css" media="all">
			<script id="tz1_ajax-js-admin">
				var TZ1ajaxURL = "' . admin_url( 'admin-ajax.php' ) . '";
			</script>';

			_e('Create a shortcode that can be inserted into any page of the site so that the user can enter a letter size and get a numeric size. An example of a shortcode is [short_code_info], which creates a form for entering a size.', 'cbs' );
		}
	}

