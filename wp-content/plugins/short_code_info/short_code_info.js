/*
	TZ1 (Form Submit)
*/
document.addEventListener('click', event => {
    if( event.target.id === 'convert_bra_size_submit' ){
		let form_input = document.getElementById('convert_bra_size_input');
        let form_button = event.target;
        let form_msg = document.getElementById('convert_bra_size_msg');

        if( form_input !== null
				&&
			form_msg !== null
        ){
        	// stop any submition
			event.preventDefault();

			// load variables
            let data = new FormData();
            data.append( 'action',                 'convert_bra_size_proceed' );
            data.append( 'convert_bra_size_input', form_input.value );

            // start request
            fetch( TZ1ajaxURL, {
                method: 'POST',
                body:   data,
            }).then(
                response => response.text()
            ).then(
                html => {
					form_msg.innerHTML = html;
                }
            );
        }
    }
});

