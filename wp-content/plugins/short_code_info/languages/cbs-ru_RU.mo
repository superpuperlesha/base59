��          \       �       �   	   �   	   �      �   �   �      �     �     �  �  �  '   �  '   �     �  z       �     �  <   �   Bra Sizes Bra sizes Convert Create a shortcode that can be inserted into any page of the site so that the user can enter a letter size and get a numeric size. An example of a shortcode is [convert_bra_size], which creates a form for entering a size. Invalid size Settings Short code to form ajax Project-Id-Version: tz1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2024-12-02 16:12+0000
PO-Revision-Date: 2024-12-02 16:14+0000
Last-Translator: 
Language-Team: Russian
Language: ru_RU
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.6.14; wp-6.7.1
X-Domain: cbs Размеры бюстгальтера Размеры бюстгальтера Конвертировать Создайте шорткод, который можно вставить на любую страницу сайта, чтобы пользователь мог ввести размер букв и получить числовой размер. Пример шорткода — [convert_bra_size], который создает форму для ввода размера. Неверный размер Настройки Короткий код для формирования ajax 