<?php

/**
 * Plugin Name: xxx PHP GuttBG Block
 * Author: John Doe
 * Version: 1.0.0
 */
 

function loadMyBlock() {
	wp_enqueue_script('my-new-block', plugin_dir_url(__FILE__).'block.js', ['wp-blocks', 'wp-editor'], true);
}
add_action('enqueue_block_editor_assets', 'loadMyBlock');


function riad_render_block_latest_post( $attribites ){
	$recent_posts = wp_get_recent_posts([
		'numberposts' => 1,
		'post_status' => 'publish',
	]);
	if(count($recent_posts) === 0){
		return 'No posts';
	}
	$post = $recent_posts[0];
	return '<a class="wp-phpfile" href="'.esc_url( get_permalink($post['ID']) ).'">*PHP*:'.esc_html( get_the_title( $post['ID'] ) ).'</a> ';
}
register_block_type('riad/latest-post', [
		'render_callback' => 'riad_render_block_latest_post',
	]);


// function gutenberg_my_block_init(){
	// register_meta( 'post', 'book-title', [
		// 'show_in_rest' => true,
		// 'single'       => true,
	// ]);
// }
// add_action( 'init', 'gutenberg_my_block_init' );