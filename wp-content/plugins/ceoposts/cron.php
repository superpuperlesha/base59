<?php
/*
 * Start import rows
 */
function pimp_import_rows(){
	$pimp_post_imgreq = @unserialize( get_option('pimp_post_imgreq', true ) );
	$pimp_post_count  = @unserialize( get_option('pimp_post_count',  true ) );
	$pimp_minutes     = @unserialize( get_option('pimp_minutes',     true ) );
	$pimp_url         = @unserialize( get_option('pimp_url',         true ) );
	$pimp_cat         = @unserialize( get_option('pimp_cat',         true ) );
	$pimp_last_start  = @unserialize( get_option('pimp_last_start',  true ) );

	$pimp_post_imgreq  = ( is_array( $pimp_post_imgreq ) ? $pimp_post_imgreq :array() );
	$pimp_post_count   = ( is_array( $pimp_post_count )  ? $pimp_post_count  :array() );
	$pimp_minutes      = ( is_array( $pimp_minutes )     ? $pimp_minutes     :array() );
	$pimp_url          = ( is_array( $pimp_url )         ? $pimp_url         :array() );
	$pimp_cat          = ( is_array( $pimp_cat )         ? $pimp_cat         :array() );
	$pimp_last_start   = ( is_array( $pimp_last_start )  ? $pimp_last_start  :array() );

	for( $i=0; $i<count( $pimp_post_count ); $i++ ){
		$ts = $pimp_last_start[ $i ] +  $pimp_minutes[ $i ] * MINUTE_IN_SECONDS;
		if( $ts < current_time('timestamp') ){
			ceo_import_posts( $pimp_post_count[ $i ], $pimp_minutes[ $i ], $pimp_url[ $i ], $pimp_cat[ $i ], $pimp_post_imgreq[ $i ] );
		}
	}
	//===// set last time import to row===
} ?>