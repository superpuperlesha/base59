<?php
/*
 * Impoort POSTS
 */
function ceo_import_posts( $max_count, $time, $url, $ceo_setup_imp_cat, $pimp_post_imgreq ){
	set_time_limit(1200 );
	ini_set('max_execution_time', 1200 );

	include_once(ABSPATH . 'wp-admin/includes/media.php');
	include_once(ABSPATH . 'wp-admin/includes/file.php');
	include_once(ABSPATH . 'wp-admin/includes/image.php');

	$res               = '';
	$count_res         = 0;
	$max_count         = (int)$max_count;
	$time              = (int)$time;
	$url               = (string)$url;
	$ceo_setup_imp_cat = (int)$ceo_setup_imp_cat;
	$inserted_res      = 0;
	$updated_res       = 0;
	$img_inserted_res  = 0;

	$array = @file_get_contents( $url );
	if( $array !== false ){
		$array = json_decode( $array, true );

		if( isset( $array['items'] ) && is_array( $array['items'] ) ){
			foreach ( $array['items'] as $item ){

				// image necessary
				if( ( $pimp_post_imgreq == 1 && isset( $item['image'] ) && $item['image'] )
						||
                      $pimp_post_imgreq == 0
				){

					// search existing posts
					$queried_post = get_posts( array('fields'     => 'ids',
					                                 'post_type'  => 'post',
					                                 'meta_query' => array(
						                                 array(
							                                 'key'   => 'imp_key',
							                                 'value' => $item['id'],
						                                 )
					                                 )
						)
					);
					if ( $queried_post ) {
						$queried_post = get_post( (int)$queried_post[0] );
						$queried_post->post_title   = $item['title'];
						$queried_post->post_content = '<p>'.$item['content_html'].'</p>';
						$queried_post->guid         = $item['id'];
						wp_update_post( $queried_post );
						$post_id = $queried_post->ID;
						if( $ceo_setup_imp_cat > 0 ){
							wp_set_post_categories( $post_id, array( $ceo_setup_imp_cat ) );
							update_term_meta( $ceo_setup_imp_cat, 'cnw_last_set_post', current_time('timestamp') );
						}
						$updated_res++;
					}else{
						$new_post = array();
						$new_post['post_title']   = $item['title'];
						$new_post['post_content'] = '<p>'.$item['content_html'].'</p>';
						$new_post['post_status']  = 'publish';
						$new_post['post_type']    = 'post';
						$post_id = wp_insert_post( $new_post );
						update_post_meta( $post_id, 'imp_key', $item['id'] );
						if( $ceo_setup_imp_cat > 0 ){
							wp_set_post_categories( $post_id, array( $ceo_setup_imp_cat ) );
						}
						$inserted_res++;

						// image
						if ( isset( $item['image'] ) && $item['image'] ) {
							$image_id = media_sideload_image( $item['image'], $post_id, $item['title'], 'id' );
							if( is_integer( $image_id ) ){
								set_post_thumbnail( $post_id, $image_id );
								$img_inserted_res++;
							}else{
								wp_delete_post( $post_id, true );
							}
						}
					}
				}

				if( ++$count_res >= $max_count ){
					break;
				}
			}

			$res_mail = '';
			if( (int)get_option('pimp_emailsend') ){
				$to      = get_option('admin_email');
				$subject = __( 'Import Posts complited!', 'ceoposts' );
				$body    = __( 'Count', 'ceoposts' ) . ':' . $count_res;
				$headers = array('Content-Type: text/html; charset=UTF-8');

				if( wp_mail( $to, $subject, $body, $headers ) === true ){
					$res_mail .= __( 'E-Mail sended to',  'ceoposts' ).': '.$to;
				}else{
					$res_mail .= __( 'E-Mail sending error',  'ceoposts' );
				}
			}

			//===set last time import to row===
			$msg_last_time   = '';
			$pimp_post_count = @unserialize( get_option('pimp_post_count', true ) );
			$pimp_minutes    = @unserialize( get_option('pimp_minutes',    true ) );
			$pimp_url        = @unserialize( get_option('pimp_url',        true ) );
			$pimp_cat        = @unserialize( get_option('pimp_cat',        true ) );
			$pimp_last_start = @unserialize( get_option('pimp_last_start', true ) );

			$pimp_post_count = ( is_array( $pimp_post_count ) ? $pimp_post_count :array() );
			$pimp_minutes    = ( is_array( $pimp_minutes )    ? $pimp_minutes :array() );
			$pimp_url        = ( is_array( $pimp_url )        ? $pimp_url :array() );
			$pimp_cat        = ( is_array( $pimp_cat )        ? $pimp_cat :array() );
			$pimp_last_start = ( is_array( $pimp_last_start ) ? $pimp_last_start :array() );

			for( $i=0; $i<count( $pimp_post_count ); $i++ ){
				if( $pimp_post_count[ $i ] == $max_count &&
				    $pimp_minutes[ $i ]    == $time &&
				    $pimp_url[ $i ]        == $url &&
				    $pimp_cat[ $i ]        == $ceo_setup_imp_cat
				){
					$pimp_last_start[ $i ] = current_time('timestamp');
					update_option('pimp_last_start', serialize( $pimp_last_start ) );
					$msg_last_time = $pimp_last_start[ $i ];
				}
			}
			//===// set last time import to row===

			$res = '<div class="updated notice is-dismissible">
						<p>' .  __( 'New Posts',       'ceoposts' ) . ': ' . $inserted_res     . '; <br/>' .
								__( 'Updated Posts',   'ceoposts' ) . ': ' . $updated_res      . '; <br/>' .
								__( 'New images',      'ceoposts' ) . ': ' . $img_inserted_res . '; <br/>' .
								__( 'Request count',   'ceoposts' ) . ': ' . $max_count        . '; <br/>' .
			                    __( 'Set time updated','ceoposts' ) . ': ' . date( 'd.m.Y H:i', $msg_last_time ) . '; <br/>' .
								$res_mail . ';
						</p>
					</div>';
		}else{
			$res = '<div class="updated notice is-dismissible">
						<p>' .  __( 'Data error in file!', 'ceoposts' ) . '</p>
					</div>';
		}
	}else{
		$res = '<div class="updated notice is-dismissible">
					<p>' .  __( 'Error upload file!', 'ceoposts' ) . '</p>
				</div>';
	}

	return $res;
} ?>