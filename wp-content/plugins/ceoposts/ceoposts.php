<?php
/**
 * Plugin Name: CeoPosts
 * Plugin URI: https://ceoposts/
 * Description: Top 400 forbes people.
 * Version: 1.0.0
 * Author: Denis Sidorkin (densid)
 * Author URI: https://densid.com/
 * Text Domain: ceoposts
 * License: GPLv2
 * Released under the GNU General Public License (GPL)
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */


require_once dirname( __FILE__ ).'/import_posts.php';
require_once dirname( __FILE__ ).'/admin_menu.php';
require_once dirname( __FILE__ ).'/cron.php';


/*
 * Plugin styles && scripts
 */
function ceopost_styles() {
	wp_register_style('ceopost', plugins_url('/front/ceoposts.css', __FILE__ ));
	wp_enqueue_style('ceopost');
	//wp_register_script( 'ceopost', plugins_url('/front/ceopost.js', __FILE__ ));
	//wp_enqueue_script('ceopost');
}
add_action( 'admin_init','ceopost_styles');



/*
 * Check start import CeoPosts
 */
add_action( 'init', 'ceo_posts_import_template_redirect' );
function ceo_posts_import_template_redirect(){
	pimp_import_rows();
}

