<?php

add_action( 'admin_menu', 'posts_import' );
function posts_import() {
	add_submenu_page(
		'edit.php',
		__(  'Import',       'ceoposts' ),
		__(  'Posts Import', 'ceoposts' ),
		 'manage_options',
		'posts_import',
		  'posts_import_page'
	);

	function posts_import_page() {
		$admin_url   = get_admin_url() . '/edit.php';

		/*$array = file_get_contents( 'https://rss.app/feeds/v1.1/_BZddOYBtp8BeApy4.json' );
		$array = json_decode( $array, true );
		echo'<pre>';
		foreach( $array['items'] as $item ){
			echo 'title: '.$item['title'].' image: '.$item['image'].'<br/>';
		}
		echo'</pre>';*/

		$list_cats = array();
		$terms    = get_terms( array(
			'taxonomy'   => 'category',
			'hide_empty' => false,
		) );
		foreach( $terms as $term ) {
			$list_cats[ $term->term_id ] = $term->name;
		}
		$list_cats_str = '';
		foreach ( $list_cats as $key=>$val ){
			$list_cats_str .= '<option value="'.$key.'">'.$val.'</option>';
		}

		$msg='';
		if( isset( $_POST['pimp_save_action'] ) ) {
			$_POST['pimp_post_imgreq'] = $_POST['pimp_post_imgreq'] ?? array();
			$_POST['pimp_post_count']  = $_POST['pimp_post_count']  ?? array();
			$_POST['pimp_minutes']     = $_POST['pimp_minutes']     ?? array();
			$_POST['pimp_url']         = $_POST['pimp_url']         ?? array();
			$_POST['pimp_cat']         = $_POST['pimp_cat']         ?? array();

			// reset array last started cron
			$pimp_last_start = array();
			foreach( $_POST['pimp_post_count'] as $i ){
				$pimp_last_start[] = current_time('timestamp');
			}

			update_option('pimp_post_imgreq', serialize( $_POST['pimp_post_imgreq'] ) );
			update_option('pimp_post_count',  serialize( $_POST['pimp_post_count'] ) );
			update_option('pimp_minutes',     serialize( $_POST['pimp_minutes'] ) );
			update_option('pimp_url',         serialize( $_POST['pimp_url'] ) );
			update_option('pimp_cat',         serialize( $_POST['pimp_cat'] ) );
			update_option('pimp_last_start',  serialize( $pimp_last_start ) );
			update_option('pimp_emailsend',   ( isset( $_POST['pimp_emailsend'] ) ? 1 : 0 ) );

			$msg = '<div class="updated notice is-dismissible">
						<p>'. __( ' Settings saved ', 'ceoposts' ) . '</p>
					</div>';
		}
		$pimp_post_imgreq = @unserialize( get_option('pimp_post_imgreq', true ) );
		$pimp_post_count  = @unserialize( get_option('pimp_post_count',  true ) );
		$pimp_minutes     = @unserialize( get_option('pimp_minutes',     true ) );
		$pimp_url         = @unserialize( get_option('pimp_url',         true ) );
		$pimp_cat         = @unserialize( get_option('pimp_cat',         true ) );
		$pimp_last_start  = @unserialize( get_option('pimp_last_start',  true ) );
		$pimp_emailsend   =         (int)@get_option('pimp_emailsend',   true );

		$pimp_post_imgreq = ( is_array( $pimp_post_imgreq ) ? $pimp_post_imgreq :array() );
		$pimp_post_count  = ( is_array( $pimp_post_count )  ? $pimp_post_count  :array() );
		$pimp_minutes     = ( is_array( $pimp_minutes )     ? $pimp_minutes     :array() );
		$pimp_url         = ( is_array( $pimp_url )         ? $pimp_url         :array() );
		$pimp_cat         = ( is_array( $pimp_cat )         ? $pimp_cat         :array() );
		$pimp_last_start  = ( is_array( $pimp_last_start )  ? $pimp_last_start  :array() ); ?>

		<div class="wrap wp-core-ui">
			<h1 class="wp-heading-inline">
				<?php _e( 'Post import settings', 'ceoposts' ) ?>
			</h1>
			<?php echo $msg ?>
			<a href="#Uid" id="mysubmit" class="page-title-action" onClick="saveRows()">
				<?php _e( 'Save records', 'ceoposts' ) ?>
			</a>
			<a href="#Uid" id="mysubmit" class="page-title-action" onClick="addMoreRows()">
				<?php _e( 'Add new Record', 'ceoposts' ) ?>
			</a>
			<br/><br/>
			<div id="pimp_import_res"></div>
			<br/>
			<form action="<?php echo $admin_url ?>?page=posts_import"
				  method="post"
				  id="pimp_save_form"
			>
				<input type="hidden" name="pimp_save_action">
				<label for="pimp_emailsend">
					<input  name="pimp_emailsend" id="pimp_emailsend" <?php echo ( $pimp_emailsend ?'checked' :'' ) ?> type="checkbox">
					<?php echo __( 'Send E-Mail', 'ceoposts' )?>
				</label>
				<table class="wp-list-table widefat fixed striped table-view-list posts"
					   id="pimp_table"
				>
					<thead>
						<tr class="manage-column">
							<th class="manage-column column-title column-primary">
								<?php _e( 'Necessary Image', 'ceoposts' ) ?>
							</th>
							<th class="manage-column column-title">
								<?php _e( 'Count import posts', 'ceoposts' ) ?>
							</th>
							<th class="manage-column column-title column-primary">
								<?php _e( 'Time import minutes', 'ceoposts' ) ?>
							</th>
							<th class="manage-column column-title column-primary">
								<?php _e( 'Feed json URL', 'ceoposts' ) ?>
							</th>
							<th class="manage-column column-title column-primary">
								<?php _e( 'Posts to category', 'ceoposts' ) ?>
							</th>
							<th class="manage-column column-title column-primary">
								<?php _e( 'Action', 'ceoposts' ) ?>
							</th>
							<th class="manage-column column-title column-primary">
								<?php _e( 'Last started', 'ceoposts' ) ?>
							</th>
						</tr>
					</thead>

					<tbody><?php
						for( $i=0; $i<count( $pimp_post_count ); $i++ ){ ?>
							<tr class="iedit author-self level-0 type-post status-publish format-standard has-post-thumbnail hentry category-blog">
								<td class="title column-title has-row-actions column-primary">
									<select name="pimp_post_imgreq[]">
										<option value="1" <?php echo ( (int)$pimp_post_imgreq[ $i ] == 1 ?'selected' :'' ) ?>><?php _e( 'yes', 'ceoposts' ) ?></option>
										<option value="0" <?php echo ( (int)$pimp_post_imgreq[ $i ] == 0 ?'selected' :'' ) ?>><?php _e( 'no', 'ceoposts'  ) ?></option>
									</select>
									<button type="button" class="toggle-row"><span class="screen-reader-text"><?php _e( 'Content Desktop', 'ceocats' ) ?></span></button>
								</td>
								<td class="title column-title has-row-actions column-primary">
									<input type="number"   name="pimp_post_count[]" value="<?php echo (int)$pimp_post_count[ $i ] ?>" min=1>
									<button type="button" class="toggle-row"><span class="screen-reader-text">Show more details</span></button>
								</td>
								<td class="title column-title has-row-actions" data-colname="<?php _e( 'Time import minutes', 'ceoposts' ) ?>">
									<input type="number"   name="pimp_minutes[]" value="<?php echo (int)$pimp_minutes[ $i ] ?>" min=1>
								</td>
								<td class="title column-title has-row-actions" data-colname="<?php _e( 'Feed json URL', 'ceoposts' ) ?>">
									<input type="text"     name="pimp_url[]" value="<?php echo esc_attr( $pimp_url[ $i ] ) ?>">
								</td>
								<td class="title column-title has-row-actions" data-colname="<?php _e( 'Posts to category', 'ceoposts' ) ?>">
									<select name="pimp_cat[]" ><?php
										foreach ( $list_cats as $key=>$val ){
											echo '<option value="'.$key.'" '.( $key == $pimp_cat[ $i ] ?'selected' :'' ).'>'.$val.'</option>';
										} ?>
									</select>
								</td>
								<td class="title column-title has-row-actions" data-colname="<?php _e( 'Action', 'ceoposts' ) ?>">
									<a class="button" onClick="deleteRow( this )"><?php _e( 'Delete', 'ceoposts' ) ?></a>
									<a class="button" onClick="importRow( this )"><?php _e( 'Import', 'ceoposts' ) ?></a>
								</td>
								<td class="title column-title has-row-actions" data-colname="<?php _e( 'Last started', 'ceoposts' ) ?>">
									<?php echo date( 'd.m.Y H:i', (int)$pimp_last_start[ $i ] ); ?>
								</td>
							</tr><?php
						} ?>
					</tbody>
				</table>
			</form>
		</div>

		<script type="text/javascript">
			var WPajaxURL = '<?php echo admin_url('admin-ajax.php') ?>';

			// add table row
			function addMoreRows() {
				let table = document.getElementById('pimp_table');
				let row   = table.insertRow();

				let td1 = row.insertCell();
				let td2 = row.insertCell();
				let td3 = row.insertCell();
				let td4 = row.insertCell();
				let td5 = row.insertCell();
				let td6 = row.insertCell();
				let td7 = row.insertCell();

				td1.innerHTML = '<select name="pimp_post_imgreq[]"><option value="1"><?php _e( 'yes', 'ceoposts' ) ?></option><option value="0" ><?php _e( 'no', 'ceoposts'  ) ?></option></select>';
				td2.innerHTML = '<input type="number" name="pimp_post_count[]"  class="pimp_post_count" min=1>';
				td3.innerHTML = '<input type="number" name="pimp_minutes[]"     class="pimp_minutes"    min=1>';
				td4.innerHTML = '<input type="text"   name="pimp_url[]"         class="pimp_url">';
				td5.innerHTML = '<select              name="pimp_cat[]"         class="pimp_cat"><?php echo $list_cats_str ?></select>';
				td6.innerHTML = '<a class="button" onClick="deleteRow( this )"><?php _e( 'Delete', 'ceoposts' ) ?></a>';
				td7.innerHTML = '';
			}

			// delete table row
			function deleteRow( el ){
				el.parentElement.parentElement.remove();
			}

			// save table rows
			function saveRows(){
				let pimp_save_form = document.getElementById('pimp_save_form');
				if( pimp_save_form !== false ){
					pimp_save_form.submit();
				}
			}

			// import record
			function importRow( el ){
				let pimp_import_res = document.getElementById('pimp_import_res');
				pimp_import_res.innerHTML = '<?php _e( 'Importing...', 'ceoposts' ) ?>';

				let record = el.parentElement.parentElement;
				let imgreq = record.querySelector("select[name='pimp_post_imgreq[]']");
				let count  = record.querySelector("input[name='pimp_post_count[]']");
				let time   = record.querySelector("input[name='pimp_minutes[]']");
				let url    = record.querySelector("input[name='pimp_url[]']");
				let catid  = record.querySelector("select[name='pimp_cat[]']");

				var data = new FormData();
				data.append( 'action', 'pimp_import_row' );
				data.append( 'imgreq', imgreq.value );
				data.append( 'count',  count.value );
				data.append( 'time',   time.value );
				data.append( 'url',    url.value );
				data.append( 'catid',  catid.value );

				fetch( WPajaxURL, {
					method: 'POST',
					body:   data,
				}).then(
					response => response.text()
				).then(
					html => {
						var response              = JSON.parse( html );
						pimp_import_res.innerHTML = response.res;
					}
				);
			}
		</script><?php
	}
}


/*
 * Import row
 */
add_action('wp_ajax_pimp_import_row',        'pimp_import_row');
add_action('wp_ajax_nopriv_pimp_import_row', 'pimp_import_row');
function pimp_import_row(){
	$res  = array( 'res'=>'' );

	$imgreq = $_POST['imgreq'] ?? 0;
	$count  = $_POST['count']  ?? 0;
	$time   = $_POST['time']   ?? '';
	$url    = $_POST['url']    ?? '';
	$catid  = $_POST['catid']  ?? 0;

	$count  = (int)$count;
	$time   = (int)$time;
	$catid  = (int)$catid;

	$res['res'] = ceo_import_posts( $count, $time, $url, $catid, $imgreq );

	echo json_encode( $res );
	die();
} ?>