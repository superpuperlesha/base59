<?php
/**
* Plugin Name: GeoIP-Widget
* Plugin URI: https://GeoIP-Widget/
* Description: GeoIP Widget.
* Version: 1.0.0
* Author: Denis Sidorkin (densid)
* Author URI: https://densid.com/
* Text Domain: geoipwidget
* License: GPLv2
* Released under the GNU General Public License (GPL)
* https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/


require_once __DIR__ . '/regions.php';
require_once __DIR__ . '/widget.php';


/*
 * front scripts
 */
add_action( 'wp_enqueue_scripts', 'geoip_widget_front' );
function geoip_widget_front() {
	wp_enqueue_script('geoip-widget-moment',   'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js');
	wp_enqueue_script('geoip-widget-timezone', 'https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone-with-data-2012-2022.min.js');
	wp_enqueue_script('geoip-widget',              plugins_url('/geoip-widget/front/geoip-widget.js'));
	wp_enqueue_style( 'geoip-widget',              plugins_url('/geoip-widget/front/geoip-widget.css'));
	// api-description https://momentjs.com/timezone/
}


/*
 * Register and load the widget
 */
function geoip_load_widget() {
	register_widget( 'geoip_widget' );
}
add_action( 'widgets_init', 'geoip_load_widget' );


/*
 * AJAX url to wp_footer
 */
function your_function() {
	echo'<script>var WPajaxURL = "' . admin_url('admin-ajax.php') . '";</script>';
}
add_action( 'wp_footer', 'your_function' );

