jQuery( document ).ready(function(){

    var geoip_max_width = 1200;
    var geoip_cur_width = jQuery( window ).width();
    var geoip_moby = geoip_cur_width < geoip_max_width ?'m' :'d';

    jQuery('.geoip-widget').each(function(){
        if( jQuery(this).data('timezone') === moment.tz.guess() && geoip_moby === jQuery(this).data('mobydesk') ){
            jQuery(this).show();
        }
    });

    jQuery('.geoip-widgetn').each(function(){
        if( jQuery(this).data('timezonen') !== moment.tz.guess() && geoip_moby === jQuery(this).data('mobydesk') ){
            jQuery(this).show();
        }
    });

});

