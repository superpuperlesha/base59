<?php


// Creating the widget
class geoip_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'geoip_widget',
			__('Geo Adds Widget', 'geoip_widget_domain'),
			array( 'description' => __( 'Geo Adds Widget', 'geoip_widget_domain' ), )
		);
	}

	// Creating widget front-end
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		} ?>
		<div class="geoip-widget"
			 data-timezone="<?php echo $instance['timezone'] ?>"
			 data-trackid="<?php echo $instance['widgetid']  ?>"
			 data-mobydesk="d"
		><?php echo $instance['code'] ?></div>
		<div class="geoip-widget"
			 data-timezone="<?php echo $instance['timezone'] ?>"
			 data-trackid="<?php echo $instance['widgetid']  ?>"
			 data-mobydesk="m"
		><?php echo $instance['mcode'] ?></div>
		<div class="geoip-widgetn"
			 data-timezonen="<?php echo $instance['timezone'] ?>"
			 data-trackid="<?php echo $instance['widgetid']   ?>"
			 data-mobydesk="d"
		><?php echo $instance['coden'] ?></div>
		<div class="geoip-widgetn"
			 data-timezonen="<?php echo $instance['timezone'] ?>"
			 data-trackid="<?php echo $instance['widgetid']   ?>"
			 data-mobydesk="m"
		><?php echo $instance['mcoden'] ?></div><?php
		echo $args['after_widget'];
	}


	// Widget Backend
	public function form( $instance ) {
		if ( isset( $instance[ 'timezone' ] ) ) {
			$timezone = $instance[ 'timezone' ];
		}else {
			$timezone = 'Iceland';
		}
		if ( isset( $instance[ 'widgetid' ] ) ) {
			$widgetid = $instance[ 'widgetid' ];
		}else {
			$widgetid = date('U');
		} ?>

		<!--Widget admin form-->
		<input id="<?php echo $this->get_field_id( 'widgetid' ) ?>"
			   name="<?php echo $this->get_field_name( 'widgetid' ) ?>"
			   type="hidden"
			   value="<?php echo esc_attr( $widgetid ); ?>"
		>
		<p>
			<label for="<?php echo $this->get_field_id( 'timezone' ) ?>"><?php _e( 'Show in Timezone:' ); ?></label>
			<select  class="widefat"
					 id="<?php echo $this->get_field_id( 'timezone' ) ?>"
					 name="<?php echo $this->get_field_name( 'timezone' ); ?>"
			><?php
				foreach( validMomentTimezones as $zone ) {
					echo '<option ' . ( $zone == esc_attr( $timezone ) ?'selected="selected"' :'' ) . '>'. esc_attr( $zone ) .'</option>';
				} ?>
			</select>
		</p>
		<h4><?php _e( 'Desktop:' ); ?></h4>
		<p>
			<label for="<?php echo $this->get_field_id( 'code' ) ?>"><?php _e( 'HTML Code In zone:' ) ?></label>
			<textarea class="widefat"
					  rows="4"
					  id="<?php echo $this->get_field_id( 'code' ) ?>"
					  name="<?php echo $this->get_field_name( 'code' ) ?>"
			><?php echo esc_attr( $instance[ 'code' ] ); ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'coden' ) ?>"><?php _e( 'HTML Code Not In zone:' ) ?></label>
			<textarea class="widefat"
					  rows="4"
					  id="<?php echo $this->get_field_id( 'coden' ) ?>"
					  name="<?php echo $this->get_field_name( 'coden' ) ?>"
			><?php echo esc_attr( $instance[ 'coden' ] ); ?></textarea>
		</p>

		<h4><?php _e( 'Mobyle:' ) ?></h4>
		<p>
			<label for="<?php echo $this->get_field_id( 'mcode' ) ?>"><?php _e( 'HTML Code In zone:' ) ?></label>
			<textarea class="widefat"
					  rows="4"
					  id="<?php echo $this->get_field_id( 'mcode' ) ?>"
					  name="<?php echo $this->get_field_name( 'mcode' ) ?>"
			><?php echo esc_attr( $instance[ 'mcode' ] ) ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'mcoden' ) ?>"><?php _e( 'HTML Code Not In zone:' ) ?></label>
			<textarea class="widefat"
					  rows="4"
					  id="<?php echo $this->get_field_id( 'mcoden' ) ?>"
					  name="<?php echo $this->get_field_name( 'mcoden' ) ?>"
			><?php echo esc_attr( $instance[ 'mcoden' ] ); ?></textarea>
		</p><?php
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['timezone'] = ( ! empty( $new_instance['timezone'] ) ) ? strip_tags( $new_instance['timezone'] ) : '';
		$instance['code']     = ( ! empty( $new_instance['code']     ) ) ? $new_instance['code']     : '';
		$instance['coden']    = ( ! empty( $new_instance['coden']    ) ) ? $new_instance['coden']    : '';
		$instance['mcode']    = ( ! empty( $new_instance['mcode']    ) ) ? $new_instance['mcode']    : '';
		$instance['mcoden']   = ( ! empty( $new_instance['mcoden']   ) ) ? $new_instance['mcoden']   : '';
		$instance['widgetid'] = ( ! empty( $new_instance['widgetid'] ) ) ? $new_instance['widgetid'] : '';
		return $instance;
	}
}
