<?php
/**
 * Plugin Name: Nasa Images
 * Plugin URI: https://test/
 * Description: Nasa Images.
 * Version: 1.0.0
 * Author: superpuperlesha@gmail.com
 * Author URI: https://superpuperlesha.com/
 * Text Domain: nasaimg
 * License: GPLv2
 * Released under the GNU General Public License (GPL)
 * https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */


require_once dirname( __FILE__ ).'/admin_menu.php';


/*
 * Plugin styles && scripts
 */
add_action( 'admin_init', 'nasaimg_styles');
function nasaimg_styles() {
	wp_register_style('nasaimg', plugins_url('/front/nasaimg.css', __FILE__ ));
	wp_enqueue_style('nasaimg');
	//wp_register_script( 'nasaimg', plugins_url('/front/nasaimg.js', __FILE__ ));
	//wp_enqueue_script('nasaimg');
}


/*
 * Add page URL to plugin
 */
add_filter( 'plugin_action_links_' . plugin_basename(__FILE__ ), 'nasaimg_settings_link' );
function nasaimg_settings_link( $links ) {
    $url           = get_admin_url() . 'upload.php?page=nasaimg';
    $settings_link = '<a href=' . esc_url( $url ) . ' data-title="'.esc_attr( __FILE__ ).'">' . __( 'Settings', 'delivery_service_insurance' ) . '</a>';
    array_push(	$links, $settings_link );
    return $links;
}