<?php

add_action( 'admin_menu', 'admin_menu_nasaimg' );
function admin_menu_nasaimg() {
	add_submenu_page(
		'upload.php',
		__(  'Import',       'nasaimg' ),
		__(  'Nasa Images',  'nasaimg' ),
		 'manage_options',
		 'nasaimg',
		  'admin_menu_nasaimg_page'
	);

	function admin_menu_nasaimg_page() {
        set_time_limit(600 );
        ini_set('max_execution_time', 600 ); ?>

		<div class="wrap wp-core-ui nasaimg_admin_box">
			<h1 class="wp-heading-inline">
				<?php _e( 'Nasa Images', 'nasaimg' ) ?>
			</h1>
            <br/><?php

            $err   = '';
            $url   = 'https://images-api.nasa.gov/search?q=apollo%2011&description=moon%20landing&media_type=image';
            //$url   = 'https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?earth_date=2015-6-3&api_key=jegTzCdIk6C2HgkYMws0FqypHQLFYCXvNrEozxRx';
            $array = @file_get_contents( $url );

            if( $array === false ){
                $err = __( 'Api error!', 'nasaimg' );
            }

            if( !$err ) {
                $array = json_decode( $array, true);

                /*echo '<pre>';
                print_r( $array );
                echo '</pre>';*/

                if( !isset( $array['collection'] ) ){
                    $err = __( 'Api data error 1!', 'nasaimg' );
                }

                if( !$err ) {

                    if( !isset( $array['collection']['items'] ) ){
                        $err = __( 'Api data error 2!', 'nasaimg' );
                    }

                    if( !$err ) { ?>
                        <table class="wp-list-table widefat fixed striped table-view-list posts">
                            <thead>
                                <tr class="manage-column">
                                    <th class="manage-column column-title column-primary">
                                        <?php _e( 'Image', 'nasaimg' ) ?>
                                    </th>
                                    <th class="manage-column column-title">
                                        <?php _e( 'Description', 'nasaimg' ) ?>
                                    </th>
                                    <th class="manage-column column-title column-primary">
                                        <?php _e( 'Author', 'nasaimg' ) ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody><?php
                                foreach ( $array['collection']['items'] as $item ) { ?>
                                    <tr class="iedit author-self level-0 type-post status-publish format-standard has-post-thumbnail hentry category-blog">
                                        <td class="title column-title has-row-actions column-primary">
                                            <img src="<?php echo $item['links'][0]['href'] ?>"
                                                 alt="<?php echo esc_attr( $item['data'][0]['description'] ) ?>"
                                                 width="200"
                                            >
                                            <button type="button" class="toggle-row"><span class="screen-reader-text"><?php _e( 'Content Desktop', 'nasaimg' ) ?></span></button>
                                        </td>
                                        <td class="title column-title has-row-actions" data-colname="<?php _e( 'Description', 'nasaimg' ) ?>">
                                            <?php echo $item['data'][0]['description'] ?>
                                        </td>
                                        <td class="title column-title has-row-actions" data-colname="<?php _e( 'Author', 'nasaimg' ) ?>">
                                            <?php echo $item['data'][0]['photographer'] ?>
                                        </td>
                                    </tr><?php
                                } ?>
                            </tbody>
                        </table><?php
                    }
                }
            }

            if( $err ){ ?>
                <h2><?php echo $err ?></h2><?php
            } ?>

		</div><?php
	}
} ?>