<?php
namespace wpsync_webspark_ns;

class wpsync_webspark{
	public static $suf = __CLASS__;
	
	public function __construct(){
		add_shortcode( 'wpsync-webspark', array( $this, 'wpsync_webspark_doshortcode' ) );
    }

	public function wpsync_webspark_doshortcode(){
		if( class_exists( 'WooCommerce' ) ){
			$data = self::wpsync_webspark_get();
			if( $data === false ){
				echo 'error api';
				wp_mail( get_option('admin_email'),
					__('Error api "wp.webspark.dev"', 'wpsync_webspark'),
					__('Error api "wp.webspark.dev"', 'wpsync_webspark'),
					array('From: Site '.get_option('admin_email') )
				);
			}else{
				self::wpsync_webspark_setproduct( $data );
				echo 'Synk ok';
				wp_mail( get_option('admin_email'),
					__('Synk ok "wp.webspark.dev"', 'wpsync_webspark'),
					__('Synk ok "wp.webspark.dev"', 'wpsync_webspark'),
					array('From: Site '.get_option('admin_email') )
				);
			}
		}else{
			echo 'Woocommerce need';
			wp_mail( get_option('admin_email'),
				__('Woocommerce need "wp.webspark.dev"', 'wpsync_webspark'),
				__('Woocommerce need "wp.webspark.dev"', 'wpsync_webspark'),
				array('From: Site '.get_option('admin_email') )
			);
		}
	}

	public static function wpsync_webspark_get(){
		$response = wp_remote_get('https://wp.webspark.dev/wp-api/products' );
		if ( is_array( $response ) && ! is_wp_error( $response ) ) {
			$body    = $response['body'];
			$body_arr = json_decode( $body, true );

			if( isset( $body_arr['error'] ) && $body_arr['error'] === false  ){
				return $body_arr;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public static function wpsync_webspark_setproduct_img( $img_url, $post_id, $desc ){

		stream_context_set_default( array(
			'http' => array(
				'method' => 'HEAD'
			)
		));
		$headers = get_headers( $img_url, 1 );
		if ( $headers !== false && isset( $headers['Location'] ) ) {
			$parse_url = parse_url( $img_url );
			$img_url = $parse_url['scheme'].'://'.$parse_url['host'] . $headers['Location'];
		}

		return media_sideload_image( $img_url, $post_id, $desc, 'id' );
	}

	public static function wpsync_webspark_setproduct( &$data ){
		set_time_limit(1200 );

		require_once(ABSPATH . 'wp-admin/includes/media.php');
		require_once(ABSPATH . 'wp-admin/includes/file.php');
		require_once(ABSPATH . 'wp-admin/includes/image.php');

		$woo_products = array();
		$woo_products_loaded = array();

		$woo_products_exists = get_posts( array( 'fields' => 'ids', 'numberposts' => -1, 'post_type' => 'product' ) );
		foreach( $woo_products_exists as $woo_posts_i ){
			$product = wc_get_product( $woo_posts_i );
			$sku = $product->get_sku();
			$woo_products[ $woo_posts_i ] = $sku;
		}

		//$i = 0;
		foreach( $data['data'] as $data_i ){
			/*if( $i++ > 100 ){
				break;
			}*/

			if( in_array( $data_i['sku'], $woo_products ) ){
				$product_id = wc_get_product_id_by_sku( $data_i['sku'] );
				$product = wc_get_product( $product_id );
			}else{
				$product = new \WC_Product_Simple();
			}

			if( $product instanceof \WC_Product ) {
				$woo_products_loaded[] = $product->get_id();

				$product->set_sku( $data_i['sku'] );
				$product->set_name( $data_i['name'] );
				$product->set_description( $data_i['description'] );
				$product->set_regular_price( (float) str_replace( '$', '', $data_i['price'] ) );
				$product->set_manage_stock( true );
				$product->set_stock_quantity( (int) $data_i['in_stock'] );
				$img_id = self::wpsync_webspark_setproduct_img( $data_i['picture'], $product->get_id(), $data_i['name'] );
				if( is_integer( $img_id ) ){
					$product->set_image_id( $img_id );
				}
				$product->save();
			}

			//echo memory_get_usage( true ).'<br/>';
		}

		$woo_products_delete = array_diff( $woo_products_exists, $woo_products_loaded );
		foreach( $woo_products_delete as $woo_posts_i ){
			$product = wc_get_product( $woo_posts_i );
			if( $product instanceof \WC_Product ){
				$product->delete( true );
			}
		}

		/*print_r( $woo_products_loaded );
		echo'<br/>';
		print_r( $woo_products_exists );
		echo'<br/>';
		print_r( $woo_products_delete );*/
	}

}