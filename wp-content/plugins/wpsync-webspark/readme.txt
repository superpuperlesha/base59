=== wpsync-webspark ===
Contributors: superpuperlesha
Donate link: 
Tags: woocommerce, product, sync
Stable tag: 1.0.1
Requires PHP: 8.1.2
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

The plugin synchronizes woocommerce products with api "wp.webspark.dev". Add shortcode [wpsync-webspark] to page. Add page to cron.

== Description ==

The plugin synchronizes woocommerce products with api wp.webspark.dev.

Installation:

* Install woocommerce
* Install plugin
* Add to cron page url with shortcode [wpsync-webspark]
* Setup cron repeater to 2H.

== Frequently Asked Questions ==

= A question that someone might have =

== Screenshots ==

1. The plugin has a menu here.

== Changelog ==


= 1.0.1 =
* Fix sanitize key before updating.

= 1.0.0 =
* First version.

== Upgrade Notice ==
= 1.0.0 =
First version

== A brief Markdown Example ==