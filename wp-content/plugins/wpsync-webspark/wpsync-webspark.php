<?php
/**
 * @link              
 * @since             1.0.1
 * @package           wpsync-webspark
 *
 * @wordpress-plugin
 * Plugin Name:       wpsync-webspark
 * Plugin URI:        
 * Description:       The plugin synchronizes woocommerce products with api "wp.webspark.dev". Add shortcode [wpsync-webspark] to page. Add page to cron.
 * Version:           1.0.1
 * Requires at least: 6.1
 * Requires PHP:      8.1.2
 * Author:            superpuperlesha@gmail.com
 * Author URI:        
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wpsync_webspark
 */


// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;


function wpsync_webspark_init(){
    $loaded = load_plugin_textdomain('wpsync_webspark', false, dirname(__FILE__).'/languages/');

    if( !$loaded ){
       $loaded = load_muplugin_textdomain('wpsync_webspark', '/languages/');
    }

    if( !$loaded ){
        $loaded = load_theme_textdomain('wpsync_webspark', get_stylesheet_directory().'/languages/');
    }

    if( !$loaded ){
        $locale = apply_filters('plugin_locale', function_exists('determine_locale') ?determine_locale() :get_locale(), 'wpsync_webspark');
        $mofile = dirname( __FILE__ ).'/languages/wpsync_webspark-'.$locale.'.mo';
        load_textdomain('wpsync_webspark', $mofile );
    }
}
add_action('init','wpsync_webspark_init');

include_once( dirname(__FILE__).'/functions.php' );

$wpsync_webspark = new \wpsync_webspark_ns\wpsync_webspark;